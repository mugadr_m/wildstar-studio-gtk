
#ifndef WILDSTAR_STUDIO_AREAVIEW_H
#define WILDSTAR_STUDIO_AREAVIEW_H

#include <deque>
#include <unordered_map>

#include "View.h"
#include "gl/Mesh.h"
#include "io/files/AreaReader.h"
#include "io/files/TblFile.h"
#include "io/files/StorageDefinitions.h"
#include "gl/Camera.h"
#include "gl/Texture.h"
#include "io/textures/TextureManager.h"
#include "ui/scene/MapAreaRender.h"
#include "ui/scene/TerrainMaterial.h"

#ifdef USE_CROSS_COMPILE
#include "cross_compile/mingw.mutex.h"
#else
#include <mutex>
#endif

namespace ui::views {
    class AreaView : public View {
        gl::MeshPtr mMesh;
        std::unique_ptr<scene::TerrainMaterial> mMaterial;

        gl::IndexBufferPtr mIndexBuffer;

        gl::ProgramPtr mProgram;
        int32_t mUniformMatProjection;
        int32_t mUniformMatView;
        int32_t mUniformColorTexture;
        int32_t mUniformBlendTexture;
        int32_t mUniformTextures[4];
        int32_t mUniformTexScales;

        std::deque<gl::Texture*> mTexturesToClean;
        std::mutex mUnloadLock;

        gl::CameraPtr mCamera = std::make_shared<gl::Camera>();

        io::files::TblFilePtr<io::files::WorldLayer> mWorldLayerTbl;

        io::textures::TextureManagerPtr mTextureManager;
        scene::MapAreaRenderPtr mCurrentArea;

        std::unordered_map<uint32_t, bool> mKeyMap;
        std::unordered_map<uint32_t, bool> mButtonMap;
        glm::vec2 mMousePosition;

        std::chrono::steady_clock::time_point mLastCameraUpdate = std::chrono::steady_clock::now();
        glm::vec2 mLastMousePosition;

        void loadProgram();
        void loadIndexBuffer();

        void unloadTexture(gl::Texture* texture, const std::string& tag);

        void initEventHandlers(GtkWidget* glArea);
        void updateCameraMovement();
        void updateCameraRotation();

        template<typename T, typename... R>
        bool isAnyKeyPressed(T key, R... rest) {
            return isAnyKeyPressed(key) || isAnyKeyPressed(rest...);
        }

        template<typename T>
        bool isAnyKeyPressed(T key) {
            const auto itr = mKeyMap.find(static_cast<uint32_t>(key));
            return itr!= mKeyMap.end() && itr->second;
        }

        template<typename T>
        bool isButtonPressed(T button) {
            const auto itr = mButtonMap.find(static_cast<uint32_t>(button));
            return itr != mButtonMap.end() && itr->second;
        }

    public:
        void onInitialize(GtkBuilder *builder) override;

        void onDataLoaded(const io::archive::FileManagerPtr &fileManager) override;

        void onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) override;

        bool needsOpenGL() const override;

        bool needsCode() const override;

        void onFrame(const std::shared_ptr<gl::GlArea> &glArea) override;
    };
}


#endif //WILDSTAR_STUDIO_AREAVIEW_H
