
#include <log.h>
#include <utils/Utilities.h>
#include "TextureView.h"
#include "io/files/TexReader.h"
#include "modules/libtex.h"
#include <sstream>

namespace ui::views {
    void TextureView::onFrame(const std::shared_ptr<gl::GlArea> &glArea) {
        View::onFrame(glArea);

        if (mActiveTexture == nullptr) {
            return;
        }

        mProgram->setMatrix(mMatrixUniform, glArea->getOrthoMatrix());

        glDisable(GL_DEPTH_TEST);

        glActiveTexture(GL_TEXTURE0);
        glEnable(GL_TEXTURE_2D);
        mActiveTexture->bind();

        mProgram->setInt(mTextureUniform, 0);

        glDisable(GL_BLEND);
        mMesh->setVertexBuffer(mTextureBuffer);
        mMesh->startDraw();
        mMesh->draw();

        if (mInfoTexture != nullptr) {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            mMesh->setVertexBuffer(mInfoBuffer);
            mInfoTexture->bind();
            mMesh->startDraw();
            mMesh->draw();
        }
    }

    void TextureView::hidden() {
        View::hidden();

        gtk_widget_hide(mTextureViewToolbar);
    }

    void TextureView::onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) {
        View::onOpen(fileName, fileContent);

        mFileName = fileName;
        std::replace(mFileName.begin(), mFileName.end(), '\\', '/');

        gtk_widget_show(mTextureViewToolbar);

        try {
            io::files::TexReader texReader(fileContent);
            const io::files::TexHeader &header = texReader.getTexHeader();

            mTextureBuffer->setData(std::array<float, 16>{
                    10.0f, 10.0f, 0.0f, 0.0f,
                    10.0f + header.width, 10.0f, 1.0f, 0.0f,
                    10.0f + header.width, 10.0f + header.height, 1.0f, 1.0f,
                    10.0f, 10.0f + header.height, 0.0f, 1.0f
            });

            mActiveTexture = std::make_shared<gl::Texture>();

            switch (header.formatIndex) {
                case 0:
                case 1:
                    if (header.isCompressed == 0) {
                        loadFromARGBUncompressed(texReader);
                    } else {
                        loadFromARGBCompressed(texReader);
                    }

                case 13:
                case 14:
                case 15:
                    loadFromDXT(texReader);
                    break;

                default: {
                    log_console->warn("Texture format is not supported yet: {}", header.formatIndex);
                    throw ::utils::RuntimeError("Unsupported texture format");
                }
            }

            mTextureWidth = header.width;
            mTextureHeight = header.height;
            uint32_t infoWidth = 0, infoHeight = 0;
            mInfoTexture = buildTextureForInfo(texReader, infoWidth, infoHeight);
            mInfoBuffer->setData(std::array<float, 16>{
                    10.0f + header.width + 10.0f, 10.0f, 0.0f, 0.0f,
                    10.0f + header.width + 10.0f + infoWidth, 10.0f, 1.0f, 0.0f,
                    10.0f + header.width + 10.0f + infoWidth, 10.0f + infoHeight, 1.0f, 1.0f,
                    10.0f + header.width + 10.0f, 10.0f + infoHeight, 0.0f, 1.0f
            });
        } catch (std::exception &e) {
            onLoadError(std::string(e.what()));
        }
    }

    void TextureView::loadFromDXT(const io::files::TexReader &reader) {
        const auto formatIndex = reader.getTexHeader().formatIndex;
        const auto dxtFormat = formatIndex == 13 ? gl::DxtImageFormat::BC1 :
                               formatIndex == 14 ? gl::DxtImageFormat::BC2 : gl::DxtImageFormat::BC3;
        mActiveTexture->loadFromMemoryDXT(dxtFormat, reader.getTexHeader().width,
                                          reader.getTexHeader().height, reader.getLayerData()[0]);
    }

    void TextureView::loadFromARGBUncompressed(const io::files::TexReader &reader) {
        std::vector<uint8_t> imageData = reader.getLayerData()[0];
        for (auto i = 0u; i < imageData.size(); i += 4) {
            auto &r = imageData[i];
            auto &b = imageData[i + 2];
            auto tmp = r;
            r = b;
            b = tmp;
        }

        mActiveTexture->loadFromMemoryARGB(reader.getTexHeader().width, reader.getTexHeader().height, imageData);
    }

    void TextureView::loadFromARGBCompressed(const io::files::TexReader &reader) {
        const auto &header = reader.getTexHeader();
        std::vector<uint8_t> colorData(header.width * header.height * 4);

        libtex::decompressImageLayer(header.textureType, reader.getLayerData()[0].data(), reader.getLayerSizes()[0], header.width, header.height,
                                     header.layerInfo[0].qualityCode, header.layerInfo[0].hasDefaultValue, header.layerInfo[0].defaultValue,
                                     header.layerInfo[1].qualityCode, header.layerInfo[1].hasDefaultValue, header.layerInfo[1].defaultValue,
                                     header.layerInfo[2].qualityCode, header.layerInfo[2].hasDefaultValue, header.layerInfo[2].defaultValue,
                                     header.layerInfo[3].qualityCode, header.layerInfo[3].hasDefaultValue, header.layerInfo[3].defaultValue,
                                     colorData.data()
        );

        mActiveTexture->loadFromMemoryARGB(header.width, header.height, colorData);
    }

    TextureView::TextureView(std::shared_ptr<gl::GlArea> glArea) : mGlArea(std::move(glArea)) {

    }

    bool TextureView::needsOpenGL() const {
        return true;
    }

    bool TextureView::needsCode() const {
        return false;
    }

    void TextureView::onInitialize(GtkBuilder *builder) {
        View::onInitialize(builder);

        mTextureViewToolbar = GTK_WIDGET(gtk_builder_get_object(builder, "textureViewToolbar"));
        initEventHandlers();

        mProgram = loadProgram();

        mMesh = std::make_shared<gl::Mesh>();
        mMesh->addElement("position", 0, 2);
        mMesh->addElement("texCoord", 0, 2);

        mMesh->setProgram(mProgram);

        mMesh->finalize();

        mTextureUniform = mProgram->getUniform("imageTexture");
        mMatrixUniform = mProgram->getUniform("projectionMatrix");

        mMesh->getIndexBuffer()->setData(std::array<uint8_t, 6>{
                0, 1, 2,
                0, 2, 3
        });

        mMesh->getIndexBuffer()->setIndexType(gl::IndexType::BYTE);
        mMesh->setIndexCount(6);

        mTextureBuffer = std::make_shared<gl::VertexBuffer>();
        mInfoBuffer = std::make_shared<gl::VertexBuffer>();
    }

    gl::ProgramPtr TextureView::loadProgram() {
        const auto program = std::make_shared<gl::Program>();
        program->compileVertexShader(R"(
attribute vec2 position0;
attribute vec2 texCoord0;

varying vec2 texCoord;

uniform mat4 projectionMatrix;

void main() {
    gl_Position = projectionMatrix * vec4(position0, 0.0, 1.0);
    texCoord = texCoord0;
}
)");

        program->compileFragmentShader(R"(
#version 130

varying vec2 texCoord;

uniform sampler2D imageTexture;

void main() {
    gl_FragColor = texture(imageTexture, texCoord);
}
)");

        program->link();

        return program;
    }

    gl::TexturePtr TextureView::createTextureFromString(const std::string &text, const std::string &error) {
        std::unique_ptr<cairo_surface_t, utils::CairoSurfaceFree> surfacePtr(cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 1024, 1024));

        {
            std::unique_ptr<cairo_t, utils::CairoContextFree> ctx(cairo_create(surfacePtr.get()));
            auto c = ctx.get();

            cairo_move_to(c, 0.0, 0.0);
            cairo_set_source_rgb(c, 1.0, 1.0, 1.0);
            cairo_rectangle(c, 0.0, 0.0, 1024.0, 1024.0);
            cairo_fill(c);

            cairo_text_extents_t te;
            cairo_text_extents_t t2;
            cairo_select_font_face(c, "sans", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
            cairo_set_font_size(c, 32.0);
            cairo_set_source_rgb(c, 0.0, 0.0, 0.0);
            cairo_text_extents(c, text.c_str(), &te);
            cairo_text_extents(c, error.c_str(), &t2);
            const auto wOffset = std::max((te.width / 2) + te.x_bearing, (t2.width / 2) + t2.x_bearing);
            const auto hOffset = ((te.height + t2.height) / 2) + te.y_bearing + t2.y_bearing;
            cairo_move_to(c, 512.0 - wOffset, 512 - hOffset);
            cairo_show_text(c, text.c_str());
            cairo_move_to(c, 512.0 - wOffset, 512 - hOffset + te.height + te.y_bearing + 32.0);
            cairo_show_text(c, error.c_str());
        }

        const auto dataSize = cairo_image_surface_get_stride(surfacePtr.get()) * cairo_image_surface_get_height(surfacePtr.get());
        const auto ptr = cairo_image_surface_get_data(surfacePtr.get());

        std::vector<uint8_t> imageData((unsigned long) dataSize);
        memcpy(imageData.data(), ptr, (size_t) dataSize);

        auto texture = std::make_shared<gl::Texture>();
        texture->loadFromMemoryARGB(1024, 1024, imageData);

        return texture;
    }

    void TextureView::onLoadError(const std::string &text) {
        mMesh->getVertexBuffer()->setData(std::array<float, 16>{
                10.0f, 10.0f, 0.0f, 0.0f,
                10.0f + 1024, 10.0f, 1.0f, 0.0f,
                10.0f + 1024, 10.0f + 1024, 1.0f, 1.0f,
                10.0f, 10.0f + 1024, 0.0f, 1.0f
        });

        mActiveTexture = createTextureFromString("There was an error loading the texture:", text);
    }

    void TextureView::initEventHandlers() {
        auto saveAsPngButton = utils::getChildByName(mTextureViewToolbar, "saveTextureAsPngButton");
        auto saveAsBmpButton = utils::getChildByName(mTextureViewToolbar, "saveTextureAsBmpButton");

        utils::registerEvent(saveAsBmpButton, "clicked", [this]() {
            if (mActiveTexture == nullptr) {
                return;
            }
            utils::writeBmp(mTextureWidth, mTextureHeight, getTexturePixels(true), setupTextureOutputPath("bmp"));
        });

        utils::registerEvent(saveAsPngButton, "clicked", [this]() {
            if (mActiveTexture == nullptr) {
                return;
            }
            utils::writePng(mTextureWidth, mTextureHeight, getTexturePixels(false), setupTextureOutputPath("png"));
        });
    }

    std::string TextureView::setupTextureOutputPath(const std::string &extension) {
        const auto index = mFileName.rfind('.');
        std::string outName;
        if (index != std::string::npos) {
            std::stringstream stream;
            stream << mFileName.substr(0, index) << '.' << extension;
            outName = stream.str();
        } else {
            std::stringstream stream;
            stream << mFileName << '.' << extension;
            outName = stream.str();
        }

        utils::createDirectories(outName);
        return outName;
    }

    std::vector<uint8_t> TextureView::getTexturePixels(bool flipRgb) {
        mActiveTexture->bind();
        std::vector<uint8_t> textureData(mTextureWidth * mTextureHeight * 4);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData.data());
        if (flipRgb) {
            for (auto i = 0u; i < textureData.size(); i += 4) {
                auto &r = textureData[i];
                auto &b = textureData[i + 2];
                const auto tmp = r;
                r = b;
                b = tmp;
            }
        }

        return textureData;
    }

    gl::TexturePtr TextureView::buildTextureForInfo(const io::files::TexReader &reader, uint32_t &width, uint32_t &height) {
        const auto &header = reader.getTexHeader();

        std::stringstream stream;
        stream << "Dimension: " << header.width << "x" << header.height << std::endl;
        stream << "Format:    " << header.formatIndex << " (" << textureFormatToString(header.formatIndex) << ")" << std::endl;
        stream << "Mip Count: " << header.mipLevels << std::endl;
        stream << "JPEG:      " << (header.isCompressed != 0 ? "yes" : "no");
        auto text = stream.str();

        utils::CairoSurfacePtr surface(cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 1, 1));
        auto *st = surface.get();
        utils::CairoContextPtr context(cairo_create(st));
        auto *ct = context.get();
        utils::PangoPtr<PangoLayout> layout(pango_cairo_create_layout(ct));
        auto *lt = layout.get();
        pango_layout_set_text(lt, text.c_str(), static_cast<int>(text.size()));
        utils::PangoFontDescPtr fontDescription(pango_font_description_new());
        auto *fontDesc = fontDescription.get();
        pango_font_description_set_family(fontDesc, "Mono");
        pango_font_description_set_weight(fontDesc, PANGO_WEIGHT_BOLD);
        pango_font_description_set_absolute_size(fontDesc, 30 * PANGO_SCALE);
        pango_layout_set_font_description(lt, fontDesc);
        PangoRectangle textRec{};
        pango_layout_get_pixel_extents(lt, nullptr, &textRec);

        const auto totalWidth = textRec.width + std::abs(textRec.x);
        const auto totalHeight = textRec.height + std::abs(textRec.y);

        layout.reset();
        context.reset();
        surface.reset();

        surface.reset(cairo_image_surface_create(CAIRO_FORMAT_ARGB32, totalWidth, totalHeight));
        st = surface.get();
        context.reset(cairo_create(st));
        ct = context.get();
        layout.reset(pango_cairo_create_layout(ct));
        lt = layout.get();

        pango_layout_set_text(lt, text.c_str(), static_cast<int>(text.size()));
        pango_layout_set_font_description(lt, fontDesc);

        cairo_set_source_rgba(ct, 0.0f, 0.0f, 0.0f, 0.0f);
        cairo_rectangle(ct, 0.0, 0.0, totalWidth, totalHeight);
        cairo_fill(ct);
        cairo_set_source_rgba(ct, 0.0f, 0.0f, 0.0f, 1.0f);
        cairo_move_to(ct, textRec.x, textRec.y);
        pango_cairo_show_layout(ct, lt);

        layout.reset();
        context.reset();

        const auto dataSize = cairo_image_surface_get_stride(st) * cairo_image_surface_get_height(st);
        const auto ptr = cairo_image_surface_get_data(st);

        std::vector<uint8_t> imageData((unsigned long) dataSize);
        memcpy(imageData.data(), ptr, (size_t) dataSize);

        auto texture = std::make_shared<gl::Texture>();
        texture->loadFromMemoryARGB((uint32_t) totalWidth, (uint32_t) totalHeight, imageData);

        width = (uint32_t) totalWidth;
        height = (uint32_t) totalHeight;

        return texture;
    }

    std::string TextureView::textureFormatToString(uint32_t formatIndex) const {
        switch (formatIndex) {
            case 0:
                return "BGRA8";
            case 1:
                return "BGRX8";
            case 6:
                return "R8";
            case 7:
                return "R16";
            case 8:
                return "R16G16";
            case 9:
                return "R16G16B16A16";
            case 10:
                return "R8G8_SNORM";
            case 11:
                return "RGBA8_SNORM";
            case 12:
                return "R16G16_SNORM";
            case 13:
                return "DXT1";
            case 14:
                return "DXT3";
            case 15:
                return "DXT5";
            case 16:
                return "R16_FLOAT";
            case 17:
                return "R16G16_FLOAT";
            case 18:
                return "R16G16B16A16_FLOAT";
            case 19:
                return "R32_FLOAT";
            case 20:
                return "R32G32B32A32_FLOAT";
            case 21:
                return "D16";
            case 23:
                return "D24_S8";
            case 26:
                return "R10G10B10A2";
            default:
                return "Unknown";
        }
    }
}
