
#ifndef WILDSTAR_STUDIO_CODEVIEW_H
#define WILDSTAR_STUDIO_CODEVIEW_H

#include <gtk/gtk.h>
#include <string>

#include "io/utils/BinaryReader.h"
#include "View.h"

namespace ui::views {
    class CodeView : public View {
        GtkWidget *mCodeView;
        GtkWidget* mCodeViewNoHighlight;

        std::string getLanguage(const std::string &fileName);

        bool isUtf8(const std::string &textData);

    public:
        CodeView(GtkWidget *codeView, GtkWidget* codeViewNoHighlight);

        void onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) override;

        bool needsOpenGL() const override {
            return false;
        }

        bool needsCode() const override {
            return true;
        }

        bool hasHighlight() const override;
    };
}


#endif //WILDSTAR_STUDIO_CODEVIEW_H
