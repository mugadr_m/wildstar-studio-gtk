
#include <io/files/DynamicTblFile.h>
#include <log.h>
#include <io/files/CsvTableWriter.h>
#include <io/files/SqlTableWriter.h>
#include <io/files/XslxTableWriter.h>
#include "TableView.h"

namespace ui::views {
    TableView::TableView(GtkWidget *control) : mWidget(control), mTreeView(GTK_TREE_VIEW(control)) {

    }

    TableView::~TableView() {
        mCancelLoad = true;
        if (mLoadThread.joinable()) {
            mLoadThread.join();
        }
    }

    void TableView::onDataLoaded(const io::archive::FileManagerPtr &fileManager) {
        View::onDataLoaded(fileManager);
    }

    void TableView::onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) {
        View::onOpen(fileName, fileContent);

        mFileName = fileName;
        std::replace(mFileName.begin(), mFileName.end(), '\\', '/');

        mTableFile = std::make_shared<io::files::DynamicTblFile>(fileContent);
        rebuildTreeStore();

        gtk_widget_show(mTableToolbarWidget);
    }

    bool TableView::needsOpenGL() const {
        return false;
    }

    bool TableView::needsCode() const {
        return false;
    }

    bool TableView::hasHighlight() const {
        return false;
    }

    void TableView::rebuildTreeStore() {
        cleanupTreeView();

        const auto &fields = mTableFile->getFields();
        std::vector<GType> columnTypes(fields.size(), G_TYPE_STRING);

        mTreeStore = gtk_tree_store_newv(static_cast<gint>(columnTypes.size()), columnTypes.data());
        gtk_tree_view_set_model(mTreeView, GTK_TREE_MODEL(mTreeStore));

        auto fieldIndex = 0u;
        for (const auto &field : fields) {
            const auto column = gtk_tree_view_column_new();
            const auto renderer = gtk_cell_renderer_text_new();

            gtk_tree_view_column_set_title(column, field.getFieldName().c_str());

            gtk_tree_view_column_pack_start(column, renderer, false);
            gtk_tree_view_column_add_attribute(column, renderer, "text", fieldIndex++);

            gtk_tree_view_append_column(mTreeView, column);
        }

        mCancelLoad = false;
        auto curIteration = mLoadIteration;

        mIsLoading = true;
        mLoadThread = std::thread([this, curIteration]() {
            for (auto i = 0u; i < mTableFile->getNumRecords(); i += 1000) {
                if (mCancelLoad || mLoadIteration != curIteration) {
                    return;
                }

                const auto recordCount = std::min<std::size_t>(100, mTableFile->getNumRecords() - i);
                const auto records = mTableFile->loadRecordPage(i, recordCount);

                utils::runInMainThread([this, records, curIteration]() {
                    for (const auto &record: records) {
                        if (mCancelLoad || mTreeStore == nullptr || mLoadIteration != curIteration) {
                            return;
                        }

                        GtkTreeIter elementIter{};
                        gtk_tree_store_append(mTreeStore, &elementIter, nullptr);
                        auto index = 0u;
                        std::vector<GValue> values(record.size(), G_VALUE_INIT);
                        std::vector<gint> columns;
                        for (const auto &field : record) {
                            auto* value = &values[index];
                            g_value_init(value, G_TYPE_STRING);
                            g_value_set_string(value, field.c_str());
                            columns.emplace_back(index++);
                        }

                        gtk_tree_store_set_valuesv(mTreeStore, &elementIter, columns.data(), values.data(), static_cast<gint>(values.size()));
                    }
                }, G_PRIORITY_LOW);

                std::this_thread::yield();
            }

            mIsLoading = false;
        });
    }

    void TableView::cleanupTreeView() {
        ++mLoadIteration;
        mCancelLoad = true;
        if (mLoadThread.joinable()) {
            mLoadThread.join();
        }

        if (mTreeStore != nullptr) {
            g_object_unref(mTreeStore);
            mTreeStore = nullptr;
            gtk_tree_view_set_model(mTreeView, nullptr);
        }

        const auto curColumns = gtk_tree_view_get_columns(mTreeView);
        for (auto l = curColumns; l != nullptr; l = l->next) {
            gtk_tree_view_remove_column(mTreeView, GTK_TREE_VIEW_COLUMN(l->data));
        }

        g_list_free(curColumns);
    }

    bool TableView::hasTable() const {
        return true;
    }

    void TableView::onInitialize(GtkBuilder *builder) {
        View::onInitialize(builder);

        mTableToolbarWidget = GTK_WIDGET(gtk_builder_get_object(builder, "tableViewToolbar"));
        initEventHandlers(builder);
    }

    void TableView::hidden() {
        gtk_widget_hide(mTableToolbarWidget);
        mTableFile = nullptr;
    }

    void TableView::initEventHandlers(GtkBuilder *builder) {
        utils::registerEvent(gtk_builder_get_object(builder, "saveTblAsTblButton"), "clicked", [this]() {
            io::utils::BinaryWriter writer{ mTableFile->getReader().getData() };
            writeTable(writer, "tbl");
        });

        utils::registerEvent(gtk_builder_get_object(builder, "saveTblAsCsvButton"), "clicked", std::bind(&TableView::saveTable, this, "csv", [this](io::utils::BinaryWriter& writer){ return std::make_shared<io::files::CsvTableWriter>(writer, mTableFile); }));
        utils::registerEvent(gtk_builder_get_object(builder, "saveTblAsSqlButton"), "clicked", std::bind(&TableView::saveTable, this, "sql", [this](io::utils::BinaryWriter& writer){ return std::make_shared<io::files::SqlTableWriter>(writer, mTableFile); }));
        utils::registerEvent(gtk_builder_get_object(builder, "saveTblAsXlsxButton"), "clicked", std::bind(&TableView::saveTable, this, "xlsx", [this](io::utils::BinaryWriter& writer){ return std::make_shared<io::files::XlsxTableWriter>(writer, mTableFile); }));
    }

    void TableView::saveTable(const std::string& extension, std::function<std::shared_ptr<io::files::TableWriter> (io::utils::BinaryWriter&)> writerSupplier) {
        io::utils::BinaryWriter writer;
        auto tblWriter = writerSupplier(writer);
        auto records = mTableFile->loadRecordPage(0, mTableFile->getNumRecords());
        for(const auto& record : records) {
            tblWriter->convertRow(record);
        }

        tblWriter->onClose();
        writeTable(writer, extension);
    }

    void TableView::writeTable(io::utils::BinaryWriter& writer, const std::string& extension) {
        const auto outName = setupTextureOutputPath(extension);
        auto file = fopen(outName.c_str(), "wb");
        std::unique_ptr<FILE, utils::FileCloser> fp(file);

        auto content = writer.getFullData();
        fwrite(content.data(), 1, content.size(), file);
    }

    std::string TableView::setupTextureOutputPath(const std::string &extension) {
        const auto index = mFileName.rfind('.');
        std::string outName;
        if (index != std::string::npos) {
            std::stringstream stream;
            stream << mFileName.substr(0, index) << '.' << extension;
            outName = stream.str();
        } else {
            std::stringstream stream;
            stream << mFileName << '.' << extension;
            outName = stream.str();
        }

        utils::createDirectories(outName);
        return outName;
    }
}