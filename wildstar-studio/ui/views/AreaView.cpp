
#include <io/files/AreaReader.h>
#include <io/archive/FileManager.h>
#include "AreaView.h"

namespace ui::views {

    void AreaView::onInitialize(GtkBuilder *builder) {
        View::onInitialize(builder);

        loadProgram();

        mMesh = std::make_shared<gl::Mesh>();
        mMesh->setProgram(mProgram);

        mMesh->addElement("position", 0, 3);
        mMesh->addElement("normal", 0, 3);
        mMesh->addElement("texCoord", 0, 2);

        mMesh->finalize();

        mIndexBuffer = std::make_shared<gl::IndexBuffer>(gl::IndexType::SHORT);
        loadIndexBuffer();

        mMaterial = std::make_unique<scene::TerrainMaterial>(mMesh, mUniformTexScales);

        initEventHandlers(GTK_WIDGET(gtk_builder_get_object(builder, "glRenderArea")));
    }

    bool AreaView::needsOpenGL() const {
        return true;
    }

    bool AreaView::needsCode() const {
        return false;
    }

    void AreaView::onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) {
        View::onOpen(fileName, fileContent);

        io::files::AreaReader areaReader(0, 0, fileContent, mWorldLayerTbl);

        const auto minPos = areaReader.getMinPos();
        const auto maxPos = areaReader.getMaxPos();

        const auto midPoint = (minPos + maxPos) / 2.0f;
        mCamera->setUp(glm::vec3(0.0f, 0.0f, 1.0f));
        mCamera->setPosition(glm::vec3(0.0f, 0.0f, midPoint.z));
        mCamera->setTarget(glm::vec3(1.0f, 1.0f, midPoint.z));

        const auto area = std::make_shared<scene::MapAreaRender>(mTextureManager);
        area->load(areaReader);
        mCurrentArea = area;

        mLastCameraUpdate = std::chrono::steady_clock::now();
        mLastMousePosition = mMousePosition;
    }

    void AreaView::onDataLoaded(const io::archive::FileManagerPtr &fileManager) {
        View::onDataLoaded(fileManager);

        mTextureManager = std::make_shared<io::textures::TextureManager>(fileManager, std::bind(&AreaView::unloadTexture, this, std::placeholders::_1, std::placeholders::_2));

        auto worldLayerData = fileManager->openFile("DB/WorldLayer.tbl");
        mWorldLayerTbl = std::make_shared<io::files::TblFile<io::files::WorldLayer>>(worldLayerData);
    }

    void AreaView::loadProgram() {
        mProgram = std::make_shared<gl::Program>();

        mProgram->compileVertexShader(R"(
attribute vec3 position0;
attribute vec3 normal0;
attribute vec2 texCoord0;

varying vec3 normal;
varying vec2 texCoord;

uniform mat4 matView;
uniform mat4 matProjection;

void main() {
    gl_Position = matProjection * matView * vec4(position0, 1.0);
    normal = normal0;
    texCoord = texCoord0;
}
)");

        mProgram->compileFragmentShader(R"(
#version 130

varying vec3 normal;
varying vec2 texCoord;

uniform sampler2D colorTexture;
uniform sampler2D blendTexture;

uniform sampler2D texture0;
uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

uniform vec4 texScales;

const vec3 sunDirection = vec3(1, 1, -1);
const vec3 diffuseLight = vec3(0.8, 0.8, 0.8);

vec3 getDiffuseLight() {
	float light = dot(normal, -normalize(sunDirection));
	if (light < 0.0)
		light = 0.0;
	if (light > 0.5)
		light = 0.5 + (light - 0.5) * 0.65;

	vec3 diffuse = diffuseLight * light;
	diffuse += vec3(0.2, 0.2, 0.2);
	diffuse = clamp(diffuse, 0.0, 1.0);

	return diffuse;
}

void main() {
    vec2 texCoordReal = clamp(texCoord, 0.0, 1.0);
    vec4 alpha = texture(blendTexture, texCoordReal);
    vec4 tex1 = texture(texture0, texCoordReal * texScales.x);
    vec4 tex2 = texture(texture1, texCoordReal * texScales.y);
    vec4 tex3 = texture(texture2, texCoordReal * texScales.z);
    vec4 tex4 = texture(texture3, texCoordReal * texScales.w);

    vec4 retColor = tex1 * alpha.x + tex2 * alpha.y + tex3 * alpha.z + tex4 * (1.0 - alpha.x - alpha.y - alpha.z);

    vec4 colorMap = texture(colorTexture, texCoordReal);
    retColor.rgb *= colorMap.rgb * 2.0;

    retColor.rgb *= getDiffuseLight();
    gl_FragColor = retColor;
}
)");

        mProgram->link();

        mUniformMatProjection = mProgram->getUniform("matProjection");
        mUniformMatView = mProgram->getUniform("matView");
        mUniformColorTexture = mProgram->getUniform("colorTexture");
        mUniformBlendTexture = mProgram->getUniform("blendTexture");
        for(auto i = 0u; i < 4; ++i) {
            mUniformTextures[i] = mProgram->getUniform(std::string("texture") + std::to_string(i));
        }

        mUniformTexScales = mProgram->getUniform("texScales");
    }

    void AreaView::loadIndexBuffer() {
        std::vector<uint16_t> indices(16 * 16 * 4 * 3);
        for(auto i = 0u; i < 16; ++i) {
            for(auto j = 0u; j < 16; ++j) {
                const auto tribase = (i * 16 + j) * 4 * 3;
                const auto ibase = i * 33 + j;

                indices[tribase] = (uint16_t) ibase;
                indices[tribase + 1] = static_cast<uint16_t>(ibase + 1);
                indices[tribase + 2] = static_cast<uint16_t>(ibase + 17);

                indices[tribase + 3] = static_cast<uint16_t>(ibase + 1);
                indices[tribase + 4] = static_cast<uint16_t>(ibase + 34);
                indices[tribase + 5] = static_cast<uint16_t>(ibase + 17);

                indices[tribase + 6] = static_cast<uint16_t>(ibase + 34);
                indices[tribase + 7] = static_cast<uint16_t>(ibase + 33);
                indices[tribase + 8] = static_cast<uint16_t>(ibase + 17);

                indices[tribase + 9] = static_cast<uint16_t>(ibase + 33);
                indices[tribase + 10] = (uint16_t) ibase;
                indices[tribase + 11] = static_cast<uint16_t>(ibase + 17);
            }
        }

        mIndexBuffer->setData(indices);
        mMesh->setIndexCount(static_cast<uint32_t>(indices.size()));
    }

    void AreaView::onFrame(const std::shared_ptr<gl::GlArea> &glArea) {
        View::onFrame(glArea);

        mTextureManager->onFrame();

        if(mCurrentArea == nullptr) {
            mLastMousePosition = mMousePosition;
            mLastCameraUpdate = std::chrono::steady_clock::now();
            return;
        }

        updateCameraMovement();
        updateCameraRotation();

        mLastCameraUpdate = std::chrono::steady_clock::now();

        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        mProgram->setMatrix(mUniformMatProjection, glArea->getPerspectiveMatrix());
        mProgram->setMatrix(mUniformMatView, mCamera->getViewMatrix());
        mProgram->setInt(mUniformColorTexture, 0);
        mProgram->setInt(mUniformBlendTexture, 1);
        for(auto i = 0u; i < 4; ++i) {
            mProgram->setInt(mUniformTextures[i], i + 2);
        }

        mMesh->setIndexBuffer(mIndexBuffer);

        mCurrentArea->onFrame(mMesh, *mMaterial);

        if(mUnloadLock.try_lock())
        {
            std::lock_guard<std::mutex> l(mUnloadLock, std::adopt_lock);
            while(!mTexturesToClean.empty()) {
                delete mTexturesToClean.front();
                mTexturesToClean.pop_front();
            }
        }
    }

    void AreaView::unloadTexture(gl::Texture *texture, const std::string& tag) {
        log_console->debug("Unloading texture {}", tag);
        std::lock_guard<std::mutex> l(mUnloadLock);
        mTexturesToClean.emplace_back(texture);
    }

    void AreaView::initEventHandlers(GtkWidget* glArea) {
        utils::registerKeyEvent(glArea, "key_press_event", [this](uint32_t key, uint32_t state) {
            mKeyMap[key] = true;
        });

        utils::registerKeyEvent(glArea, "key_release_event", [this](uint32_t key, uint32_t state) {
            mKeyMap.erase(key);
        });

        std::function<void(uint32_t, bool)> buttonCallback = [this](uint32_t button, bool pressed) {
            if(pressed) {
                mButtonMap[button] = true;
            } else {
                mButtonMap.erase(button);
            }
        };

        gtk_widget_add_events(glArea, GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_BUTTON_MOTION_MASK | GDK_POINTER_MOTION_MASK);

        utils::registerButtonEvent(glArea, "button-press-event", buttonCallback);
        utils::registerButtonEvent(glArea, "button-release-event", buttonCallback);
        utils::registerMotionEvent(glArea, "motion_notify_event", [this](double x, double y, double rootX, double rootY) {
            mMousePosition = glm::vec2(rootX, rootY);
        });
    }

    void AreaView::updateCameraMovement() {
        namespace cr = std::chrono;
        const auto now = cr::steady_clock::now();
        const auto diff = (cr::duration_cast<cr::microseconds>(now - mLastCameraUpdate).count() / 1.e6);

        const auto amount = static_cast<float>(diff * 50.0);

        if(isAnyKeyPressed(GDK_KEY_s, GDK_KEY_S)) {
            mCamera->moveBackward(amount);
        }
        if(isAnyKeyPressed(GDK_KEY_w, GDK_KEY_W)) {
            mCamera->moveForward(amount);
        }
        if(isAnyKeyPressed(GDK_KEY_a, GDK_KEY_A)) {
            mCamera->moveLeft(amount);
        }
        if(isAnyKeyPressed(GDK_KEY_d, GDK_KEY_D)) {
            mCamera->moveRight(amount);
        }
        if(isAnyKeyPressed(GDK_KEY_q, GDK_KEY_Q)) {
            mCamera->moveUp(amount);
        }
        if(isAnyKeyPressed(GDK_KEY_e, GDK_KEY_E)) {
            mCamera->moveDown(amount);
        }
    }

    void AreaView::updateCameraRotation() {
        if(isButtonPressed(3)) {
            const auto dx = mMousePosition.x - mLastMousePosition.x;
            const auto dy = mMousePosition.y - mLastMousePosition.y;
            if(std::abs(dx) >= 1e-5) {
                mCamera->yaw(dx * 0.25f);
            }
            if(std::abs(dy) >= 1e-5) {
                mCamera->pitch(dy * 0.25f);
            }
        }

        mLastMousePosition = mMousePosition;
    }
}