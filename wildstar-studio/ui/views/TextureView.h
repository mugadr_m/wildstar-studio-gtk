
#ifndef WILDSTAR_STUDIO_TEXTUREVIEW_H
#define WILDSTAR_STUDIO_TEXTUREVIEW_H

#include <memory>
#include <io/files/TexReader.h>
#include "View.h"
#include "gl/Texture.h"
#include "gl/GlArea.h"
#include "gl/Mesh.h"

namespace ui::views {
    class TextureView : public View {
        gl::TexturePtr mActiveTexture;
        gl::TexturePtr mInfoTexture;

        std::shared_ptr<gl::GlArea> mGlArea;

        uint32_t mTextureWidth;
        uint32_t mTextureHeight;

        int32_t mTextureUniform;
        int32_t mMatrixUniform;

        gl::MeshPtr mMesh;
        gl::ProgramPtr mProgram;

        gl::VertexBufferPtr mTextureBuffer;
        gl::VertexBufferPtr mInfoBuffer;

        GtkWidget* mTextureViewToolbar;

        std::string mFileName;

        void loadFromDXT(const io::files::TexReader &reader);

        void loadFromARGBUncompressed(const io::files::TexReader &reader);

        void loadFromARGBCompressed(const io::files::TexReader &reader);

        gl::ProgramPtr loadProgram();

        void onLoadError(const std::string& text);

        gl::TexturePtr createTextureFromString(const std::string& text, const std::string& error);

        void initEventHandlers();

        std::vector<uint8_t> getTexturePixels(bool fipRgba);
        std::string setupTextureOutputPath(const std::string& extension);

        gl::TexturePtr buildTextureForInfo(const io::files::TexReader& reader, uint32_t& width, uint32_t& height);

        std::string textureFormatToString(uint32_t formatIndex) const;

    public:
        explicit TextureView(std::shared_ptr<gl::GlArea> glArea);

        void onInitialize(GtkBuilder* builder) override;

        void onFrame(const std::shared_ptr<gl::GlArea>& glArea) override;

        void hidden() override;

        void onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) override;

        bool needsOpenGL() const override;

        bool needsCode() const override;
    };
}


#endif //WILDSTAR_STUDIO_TEXTUREVIEW_H
