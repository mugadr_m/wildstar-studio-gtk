
#ifndef WILDSTAR_STUDIO_TABLEVIEW_H
#define WILDSTAR_STUDIO_TABLEVIEW_H

#include <atomic>
#include "View.h"
#include "io/files/DynamicTblFile.h"

#ifdef USE_CROSS_COMPILE
#include "cross_compile/mingw.thread.h"
#include "cross_compile/mingw.mutex.h"
#include "cross_compile/mingw.condition_variable.h"
#else
#include <thread>
#include <mutex>
#include <condition_variable>
#include <io/files/TableWriter.h>

#endif

namespace ui::views {
    class TableView : public View {
        GtkWidget* mWidget;
        GtkTreeView* mTreeView;
        GtkTreeStore* mTreeStore = nullptr;

        uint64_t mLoadIteration;
        std::atomic_bool mCancelLoad;
        std::atomic_bool mIsLoading;
        std::thread mLoadThread;

        GtkWidget* mTableToolbarWidget;

        io::files::DynamicTblFilePtr mTableFile;
        std::string mFileName;

        void rebuildTreeStore();

        void cleanupTreeView();

        void initEventHandlers(GtkBuilder* builder);

        void saveTable(const std::string& extension, std::function<std::shared_ptr<io::files::TableWriter> (io::utils::BinaryWriter&)> writerSupplier);
        void writeTable(io::utils::BinaryWriter& writer, const std::string& extension);
        std::string setupTextureOutputPath(const std::string& extension);

    public:
        explicit TableView(GtkWidget* control);
        ~TableView() override;

        void onInitialize(GtkBuilder *builder) override;

        void onDataLoaded(const io::archive::FileManagerPtr &fileManager) override;

        void onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) override;

        void hidden() override;

        bool needsOpenGL() const override;

        bool needsCode() const override;

        bool hasHighlight() const override;

        bool hasTable() const override;
    };
}


#endif //WILDSTAR_STUDIO_TABLEVIEW_H
