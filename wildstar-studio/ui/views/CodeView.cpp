
#include <io/utils/FileReader.h>
#include "CodeView.h"
#include <sstream>
#include <iomanip>
#include <algorithm>
#include "log.h"
#include "utils/Utilities.h"
#include <gtksourceview/gtksource.h>
#include "io/settings/SettingsManager.h"

namespace ui::views {
    CodeView::CodeView(GtkWidget *codeView, GtkWidget* codeViewNoHighlight) : mCodeView(codeView), mCodeViewNoHighlight(codeViewNoHighlight) {
    }

    void CodeView::onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) {
        std::vector<uint8_t> content = fileContent.getData();
        content.push_back('\0');
        content.push_back('\0');
        std::string code = (const char *) content.data();

        if (!isUtf8(code)) {
            std::basic_string<char16_t> wcode = (const char16_t *) content.data();
            GError *error = nullptr;
            const auto utf8Ptr = g_utf16_to_utf8((gunichar2 *) wcode.c_str(), wcode.size(), nullptr, nullptr, &error);
            if (error != nullptr) {
                log_console->error("Error converting UTF-16 to UTF-8: {}", error->message);
                if (utf8Ptr != nullptr) {
                    g_free(utf8Ptr);
                }
                return;
            }

            if (utf8Ptr == nullptr) {
                log_console->error("Error converting UTF-16 to UTF-8: Unknown error");
                return;
            }

            code = utf8Ptr;
            g_free(utf8Ptr);
        }

        const auto hasHighlight = io::settings::SettingsManager::instance()->getBoolean("code.highlight");

        if(hasHighlight) {
            const auto language = getLanguage(fileName);
            const auto languageManager = gtk_source_language_manager_get_default();
            auto gtkLang = gtk_source_language_manager_get_language(languageManager, language.c_str());
            if (gtkLang == nullptr) {
                log_console->warn("Language {} determined for file {} not found in GtkSourceView", language, fileName);
            }

            const auto buffer = GTK_SOURCE_BUFFER(gtk_text_view_get_buffer(GTK_TEXT_VIEW(mCodeView)));
            gtk_widget_hide(GTK_WIDGET(mCodeViewNoHighlight));
            gtk_source_buffer_set_language(buffer, gtkLang);
            gtk_source_buffer_set_highlight_syntax(buffer, gtkLang != nullptr);
            gtk_text_buffer_set_text(GTK_TEXT_BUFFER(buffer), code.c_str(), static_cast<gint>(code.size()));
        } else {
            gtk_widget_hide(GTK_WIDGET(mCodeView));
            const auto buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(mCodeViewNoHighlight));
            gtk_text_buffer_set_text(buffer, code.c_str(), static_cast<gint>(code.size()));
        }
    }

    std::string CodeView::getLanguage(const std::string &fileName) {
        const auto extension = utils::getExtension(fileName);
        if (extension == ".lua") {
            return "lua";
        } else if (extension == ".xml" || extension == ".form") {
            return "xml";
        } else {
            return "text";
        }
    }

    bool CodeView::isUtf8(const std::string &textData) {
        return g_utf8_validate(textData.c_str(), textData.size(), nullptr) == TRUE;
    }

    bool CodeView::hasHighlight() const {
        return io::settings::SettingsManager::instance()->getBoolean("code.highlight");
    }
};