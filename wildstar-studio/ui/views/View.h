
#ifndef WILDSTAR_STUDIO_VIEW_H
#define WILDSTAR_STUDIO_VIEW_H

#include <memory>
#include <string>
#include <io/archive/FileManager.h>
#include "io/utils/BinaryReader.h"
#include "gl/GlArea.h"

namespace ui::views {
    class View {
    public:
        virtual ~View() = default;

        virtual void onInitialize(GtkBuilder* builder) { }

        virtual void onDataLoaded(const io::archive::FileManagerPtr& fileManager) {}

        virtual void onFrame(const std::shared_ptr<gl::GlArea>& glArea) {}

        virtual void hidden() {}

        virtual void onOpen(const std::string &fileName, io::utils::BinaryReader &fileContent) {}

        virtual bool needsOpenGL() const = 0;

        virtual bool needsCode() const = 0;

        virtual bool hasHighlight() const {
            return false;
        }

        virtual bool hasTable() const {
            return false;
        }
    };
}


#endif //WILDSTAR_STUDIO_VIEW_H
