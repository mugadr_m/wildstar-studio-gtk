
#ifndef WILDSTAR_STUDIO_MAPAREARENDER_H
#define WILDSTAR_STUDIO_MAPAREARENDER_H

#include "io/files/AreaReader.h"
#include "gl/Buffers.h"
#include "gl/Mesh.h"
#include "MapChunkRender.h"
#include "io/textures/TextureManager.h"
#include "ui/scene/TerrainMaterial.h"

namespace ui::scene {
    class MapAreaRender {
        gl::VertexBufferPtr mVertexBuffer;
        std::vector<MapChunkRenderPtr> mChunks;

        io::textures::TextureManagerPtr mTextureManager;
    public:
        explicit MapAreaRender(io::textures::TextureManagerPtr textureManager);

        void load(const io::files::AreaReader& area);

        void onFrame(gl::MeshPtr mesh, TerrainMaterial& material);
    };

    typedef std::shared_ptr<MapAreaRender> MapAreaRenderPtr;
}


#endif //WILDSTAR_STUDIO_MAPAREARENDER_H
