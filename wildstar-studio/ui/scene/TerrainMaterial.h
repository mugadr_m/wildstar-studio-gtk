
#ifndef WILDSTAR_STUDIO_TERRAINMATERIAL_H
#define WILDSTAR_STUDIO_TERRAINMATERIAL_H

#include <vector>
#include "gl/Texture.h"
#include "gl/Mesh.h"
#include "glm/glm.hpp"

namespace ui::scene {
    class TerrainMaterial {
        int32_t mUniformTexScales;
        gl::MeshPtr mMesh;

    public:
        explicit TerrainMaterial(gl::MeshPtr mesh, int32_t uniformTexScales);
        void setTextures(const std::vector<gl::TexturePtr>& textures);
        void setTextureScales(const glm::vec4& textureScales);
    };
}


#endif //WILDSTAR_STUDIO_TERRAINMATERIAL_H
