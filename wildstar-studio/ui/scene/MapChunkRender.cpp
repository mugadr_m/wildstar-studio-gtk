
#include "MapChunkRender.h"

namespace ui::scene {

    MapChunkRender::MapChunkRender(io::textures::TextureManagerPtr textureManager) : mTextureManager(std::move(textureManager)) {

    }

    void MapChunkRender::load(const io::files::AreaChunkPtr &chunk) {
        loadColorTexture(chunk);
        loadBlendTexture(chunk);
        loadTextures(chunk);

        mStartVertex = chunk->getVertexOffset();
    }

    void MapChunkRender::loadColorTexture(const io::files::AreaChunkPtr &chunk) {
        mColorTexture = std::make_shared<gl::Texture>();

        if(chunk->hasNewColorMap()) {
            mColorTexture->loadFromMemoryDXT(gl::DxtImageFormat::BC3, 65, 65, chunk->getColorMap());
        } else if(chunk->hasOldColorMap()) {
            mColorTexture->loadFromMemoryARGB(65, 65, chunk->getColorMap());
        } else {
            mColorTexture->loadFromMemoryARGB(1, 1, { 0x7F, 0x7F, 0x7F, 0x7F });
        }

        mColorTexture->setLinearFiltering(false);
        mColorTexture->setAddressingMode(gl::AddressingMode::CLAMP);
    }

    void MapChunkRender::loadBlendTexture(const io::files::AreaChunkPtr &chunk) {
        mBlendTexture = std::make_shared<gl::Texture>();

        if(chunk->hasNewBlendMap()) {
            mBlendTexture->loadFromMemoryDXT(gl::DxtImageFormat::BC1, 65, 65, chunk->getBlendMap());
        } else if(chunk->hasOldBlendMap()) {
            mBlendTexture->loadFromMemoryARGB(65, 65, chunk->getBlendMap());
        } else {
            mBlendTexture->loadFromMemoryARGB(1, 1, { 0x00, 0x00, 0x00, 0xFF });
        }

        mBlendTexture->setLinearFiltering(false);
        mBlendTexture->setAddressingMode(gl::AddressingMode::CLAMP);
    }

    void MapChunkRender::onFrame(gl::MeshPtr mesh, TerrainMaterial& material) {
        mesh->setStartVertex(mStartVertex);

        glActiveTexture(GL_TEXTURE0);
        mColorTexture->bind();
        glActiveTexture(GL_TEXTURE1);
        mBlendTexture->bind();

        material.setTextureScales(mTextureScales);
        material.setTextures(mTextures);

        mesh->draw();
    }

    void MapChunkRender::loadTextures(const io::files::AreaChunkPtr &chunk) {
        const auto& textures = chunk->getTextureNames();
        for(const auto& texture : textures) {
            mTextures.emplace_back(mTextureManager->getTexture(texture, false));
        }

        while(mTextures.size() < 4) {
            mTextures.emplace_back(nullptr);
        }

        auto scales = chunk->getTextureScales();
        while(scales.size() < mTextures.size()) {
            scales.emplace_back(1.0f);
        }

        mTextureScales = glm::vec4(scales[0], scales[1], scales[2], scales[3]);
    }
}