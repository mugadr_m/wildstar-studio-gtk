
#ifndef WILDSTAR_STUDIO_MAPCHUNKRENDER_H
#define WILDSTAR_STUDIO_MAPCHUNKRENDER_H

#include <memory>
#include <gl/Mesh.h>
#include "gl/Texture.h"
#include "io/files/AreaReader.h"
#include "io/textures/TextureManager.h"
#include "ui/scene/TerrainMaterial.h"
#include "glm/glm.hpp"

namespace ui::scene {
    class MapChunkRender {
        gl::TexturePtr mColorTexture;
        gl::TexturePtr mBlendTexture;

        std::vector<gl::TexturePtr> mTextures;
        glm::vec4 mTextureScales;

        uint32_t mStartVertex;

        io::textures::TextureManagerPtr mTextureManager;

        void loadColorTexture(const io::files::AreaChunkPtr& chunk);
        void loadBlendTexture(const io::files::AreaChunkPtr& chunk);
        void loadTextures(const io::files::AreaChunkPtr& chunk);

    public:
        explicit MapChunkRender(io::textures::TextureManagerPtr textureManager);

        void load(const io::files::AreaChunkPtr& chunk);

        void onFrame(gl::MeshPtr mesh, TerrainMaterial& material);
    };

    typedef std::shared_ptr<MapChunkRender> MapChunkRenderPtr;
}


#endif //WILDSTAR_STUDIO_MAPCHUNKRENDER_H
