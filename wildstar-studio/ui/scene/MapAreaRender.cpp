
#include "MapAreaRender.h"

namespace ui::scene {

    MapAreaRender::MapAreaRender(io::textures::TextureManagerPtr textureManager) : mTextureManager(std::move(textureManager)) {

    }

    void MapAreaRender::load(const io::files::AreaReader &area) {
        for(const auto& chunkData : area.getChunks()) {
            const auto chunk = std::make_shared<MapChunkRender>(mTextureManager);
            chunk->load(chunkData);
            mChunks.emplace_back(chunk);
        }

        mVertexBuffer = std::make_shared<gl::VertexBuffer>();
        mVertexBuffer->setData(area.getVertices());
    }

    void MapAreaRender::onFrame(gl::MeshPtr mesh, TerrainMaterial& material) {
        mesh->setVertexBuffer(mVertexBuffer);
        mesh->startDraw();

        for(const auto& chunk : mChunks) {
            chunk->onFrame(mesh, material);
        }
    }
}