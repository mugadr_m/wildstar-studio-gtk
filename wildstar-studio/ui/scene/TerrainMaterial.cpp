
#include "TerrainMaterial.h"

namespace ui::scene {

    void TerrainMaterial::setTextures(const std::vector<gl::TexturePtr> &textures) {
        for(auto i = 0u; i < 4; ++i) {
            glActiveTexture(GL_TEXTURE2 + i);
            if(i >= textures.size()) {
                glBindTexture(GL_TEXTURE_2D, 0);
                continue;
            }

            const auto texture = textures[i];
            if(texture != nullptr) {
                texture->bind();
            } else {
                glBindTexture(GL_TEXTURE_2D, 0);
            }
        }
    }

    void TerrainMaterial::setTextureScales(const glm::vec4 &textureScales) {
        const auto program = mMesh->getProgram();
        program->setVector4(mUniformTexScales, textureScales);
    }

    TerrainMaterial::TerrainMaterial(gl::MeshPtr mesh, int32_t uniformTexScales) : mMesh(std::move(mesh)), mUniformTexScales(uniformTexScales) {

    }
}