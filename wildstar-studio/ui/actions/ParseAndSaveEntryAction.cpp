#include <utility>

#include <utility>

#include <utility>
#include <ui/dialogs/LongRunningDialog.h>

#if !defined(USE_CROSS_COMPILE) || defined(_WIN32)

#include <thread>
#include <io/archive/FileManager.h>
#include <io/settings/SettingsManager.h>
#include <io/files/TexReader.h>
#include <log.h>
#include <modules/libtex.h>
#include <utils/DxtHelper.h>
#include <io/files/DynamicTblFile.h>
#include <io/files/CsvTableWriter.h>
#include <io/files/XslxTableWriter.h>
#include <io/files/SqlTableWriter.h>

#else
#include "cross_compile/mingw.thread.h"
#endif

#include "ParseAndSaveEntryAction.h"
#include "half/half.hpp"

extern std::unique_ptr<ui::MainWindow> mainWindowPtr;

namespace ui::actions {

    ParseAndSaveEntryAction::ParseAndSaveEntryAction(io::archive::FileSystemEntryPtr rootEntry) : mRootEntry(std::move(rootEntry)) {

    }

    void ParseAndSaveEntryAction::execute() {
        if (!mRootEntry->isDirectory()) {
            saveFile(std::dynamic_pointer_cast<io::archive::FileEntry>(mRootEntry));
            return;
        }

        const auto directory = std::dynamic_pointer_cast<io::archive::DirectoryEntry>(mRootEntry);
        const auto numFiles = directory->countChildren();
        if (numFiles < 20) {
            saveDirectory(directory);
            return;
        }

        const auto dlg = std::make_shared<ui::dialogs::LongRunningDialog>(*mainWindowPtr, "Extracting & Parsing Files");

        uint64_t childrenParsed = 0;
        std::function<void()> progressCallback = [&childrenParsed, dlg, numFiles]() {
            ++childrenParsed;
            dlg->updateProgress(static_cast<float>(childrenParsed) / numFiles);
        };

        std::atomic_bool completeFlag{false};
        auto thread = std::thread([this, progressCallback, directory, dlg, &completeFlag]() {
            saveDirectoryWithProgress(directory, progressCallback, completeFlag);
            dlg->complete();
        });

        const auto result = dlg->run();
        if (result != -1) {
            completeFlag.store(true);
        }

        if (thread.joinable()) {
            thread.join();
        }
    }

    void ParseAndSaveEntryAction::saveFile(io::archive::FileEntryPtr file) {
        const auto extension = utils::toUpper(utils::getExtension(file->getName()));
        if (extension == ".TEX") {
            saveTexFile(file);
        } else if (extension == ".TBL") {
            saveTblFile(file);
        } else {
            saveFileRaw(file);
        }
    }

    void ParseAndSaveEntryAction::saveDirectory(io::archive::DirectoryEntryPtr directory) {
        for (const auto &child : directory->getChildren()) {
            if (child->isDirectory()) {
                saveDirectory(std::dynamic_pointer_cast<io::archive::DirectoryEntry>(child));
            } else {
                saveFile(std::dynamic_pointer_cast<io::archive::FileEntry>(child));
            }
        }
    }

    void ParseAndSaveEntryAction::saveDirectoryWithProgress(io::archive::DirectoryEntryPtr directory, const std::function<void()> &progressCallback, const std::atomic_bool &completeFlag) {
        for (const auto &child : directory->getChildren()) {
            if (completeFlag) {
                return;
            }

            if (child->isDirectory()) {
                saveDirectoryWithProgress(std::dynamic_pointer_cast<io::archive::DirectoryEntry>(child), progressCallback, completeFlag);
            } else {
                saveFile(std::dynamic_pointer_cast<io::archive::FileEntry>(child));
                progressCallback();
            }
        }
    }

    void ParseAndSaveEntryAction::saveTexFile(io::archive::FileEntryPtr texFile) {
        if (!io::settings::SettingsManager::instance()->getBoolean(io::settings::SettingsKeys::SAVE_TEXTURE_AS_PNG, true)) {
            return saveFileRaw(std::move(texFile));
        }

        io::utils::BinaryReader file = io::archive::FileManager::instance()->openFile(texFile);
        io::files::TexReader texReader(file);
        io::files::TexHeader header = texReader.getTexHeader();

        if (header.sides > 1 || header.depth > 1) {
            log_console->warn("Texture cannot be exported as PNG: {}. Cube texture not supported", texFile->getFullPath());
            return saveFileRaw(std::move(texFile));
        }

        auto filePath = texFile->getFullPath();
        std::replace(filePath.begin(), filePath.end(), '\\', '/');
        filePath = utils::changeExtension(filePath, "png");

        const auto &layerData = texReader.getLayerData()[0];

        using half_float::detail::half2float;
        std::vector<uint8_t> colorData(header.width * header.height * 4);
        switch (header.formatIndex) {
            case 0: {
                if (!header.isCompressed) {
                    memcpy(colorData.data(), layerData.data(), colorData.size());
                    for (auto i = 0u; i < layerData.size(); i += 4) {
                        const auto r = colorData[i];
                        const auto b = colorData[i + 2];
                        colorData[i] = b;
                        colorData[i + 2] = r;
                    }

                } else {
                    libtex::decompressImageLayer(header.textureType, layerData.data(), texReader.getLayerSizes()[0],
                                                 header.width, header.height,
                                                 header.layerInfo[0].qualityCode, header.layerInfo[0].hasDefaultValue, header.layerInfo[0].defaultValue,
                                                 header.layerInfo[1].qualityCode, header.layerInfo[1].hasDefaultValue, header.layerInfo[1].defaultValue,
                                                 header.layerInfo[2].qualityCode, header.layerInfo[2].hasDefaultValue, header.layerInfo[2].defaultValue,
                                                 header.layerInfo[3].qualityCode, header.layerInfo[3].hasDefaultValue, header.layerInfo[3].defaultValue,
                                                 colorData.data()
                    );
                }
                break;
            }

            case 11:
            case 1: {
                memcpy(colorData.data(), layerData.data(), colorData.size());
                for (auto i = 0u; i < layerData.size(); i += 4) {
                    const auto r = colorData[i];
                    const auto b = colorData[i + 2];
                    colorData[i] = b;
                    colorData[i + 2] = r;
                }
                break;
            }

            case 6: {
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = layerData[i];
                    colorData[i * 4 + 3] = 0xFF;
                }
                break;
            }

            case 7: {
                const auto *rawData = (const uint16_t *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = static_cast<uint8_t>((rawData[i] / static_cast<float>(std::numeric_limits<uint16_t>::max())) * 255.0f);
                    colorData[i * 4 + 3] = 0xFF;
                }
                break;
            }

            case 12:
            case 8: {
                const auto *rawData = (const uint16_t *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = static_cast<uint8_t>((rawData[i * 2] / static_cast<float>(std::numeric_limits<uint16_t>::max())) * 255.0f);
                    colorData[i * 4 + 1] = static_cast<uint8_t>((rawData[i * 2 + 1] / static_cast<float>(std::numeric_limits<uint16_t>::max())) * 255.0f);
                    colorData[i * 4 + 3] = 0xFF;
                }
                break;
            }

            case 9: {
                const auto *rawData = (const uint16_t *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = static_cast<uint8_t>((rawData[i * 4] / static_cast<float>(std::numeric_limits<uint16_t>::max())) * 255.0f);
                    colorData[i * 4 + 1] = static_cast<uint8_t>((rawData[i * 4 + 1] / static_cast<float>(std::numeric_limits<uint16_t>::max())) * 255.0f);
                    colorData[i * 4 + 2] = static_cast<uint8_t>((rawData[i * 4 + 2] / static_cast<float>(std::numeric_limits<uint16_t>::max())) * 255.0f);
                    colorData[i * 4 + 3] = static_cast<uint8_t>((rawData[i * 4 + 3] / static_cast<float>(std::numeric_limits<uint16_t>::max())) * 255.0f);
                }
                break;
            }

            case 10: {
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = layerData[i * 2];
                    colorData[i * 4 + 1] = layerData[i * 2 + 1];
                    colorData[i * 4 + 3] = 0xFF;
                }
                break;
            }

            case 16: {
                const auto rawData = (const uint16_t *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = static_cast<uint8_t>(std::abs(half2float<float>(rawData[i]) * 255.0f));
                    colorData[i * 4 + 3] = 0xFF;
                }
                break;
            }

            case 17: {
                const auto *rawData = (const uint16_t *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = static_cast<uint8_t>(std::abs(half2float<float>(rawData[i * 2]) * 255.0f));
                    colorData[i * 4 + 1] = static_cast<uint8_t>(std::abs(half2float<float>(rawData[i * 2 + 1]) * 255.0f));
                    colorData[i * 4 + 3] = 0xFF;
                }
                break;
            }

            case 18: {
                const auto *rawData = (const uint16_t *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = static_cast<uint8_t>(std::abs(half2float<float>(rawData[i * 4]) * 255.0f));
                    colorData[i * 4 + 1] = static_cast<uint8_t>(std::abs(half2float<float>(rawData[i * 4 + 1]) * 255.0f));
                    colorData[i * 4 + 2] = static_cast<uint8_t>(std::abs(half2float<float>(rawData[i * 4 + 2]) * 255.0f));
                    colorData[i * 4 + 3] = static_cast<uint8_t>(std::abs(half2float<float>(rawData[i * 4 + 3]) * 255.0f));
                }
                break;
            }

            case 19: {
                const auto *rawData = (const float *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = static_cast<uint8_t>(std::abs(rawData[i] * 255.0f));
                    colorData[i * 4 + 3] = 0xFF;
                }
                break;
            }

            case 20: {
                const auto *rawData = (const float *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    colorData[i * 4] = static_cast<uint8_t>(std::abs(rawData[i * 4] * 255.0f));
                    colorData[i * 4 + 1] = static_cast<uint8_t>(std::abs(rawData[i * 4 + 1] * 255.0f));
                    colorData[i * 4 + 2] = static_cast<uint8_t>(std::abs(rawData[i * 4 + 2] * 255.0f));
                    colorData[i * 4 + 3] = static_cast<uint8_t>(std::abs(rawData[i * 4 + 3] * 255.0f));
                }
                break;
            }

            case 26: {
                const auto *rawData = (const uint32_t *) layerData.data();
                for (auto i = 0u; i < header.width * header.height; ++i) {
                    const auto &value = rawData[i];
                    colorData[i * 4] = static_cast<uint8_t>(((value & 0x3FF) / 1023.0f) * 255.0f);
                    colorData[i * 4 + 1] = static_cast<uint8_t>((((value >> 10) & 0x3FF) / 1023.0f) * 255.0f);
                    colorData[i * 4 + 2] = static_cast<uint8_t>((((value >> 20) & 0x3FF) / 1023.0f) * 255.0f);
                    colorData[i * 4 + 3] = static_cast<uint8_t>((((value >> 30) & 3) / 3.0f) * 255.0f);
                }
                break;
            }

            case 13:
            case 14:
            case 15: {
                std::vector<uint32_t> colors(header.width * header.height);
                utils::DxtHelper::decompress(static_cast<utils::DxtHelper::Format>(header.formatIndex - 13), layerData, header.width, header.height, colors);
                memcpy(colorData.data(), colors.data(), colorData.size());
                break;
            }

            default: {
                log_console->warn("Texture cannot be exported as PNG: {}. Texture format {} is not supported", texFile->getFullPath(), header.formatIndex);
                return saveFileRaw(std::move(texFile));
            }
        }

        std::stringstream pathStream;
        pathStream << io::settings::SettingsManager::instance()->getString(io::settings::SettingsKeys::OUTPUT_PATH) << "/" << filePath;
        utils::createDirectories(pathStream.str());
        utils::writePng(header.width, header.height, colorData, filePath);
    }

    void ParseAndSaveEntryAction::saveFileRaw(io::archive::FileEntryPtr file) {
        const auto content = io::archive::FileManager::instance()->openFile(file);
        writeFile(file, content);
    }

    void ParseAndSaveEntryAction::writeFile(io::archive::FileEntryPtr file, io::utils::BinaryReader data) {
        writeFile(std::move(file), std::move(data), [](auto path) { return path; });
    }

    void ParseAndSaveEntryAction::saveTblFile(io::archive::FileEntryPtr tblFile) {
        const auto tblSaveFormat = io::settings::SettingsManager::instance()->getEnum(io::settings::SettingsKeys::TBL_SAVE_MODE, io::settings::TableSaveMode::TBL);

        auto tbl = std::make_shared<io::files::DynamicTblFile>(io::archive::FileManager::instance()->openFile(tblFile));
        if (tblSaveFormat == io::settings::TableSaveMode::TBL) {
            return saveFileRaw(tblFile);
        }

        switch (tblSaveFormat) {
            case io::settings::TableSaveMode::CSV: {
                io::utils::BinaryWriter writer;
                io::files::CsvTableWriter tblWriter(writer, tbl);
                const auto numRows = tbl->getNumRecords();
                const auto records = tbl->loadRecordPage(0, numRows);
                for (const auto &record : records) {
                    tblWriter.convertRow(record);
                }

                tblWriter.onClose();
                writeFile(tblFile, io::utils::BinaryReader{writer.getFullData()}, [](const std::string& fileName) {
                    return utils::changeExtension(fileName, "csv");
                });
                break;
            }

            case io::settings::TableSaveMode::XLSX: {
                io::utils::BinaryWriter writer;
                io::files::XlsxTableWriter tblWriter(writer, tbl);
                const auto numRows = tbl->getNumRecords();
                const auto records = tbl->loadRecordPage(0, numRows);
                for (const auto &record : records) {
                    tblWriter.convertRow(record);
                }

                tblWriter.onClose();
                writeFile(tblFile, io::utils::BinaryReader{writer.getFullData()}, [](const std::string& fileName) {
                    return utils::changeExtension(fileName, "xlsx");
                });
                break;
            }

            case io::settings::TableSaveMode::SQL: {
                io::utils::BinaryWriter writer;
                io::files::SqlTableWriter tblWriter(writer, tbl);
                const auto numRows = tbl->getNumRecords();
                const auto records = tbl->loadRecordPage(0, numRows);
                for (const auto &record : records) {
                    tblWriter.convertRow(record);
                }

                tblWriter.onClose();
                writeFile(tblFile, io::utils::BinaryReader{writer.getFullData()}, [](const std::string& fileName) {
                    return utils::changeExtension(fileName, "sql");
                });
                break;
            }

            default: {
                log_console->warn("Unsupported table save format {}", static_cast<uint32_t>(tblSaveFormat));
                saveFileRaw(tblFile);
                break;
            }
        }
    }

    void ParseAndSaveEntryAction::writeFile(io::archive::FileEntryPtr file, io::utils::BinaryReader data, const std::function<std::string(const std::string &)> &transform) {
        auto filePath = transform(file->getFullPath());
        std::replace(filePath.begin(), filePath.end(), '\\', '/');
        std::stringstream pathStream;
        pathStream << io::settings::SettingsManager::instance()->getString(io::settings::SettingsKeys::OUTPUT_PATH) << "/" << filePath;
        utils::createDirectories(pathStream.str());

        const auto &content = data.getData();
        const auto f = fopen(pathStream.str().c_str(), "wb");
        std::unique_ptr<FILE, utils::FileCloser> fp(f);
        fwrite(content.data(), 1, content.size(), f);
    }
}