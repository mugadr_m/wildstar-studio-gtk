
#ifndef WILDSTAR_STUDIO_PARSEANDSAVEENTRYACTION_H
#define WILDSTAR_STUDIO_PARSEANDSAVEENTRYACTION_H


#include <io/archive/IndexParser.h>
#include <atomic>
#include <io/utils/BinaryWriter.h>

namespace ui::actions {
    class ParseAndSaveEntryAction {
        io::archive::FileSystemEntryPtr mRootEntry;

        void writeFile(io::archive::FileEntryPtr file, io::utils::BinaryReader data);
        void writeFile(io::archive::FileEntryPtr file, io::utils::BinaryReader data, const std::function<std::string(const std::string&)>& transform);

        void saveFile(io::archive::FileEntryPtr file);
        void saveDirectory(io::archive::DirectoryEntryPtr directory);
        void saveDirectoryWithProgress(io::archive::DirectoryEntryPtr directory, const std::function<void()>& progressCallback, const std::atomic_bool& completeFlag);

        void saveTexFile(io::archive::FileEntryPtr texFile);
        void saveTblFile(io::archive::FileEntryPtr tblFile);

        void saveFileRaw(io::archive::FileEntryPtr file);

    public:
        explicit ParseAndSaveEntryAction(io::archive::FileSystemEntryPtr rootEntry);

        void execute();
    };
}

#endif //WILDSTAR_STUDIO_PARSEANDSAVEENTRYACTION_H
