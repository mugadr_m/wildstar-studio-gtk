
#ifndef WILDSTAR_STUDIO_MAINWINDOW_H
#define WILDSTAR_STUDIO_MAINWINDOW_H


#include <gtk/gtk.h>
#include <string>
#include <functional>
#include <memory>
#include <io/archive/IndexParser.h>
#include <atomic>
#include <ui/views/AreaView.h>
#include <ui/views/TableView.h>
#include "gl/GlArea.h"
#include "ui/views/CodeView.h"
#include "ui/views/TextureView.h"
#include "ui/views/View.h"
#include "io/archive/FileManager.h"
#include "ui/dialogs/SettingsDialog.h"

namespace ui {
    class MainWindow {
        GtkWindow *mWindow;
        GObject *mWindowObject;
        GtkBuilder *mBuilder;

        GtkProgressBar *mLoadProgressBar;
        GtkTreeView *mFileTreeView;

        GtkButton *mSaveFileButton;
        GtkButton *mParseAndSaveFileButton;

        GtkWidget *mGlControl;
        GtkWidget *mCodeViewParentControl;
        GtkWidget *mCodeViewNoHighlightParentControl;
        GtkWidget *mTableWidget;
        GtkWidget *mTableWidgetParent;

        GtkLabel *mStatusLabel;

        std::shared_ptr<gl::GlArea> mGlArea;
        std::shared_ptr<views::CodeView> mCodeView;
        std::shared_ptr<views::TextureView> mTextureView;
        std::shared_ptr<views::AreaView> mAreaView;
        std::shared_ptr<views::TableView> mTblView;

        std::shared_ptr<io::archive::FileSystemEntry> mSelectedEntry;

        std::shared_ptr<views::View> mActiveView;

        std::shared_ptr<dialogs::SettingsDialog> mSettingsDialog;

        GtkButton *getButton(const std::string &name);

        GtkLabel *getLabel(const std::string &name);

        GtkWidget *getWidget(const std::string &name);

        GtkProgressBar *getProgressBar(const std::string &name);

        GtkTreeView *getTreeView(const std::string &name);

        void initFromBuilder();

        void initEventHandlers();

        void hideLoadButtons();

        void showLoadingIndicator();

        void onLoadFinished(bool success, GtkTreeStore *treeStore);

        void onLoadMessage(const std::string &message);

        void onLoadProgress(float percentage);

        void onLoadLocalButtonClicked();

        void onFileTreeViewChanged();

        void onFileOpen(const io::archive::FileSystemEntryPtr &file);

        void onGlFrame();

        std::shared_ptr<views::View> getViewForFile(const std::string &fileName) const;

        void setTopLevelFileButtonsVisible(bool visible);

        void onSaveEntry();

        void onParseAndSaveEntry();

        void saveFile(const io::archive::FileSystemEntryPtr &file, const std::string &basePath);

        void saveDirectory(const io::archive::FileSystemEntryPtr &dir, const std::string &basePath, const std::function<void()> &progressFunction, std::atomic_bool &completeFlag);

        bool fileExists(const std::string &fileName) const;

        std::string getUiTemplateFile() const;

    public:
        MainWindow();

        ~MainWindow();

        GtkWindow *getWindow() const {
            return mWindow;
        }

        GObject *getWindowObject() const {
            return mWindowObject;
        }

        GtkBuilder *getBuilder() const {
            return mBuilder;
        }

        void runLoop();
    };
};

#endif //WILDSTAR_STUDIO_MAINWINDOW_H
