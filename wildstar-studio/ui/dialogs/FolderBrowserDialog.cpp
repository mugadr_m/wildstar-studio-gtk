#include "FolderBrowserDialog.h"
#include "ui/MainWindow.h"
#include <stdexcept>
#include <iostream>

namespace ui::dialogs {
    FolderBrowserDialog::FolderBrowserDialog(const MainWindow &parent, const std::string &caption) {
        mWidget = gtk_file_chooser_dialog_new(caption.c_str(), parent.getWindow(),
                                              GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                              "Open", GTK_RESPONSE_ACCEPT, "Cancel", GTK_RESPONSE_CANCEL, nullptr);
    }

    FolderBrowserDialog::~FolderBrowserDialog() {
        if (mWidget != nullptr) {
            gtk_widget_destroy(mWidget);
        }
    }

    bool FolderBrowserDialog::run(std::string &filePath) {
        if (mWidget == nullptr) {
            throw ::utils::RuntimeError("Cannot show dialog multiple times");
        }

        const auto result = gtk_dialog_run(GTK_DIALOG(mWidget));
        auto retVal = false;
        if (result == GTK_RESPONSE_ACCEPT) {
            const auto fileName = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(mWidget));
            if (fileName != nullptr) {
                filePath = fileName;
                g_free(fileName);
                retVal = true;
            }
        }

        gtk_widget_destroy(mWidget);
        mWidget = nullptr;
        return retVal;
    }
};
