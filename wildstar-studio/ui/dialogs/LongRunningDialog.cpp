#include "ui/MainWindow.h"
#include "LongRunningDialog.h"

namespace ui::dialogs {
    LongRunningDialog::LongRunningDialog(const ui::MainWindow &mainWindow, const std::string& title) : mParent(mainWindow.getWindow()) {
        mDialog = GTK_DIALOG(gtk_builder_get_object(mainWindow.getBuilder(), "longRunDialog"));
        mProgressBar = GTK_WIDGET(gtk_builder_get_object(mainWindow.getBuilder(), "longRunningProgressBar"));

        gtk_window_set_title(GTK_WINDOW(mDialog), title.c_str());
    }

    int32_t LongRunningDialog::run() {
        gtk_window_set_transient_for(GTK_WINDOW(mDialog), mParent);
        const auto ret = gtk_dialog_run(mDialog);
        gtk_widget_hide(GTK_WIDGET(mDialog));

        return ret;
    }

    void LongRunningDialog::updateProgress(float progress) {
        gtk_progress_bar_set_fraction(GTK_PROGRESS_BAR(mProgressBar), progress);
    }

    void LongRunningDialog::complete() {
        gtk_dialog_response(mDialog, -1);
    }
}