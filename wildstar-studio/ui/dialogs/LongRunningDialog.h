
#ifndef WILDSTAR_STUDIO_LONGRUNNINGDIALOG_H
#define WILDSTAR_STUDIO_LONGRUNNINGDIALOG_H

#include "gtk/gtk.h"
#include <memory>

namespace ui {
    class MainWindow;
}

namespace ui::dialogs {

    class LongRunningDialog {
        GtkWindow* mParent;
        GtkDialog* mDialog;
        GtkWidget* mProgressBar;

    public:
        explicit LongRunningDialog(const ui::MainWindow& mainWindow, const std::string& title = "");

        int32_t run();

        void updateProgress(float progress);

        void complete();
    };
}


#endif //WILDSTAR_STUDIO_LONGRUNNINGDIALOG_H
