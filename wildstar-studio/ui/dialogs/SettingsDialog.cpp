
#include <io/settings/SettingsManager.h>
#include "SettingsDialog.h"
#include "ui/MainWindow.h"
#include "log.h"

namespace ui::dialogs {

    SettingsDialog::SettingsDialog(const ui::MainWindow &mainWindow) : mParent(mainWindow.getWindow()) {
        const auto builder = mainWindow.getBuilder();
        mDialog = GTK_DIALOG(gtk_builder_get_object(builder, "settingsDialog"));

        loadElementsFromBuilder(builder);
        loadSettings();
    }

    void SettingsDialog::run() {
        gtk_window_set_transient_for(GTK_WINDOW(mDialog), mParent);
        const auto ret = gtk_dialog_run(mDialog);
        gtk_widget_hide(GTK_WIDGET(mDialog));

        if(ret != 0) {
            return;
        }

        saveSettings();
    }

    void SettingsDialog::loadElementsFromBuilder(GtkBuilder *builder) {
        mOutputPathChooser = GTK_FILE_CHOOSER(gtk_builder_get_object(builder, "outputPathChooser"));
        mCodeCheckBox = GTK_CHECK_BUTTON(gtk_builder_get_object(builder, "highlightCodeCheckbox"));
        mSaveTblAsTbl = GTK_RADIO_BUTTON(gtk_builder_get_object(builder, "saveTblAsTbl"));
        mSaveTblAsCsv = GTK_RADIO_BUTTON(gtk_builder_get_object(builder, "saveTblAsCsv"));
        mSaveTblAsXlsx = GTK_RADIO_BUTTON(gtk_builder_get_object(builder, "saveTblAsXlsx"));
        mSaveTblAsSql = GTK_RADIO_BUTTON(gtk_builder_get_object(builder, "saveTblAsSql"));
    }

    void SettingsDialog::saveSettings() {
        const auto mgr = io::settings::SettingsManager::instance();

        const auto outputPath = std::string(gtk_file_chooser_get_filename(mOutputPathChooser));
        const auto highlightCode = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(mCodeCheckBox)) != FALSE;

        mgr->setString(io::settings::SettingsKeys::OUTPUT_PATH, outputPath);
        mgr->setBoolean(io::settings::SettingsKeys::HIGHLIGHT_CODE, highlightCode);

        if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(mSaveTblAsTbl)) != FALSE) {
            mgr->setEnum(io::settings::SettingsKeys::TBL_SAVE_MODE, io::settings::TableSaveMode::TBL);
        }

        if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(mSaveTblAsCsv)) != FALSE) {
            mgr->setEnum(io::settings::SettingsKeys::TBL_SAVE_MODE, io::settings::TableSaveMode::CSV);
        }

        if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(mSaveTblAsXlsx)) != FALSE) {
            mgr->setEnum(io::settings::SettingsKeys::TBL_SAVE_MODE, io::settings::TableSaveMode::XLSX);
        }

        if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(mSaveTblAsSql)) != FALSE) {
            mgr->setEnum(io::settings::SettingsKeys::TBL_SAVE_MODE, io::settings::TableSaveMode::SQL);
        }

        mgr->saveAll();
    }

    void SettingsDialog::loadSettings() {
        const auto mgr = io::settings::SettingsManager::instance();

        const auto outputPath = mgr->getString(io::settings::SettingsKeys::OUTPUT_PATH);
        const auto highlightCode = mgr->getBoolean(io::settings::SettingsKeys::HIGHLIGHT_CODE);
        const auto tblSaveMode = mgr->getEnum(io::settings::SettingsKeys::TBL_SAVE_MODE, io::settings::TableSaveMode::TBL);

        gtk_file_chooser_set_filename(mOutputPathChooser, outputPath.c_str());
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mCodeCheckBox), highlightCode ? TRUE : FALSE);

        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mSaveTblAsTbl), tblSaveMode == io::settings::TableSaveMode::TBL);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mSaveTblAsCsv), tblSaveMode == io::settings::TableSaveMode::CSV);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mSaveTblAsXlsx), tblSaveMode == io::settings::TableSaveMode::XLSX);
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(mSaveTblAsSql), tblSaveMode == io::settings::TableSaveMode::SQL);
    }
}