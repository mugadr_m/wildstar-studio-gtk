#ifndef WILDSTAR_STUDIO_FOLDERBROWSERDIALOG_H
#define WILDSTAR_STUDIO_FOLDERBROWSERDIALOG_H


#include <gtk/gtk.h>
#include <string>

namespace ui {
    class MainWindow;
}

namespace ui::dialogs {

    class FolderBrowserDialog {
        GtkWidget *mWidget;

    public:
        explicit FolderBrowserDialog(const MainWindow &parent, const std::string &caption);

        ~FolderBrowserDialog();

        bool run(std::string &filePath);
    };
};


#endif //WILDSTAR_STUDIO_FOLDERBROWSERDIALOG_H
