
#ifndef WILDSTAR_STUDIO_SETTINGSDIALOG_H
#define WILDSTAR_STUDIO_SETTINGSDIALOG_H

#include <gtk/gtk.h>

namespace ui {
    class MainWindow;
}

namespace ui::dialogs {
    class SettingsDialog {
        GtkDialog* mDialog;
        GtkWindow* mParent;
        GtkFileChooser* mOutputPathChooser;
        GtkCheckButton* mCodeCheckBox;

        GtkRadioButton* mSaveTblAsTbl;
        GtkRadioButton* mSaveTblAsCsv;
        GtkRadioButton* mSaveTblAsXlsx;
        GtkRadioButton* mSaveTblAsSql;

        void loadSettings();
        void saveSettings();

        void loadElementsFromBuilder(GtkBuilder* builder);
    public:
        explicit SettingsDialog(const ui::MainWindow& mainWindow);

        void run();
    };
}

#endif //WILDSTAR_STUDIO_SETTINGSDIALOG_H
