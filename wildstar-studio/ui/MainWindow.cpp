#include "MainWindow.h"
#include "ui/dialogs/FolderBrowserDialog.h"
#include "io/archive/FileManager.h"
#include <stdexcept>
#include <memory>
#include <iostream>
#include <io/archive/IndexParser.h>
#include <utils/Utilities.h>
#include <revision.hpp>
#include "log.h"
#include "ui/dialogs/LongRunningDialog.h"
#include <fstream>
#include <io/settings/SettingsManager.h>
#include <ui/actions/ParseAndSaveEntryAction.h>

namespace ui {
    MainWindow::MainWindow() : mWindowObject(nullptr), mWindow(nullptr), mBuilder(nullptr) {
        mBuilder = gtk_builder_new();
        const auto uiFile = getUiTemplateFile();
        if (uiFile.empty()) {
            log_console->error("Unable to find UI template in glade/ws-studio-main-window.glade or bin/glade/ws-studio-main-window.glade");
            throw ::utils::RuntimeError("UI template not found");
        }

        GError *loadError = nullptr;
        if (gtk_builder_add_from_file(mBuilder, uiFile.c_str(), &loadError) <= 0) {
            log_console->error("Error loading UI template: {}", loadError->message);
            throw ::utils::RuntimeError("Invalid UI template");
        }

        initFromBuilder();
    }

    MainWindow::~MainWindow() {
        mWindow = nullptr;

        if (mWindowObject != nullptr) {
            g_object_unref(mWindowObject);
            mWindowObject = nullptr;
        }
    }

    void MainWindow::initFromBuilder() {
        const auto windowObject = gtk_builder_get_object(mBuilder, "mainWindow");
        if (windowObject == nullptr) {
            log_console->error("Window object named 'mainWindow' not found in UI template");
            throw ::utils::RuntimeError("Window object named 'mainWindow' not found in template");
        }

        if (!GTK_IS_WINDOW(windowObject)) {
            log_console->error("Object named 'mainWindow' in UI template is not a GtkWindow");
            throw ::utils::RuntimeError("Object named 'mainWindow' in template is not a GtkWindow");
        }

        mWindow = GTK_WINDOW(windowObject);
        mWindowObject = windowObject;

        std::stringstream nameStream;
        nameStream << "Wildstar Studio - " << GIT_VERSION_NAME << " @ " << GIT_VERSION_HASH;
        gtk_window_set_title(mWindow, nameStream.str().c_str());

        gtk_window_set_icon_name(mWindow, "applications-development");

        mLoadProgressBar = getProgressBar("loadStatusProgressBar");
        mFileTreeView = getTreeView("fileTreeView");

        mGlControl = getWidget("glRenderArea");
        mCodeViewParentControl = getWidget("codeViewParent");
        mCodeViewNoHighlightParentControl = getWidget("codeViewParentNoHighlight");

        mStatusLabel = getLabel("statusLabel");

        mSettingsDialog = std::make_shared<dialogs::SettingsDialog>(*this);

        mGlArea = std::make_shared<gl::GlArea>(mGlControl);
        mGlArea->setOnFrameCallback(std::bind(&MainWindow::onGlFrame, this));

        mTableWidget = getWidget("tableViewControl");
        mTableWidgetParent = getWidget("tableViewParent");

        const auto aboutDialog = GTK_ABOUT_DIALOG(getWidget("aboutDialog"));
        gtk_about_dialog_set_version(aboutDialog, (GIT_VERSION_HASH + " @ " + GIT_VERSION_NAME).c_str());
        std::string aboutComments = gtk_about_dialog_get_comments(aboutDialog);
        std::stringstream aboutVersion;
        aboutVersion << aboutComments << std::endl;
        aboutVersion << std::endl;
        aboutVersion << "Built on:" << std::endl << CMAKE_OS_NAME;
        gtk_about_dialog_set_comments(aboutDialog, aboutVersion.str().c_str());

        const auto nameRenderer = gtk_cell_renderer_text_new();
        const auto iconRenderer = gtk_cell_renderer_pixbuf_new();
        const auto fileColumn = gtk_tree_view_column_new();
        gtk_tree_view_column_pack_start(fileColumn, iconRenderer, false);
        gtk_tree_view_column_pack_end(fileColumn, nameRenderer, false);
        gtk_tree_view_column_add_attribute(fileColumn, iconRenderer, "icon-name", 0);
        gtk_tree_view_column_add_attribute(fileColumn, nameRenderer, "text", 1);
        gtk_tree_view_append_column(mFileTreeView, fileColumn);

        mCodeView = std::make_shared<views::CodeView>(getWidget("codeView"), getWidget("codeViewNoHighlight"));
        mTextureView = std::make_shared<views::TextureView>(mGlArea);
        mAreaView = std::make_shared<views::AreaView>();
        mTblView = std::make_shared<views::TableView>(mTableWidget);

        mGlArea->setOnLoadCallback([this]() {
            mCodeView->onInitialize(mBuilder);
            mTextureView->onInitialize(mBuilder);
            mAreaView->onInitialize(mBuilder);
            mTblView->onInitialize(mBuilder);
        });

        mSaveFileButton = getButton("saveFileButton");
        mParseAndSaveFileButton = getButton("parseAndSaveFileButton");

        initEventHandlers();
    }

    void MainWindow::runLoop() {
        gtk_window_present(mWindow);
        g_signal_connect_swapped(mWindowObject, "destroy", gtk_main_quit, nullptr);

        gtk_main();
    }

    void MainWindow::initEventHandlers() {
        const auto loadLocalButton = getButton("loadLocalButton");
        utils::registerEvent(loadLocalButton, "clicked", [this]() { onLoadLocalButtonClicked(); });

        utils::registerEvent(gtk_tree_view_get_selection(mFileTreeView), "changed", [this]() { onFileTreeViewChanged(); });

        utils::registerEvent(mSaveFileButton, "clicked", [this]() { onSaveEntry(); });
        utils::registerEvent(mParseAndSaveFileButton, "clicked", [this]() { onParseAndSaveEntry(); });

        utils::registerEvent(getButton("aboutDialogButton"), "clicked", [this]() {
            const auto dialog = GTK_DIALOG(getWidget("aboutDialog"));
            gtk_window_set_transient_for(GTK_WINDOW(dialog), mWindow);
            gtk_dialog_run(dialog);
            gtk_widget_hide(GTK_WIDGET(dialog));
        });

        utils::registerEvent(getButton("settingsDialogButton"), "clicked", [this]() {
            mSettingsDialog->run();
        });
    }

    GtkButton *MainWindow::getButton(const std::string &name) {
        const auto widgetObject = gtk_builder_get_object(mBuilder, name.c_str());
        if (widgetObject == nullptr) {
            log_console->error("Widget not found: {}", name);
            throw ::utils::RuntimeError("Widget not found");
        }

        if (!GTK_IS_BUTTON(widgetObject)) {
            log_console->error("Widget {} is not a button", name);
            throw ::utils::RuntimeError("Widget is not a button");
        }

        return GTK_BUTTON(widgetObject);
    }

    void MainWindow::onLoadLocalButtonClicked() {
        ui::dialogs::FolderBrowserDialog folderDialog(*this, "Select Folder of ClientData.index");
        std::string selectedFolder;
        const auto isSelected = folderDialog.run(selectedFolder);
        if (!isSelected) {
            return;
        }

        hideLoadButtons();
        showLoadingIndicator();

        io::archive::FileManager::instance()->initFromLocal(selectedFolder,
                                                            std::bind(&MainWindow::onLoadMessage, this, std::placeholders::_1),
                                                            std::bind(&MainWindow::onLoadProgress, this, std::placeholders::_1),
                                                            std::bind(&MainWindow::onLoadFinished, this, std::placeholders::_1,
                                                                      std::placeholders::_2)
        );
    }

    void MainWindow::hideLoadButtons() {
        const auto loadLocalButton = getButton("loadLocalButton");
        const auto loadCdnButton = getButton("loadCDNButton");
        gtk_widget_hide(GTK_WIDGET(loadLocalButton));
        gtk_widget_hide(GTK_WIDGET(loadCdnButton));
    }

    void MainWindow::showLoadingIndicator() {
        gtk_progress_bar_set_text(mLoadProgressBar, "Loading Files");
        gtk_widget_show(GTK_WIDGET(mLoadProgressBar));
    }

    GtkWidget *MainWindow::getWidget(const std::string &name) {
        const auto widgetObject = gtk_builder_get_object(mBuilder, name.c_str());
        if (widgetObject == nullptr) {
            log_console->error("Widget not found: {}", name);
            throw ::utils::RuntimeError("Widget not found");
        }

        if (!GTK_IS_WIDGET(widgetObject)) {
            log_console->error("Object {} is not a widget", name);
            throw ::utils::RuntimeError("Object is not a widget");
        }

        return GTK_WIDGET(widgetObject);
    }

    void MainWindow::onLoadFinished(bool success, GtkTreeStore *store) {
        if (!success) {
            utils::runInMainThread([this]() {
                gtk_widget_hide(GTK_WIDGET(mLoadProgressBar));
                const auto loadLocalButton = getButton("loadLocalButton");
                const auto loadCdnButton = getButton("loadCDNButton");
                gtk_widget_show(GTK_WIDGET(loadLocalButton));
                gtk_widget_show(GTK_WIDGET(loadCdnButton));
            });
            return;
        }

        const auto fileManager = io::archive::FileManager::instance();

        mCodeView->onDataLoaded(fileManager);
        mTextureView->onDataLoaded(fileManager);
        mAreaView->onDataLoaded(fileManager);

        utils::runInMainThread([this, store]() {
            gtk_tree_view_set_model(mFileTreeView, GTK_TREE_MODEL(store));
            gtk_widget_hide(GTK_WIDGET(mLoadProgressBar));
            gtk_widget_show(gtk_widget_get_parent(GTK_WIDGET(mFileTreeView)));
        });
    }

    void MainWindow::onLoadMessage(const std::string &message) {
        utils::runInMainThread([this, message]() {
            gtk_progress_bar_set_text(mLoadProgressBar, message.c_str());
        });
    }

    GtkProgressBar *MainWindow::getProgressBar(const std::string &name) {
        const auto widgetObject = gtk_builder_get_object(mBuilder, name.c_str());
        if (widgetObject == nullptr) {
            log_console->error("Widget not found: {}", name);
            throw ::utils::RuntimeError("Widget not found");
        }

        if (!GTK_IS_PROGRESS_BAR(widgetObject)) {
            log_console->error("Widget {} is not a progress bar", name);
            throw ::utils::RuntimeError("Widget is not a progress bar");
        }

        return GTK_PROGRESS_BAR(widgetObject);
    }

    void MainWindow::onLoadProgress(float percentage) {
        percentage = std::min(std::max(0.0f, percentage), 1.0f);
        utils::runInMainThread([this, percentage]() {
            gtk_progress_bar_set_fraction(mLoadProgressBar, percentage);
        });
    }

    GtkTreeView *MainWindow::getTreeView(const std::string &name) {
        const auto widgetObject = gtk_builder_get_object(mBuilder, name.c_str());
        if (widgetObject == nullptr) {
            log_console->error("Widget not found: {}", name);
            throw ::utils::RuntimeError("Widget not found");
        }

        if (!GTK_IS_TREE_VIEW(widgetObject)) {
            log_console->error("Widget {} is not a tree view", name);
            throw ::utils::RuntimeError("Widget is not a tree view");
        }

        return GTK_TREE_VIEW(widgetObject);
    }

    void MainWindow::onFileTreeViewChanged() {
        mSelectedEntry = nullptr;

        const auto selection = gtk_tree_view_get_selection(mFileTreeView);
        const auto numSelected = gtk_tree_selection_count_selected_rows(selection);
        if (numSelected <= 0) {
            setTopLevelFileButtonsVisible(false);
            return;
        }

        GtkTreeModel *treeModel = nullptr;
        GtkTreeIter iter;
        const auto hasSelected = gtk_tree_selection_get_selected(selection, &treeModel, &iter);
        if (hasSelected == FALSE) {
            setTopLevelFileButtonsVisible(false);
            return;
        }

        setTopLevelFileButtonsVisible(true);
        gpointer elementPointer = nullptr;
        gtk_tree_model_get(treeModel, &iter, 2, &elementPointer, -1);
        auto fileSystemEntry = reinterpret_cast<io::archive::FileSystemEntry *>(elementPointer);
        mSelectedEntry = fileSystemEntry->shared_from_this();
        if (fileSystemEntry->isDirectory()) {
            return;
        }

        log_console->info("Selected element: {}", fileSystemEntry->getFullPath());
        try {
            onFileOpen(fileSystemEntry->shared_from_this());
        } catch (std::exception &ex) {
            log_console->warn("error opening file: {}", std::string(ex.what()));
        }
    }

    void MainWindow::onFileOpen(const io::archive::FileSystemEntryPtr &file) {
        auto filePtr = std::dynamic_pointer_cast<io::archive::FileEntry>(file);
        auto fileBytes = io::archive::FileManager::instance()->openFile(filePtr);
        if (mActiveView != nullptr) {
            mActiveView->hidden();
        }

        const auto fileView = getViewForFile(file->getName());
        if (fileView == nullptr) {
            mActiveView = fileView;
            return;
        }

        fileView->onOpen(file->getFullPath(), fileBytes);
        mActiveView = fileView;

        if (mActiveView->needsOpenGL()) {
            gtk_widget_show(mGlControl);
            utils::runInMainThread([this]() { gtk_widget_grab_focus(mGlControl); });
            mGlArea->setVisible(true);
        } else {
            gtk_widget_hide(mGlControl);
            mGlArea->setVisible(false);
        }

        if (mActiveView->needsCode()) {
            if (mActiveView->hasHighlight()) {
                gtk_widget_show(mCodeViewParentControl);
            } else {
                gtk_widget_show(mCodeViewNoHighlightParentControl);
            }
        } else {
            gtk_widget_hide(mCodeViewParentControl);
            gtk_widget_hide(mCodeViewNoHighlightParentControl);
        }

        if (mActiveView->hasTable()) {
            gtk_widget_show(mTableWidgetParent);
        } else {
            gtk_widget_hide(mTableWidgetParent);
        }
    }

    std::shared_ptr<views::View> MainWindow::getViewForFile(const std::string &fileName) const {
        static std::map<std::string, std::shared_ptr<views::View>> viewMap;
        if (viewMap.empty()) {
            viewMap[".lua"] = mCodeView;
            viewMap[".form"] = mCodeView;
            viewMap[".xml"] = mCodeView;
            viewMap[".tex"] = mTextureView;
            viewMap[".area"] = mAreaView;
            viewMap[".tbl"] = mTblView;
        }

        const auto extension = utils::getExtension(fileName);
        const auto itr = viewMap.find(extension);
        if (itr == viewMap.end()) {
            log_console->warn("No view defined for file extension {}", extension);
            return nullptr;
        }

        return itr->second;
    }

    void MainWindow::onGlFrame() {
        if (mActiveView != nullptr) {
            mActiveView->onFrame(mGlArea);
        }
    }

    void MainWindow::setTopLevelFileButtonsVisible(bool visible) {
        gtk_widget_set_sensitive(GTK_WIDGET(mSaveFileButton), visible ? TRUE : FALSE);
        gtk_widget_set_sensitive(GTK_WIDGET(mParseAndSaveFileButton), visible ? TRUE : FALSE);
    }

    void MainWindow::onSaveEntry() {
        if (mSelectedEntry == nullptr) {
            return;
        }

        std::string outPath = io::settings::SettingsManager::instance()->getString(io::settings::SettingsKeys::OUTPUT_PATH);

        if (mSelectedEntry->isDirectory()) {
            std::shared_ptr<dialogs::LongRunningDialog> dlg = std::make_shared<dialogs::LongRunningDialog>(*this, "Extracting Files");
            const auto numChildren = std::dynamic_pointer_cast<io::archive::DirectoryEntry>(mSelectedEntry)->countChildren();
            uint32_t parsedChildren = 0;
            const auto entry = mSelectedEntry;

            std::function<void()> onProgress = [this, numChildren, &parsedChildren, dlg]() {
                const auto progress = std::min(std::max(((float) ++parsedChildren) / numChildren, 0.0f), 1.0f);
                utils::runInMainThread([dlg, progress]() {
                    dlg->updateProgress(progress);
                });
            };

            std::atomic_bool completeFlag;
            auto thread = std::thread([this, entry, outPath, dlg, numChildren, onProgress, &completeFlag]() {
                saveDirectory(entry, outPath, onProgress, completeFlag);
                dlg->complete();
            });

            const auto response = dlg->run();
            if (response != -1) {
                completeFlag.store(true);
            }

            if (thread.joinable()) {
                thread.join();
            }

            utils::runInMainThread([this]() { gtk_label_set_text(mStatusLabel, ""); });
        } else {
            saveFile(mSelectedEntry, outPath);
        }
    }

    void MainWindow::saveFile(const io::archive::FileSystemEntryPtr &file, const std::string &basePath) {
        auto filePath = file->getFullPath();
        std::replace(filePath.begin(), filePath.end(), '\\', '/');
        std::stringstream pathStream;
        pathStream << basePath << "/" << filePath;
        utils::createDirectories(pathStream.str());

        const auto content = io::archive::FileManager::instance()->openFile(std::dynamic_pointer_cast<io::archive::FileEntry>(file));
        const auto f = fopen(pathStream.str().c_str(), "wb");
        std::unique_ptr<FILE, utils::FileCloser> fp(f);
        fwrite(content.getData().data(), 1, content.getData().size(), f);
    }

    void MainWindow::onParseAndSaveEntry() {
        if (mSelectedEntry == nullptr) {
            return;
        }

        actions::ParseAndSaveEntryAction action(mSelectedEntry);
        action.execute();
    }

    void MainWindow::saveDirectory(const io::archive::FileSystemEntryPtr &dir, const std::string &basePath, const std::function<void()> &progressFunction, std::atomic_bool &completeFlag) {
        const auto directory = std::dynamic_pointer_cast<io::archive::DirectoryEntry>(dir);

        for (const auto &entry : directory->getChildren()) {
            if (completeFlag) {
                return;
            }

            if (entry->isDirectory()) {
                saveDirectory(entry, basePath, progressFunction, completeFlag);
            } else {
                utils::runInMainThread([this, entry]() {
                    std::stringstream stream;
                    stream << "Processing: " << entry->getFullPath();
                    gtk_label_set_text(mStatusLabel, stream.str().c_str());
                });
                saveFile(entry, basePath);
                progressFunction();
            }
        }
    }

    std::string MainWindow::getUiTemplateFile() const {
        static const std::string locations[]{
                "./glade/ws-studio-main-window.glade",
                "./bin/glade/ws-studio-main-window.glade"
        };

        for (const std::string &file : locations) {
            if (fileExists(file)) {
                return file;
            }
        }

        return {};
    }

    bool MainWindow::fileExists(const std::string &fileName) const {
        return std::unique_ptr<FILE, utils::FileCloser>(fopen(fileName.c_str(), "r")) != nullptr;
    }

    GtkLabel *MainWindow::getLabel(const std::string &name) {
        const auto widget = getWidget(name);
        if (GTK_IS_LABEL(widget)) {
            return GTK_LABEL(widget);
        }

        log_console->warn("Widget is not a label: {}", name);
        throw ::utils::RuntimeError("Widget is not a label");
    }
};
