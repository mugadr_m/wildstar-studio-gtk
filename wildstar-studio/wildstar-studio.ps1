Add-Type -AssemblyName System.IO.Compression.FileSystem
$BASE_URL = "https://bitbucket.org/mugadr_m/wildstar-studio-gtk/downloads"

function check_for_updates {
    Write-Host "Getting route to default gateway..."
    $gateway = Get-NetRoute -DestinationPrefix '0.0.0.0/0' | Select-Object -ExpandProperty 'NextHop'
    if (!$gateway) {
        Write-Host "No default getway found, wont check for updates"
        return 0
    }

    $connection = Test-Connection $gateway -Count 1 -Quiet
    if (-Not$connection -eq 'True') {
        Write-Host "Unable to reach default gateway, wont check of updates"
        return 0
    }

    $version_url = "$BASE_URL/version_windows.txt"
    Write-Host "Getting latest version from $version_url"
    $result = (New-Object Net.WebClient).DownloadString($version_url)

    if (!$result) {
        Write-Host "Error getting remote version"
        return 0
    }

    $script:target_version = $result.Trim()
    Write-Host "Remote version: $target_version"

    if (!(Test-Path .\wildstar_studio.exe)) {
        Write-Host "No current version found, will update"
        return 1
    }

    $current_version = .\wildstar_studio.exe --version
    if (!$current_version) {
        Write-Host "Error getting version from wildstar_studio.exe, redownloading"
        return 1
    }

    $current_version = $current_version.Trim()
    if ($current_version = = $target_version) {
        return 0
    }
    else {
        return 1
    }
    return !($current_version = = $target_version)
}

function download_version() {
    $zip_name = "wildstar_studio_windows_x64_$target_version.zip"
    $download_url = "$BASE_URL/$zip_name"
    Write-Host "Downloading version $target_version from $download_url"
    try {
        (New-Object Net.WebClient).DownloadFile($download_url, ".\$zip_name")
    }
    catch [Exception] {
        Write-Host $_.Exception.InnerException
        Write-Host "Download failed, abort update: ${_.Exception.Message}"
        return
    }

    Write-Host "Extracting $zip_name"
    $zip = [System.IO.Compression.ZipFile]::OpenRead($zip_name)
    try {
        $entries = $zip.Entries
        $entries | ForEach-Object {
            if ( [System.IO.File]::Exists(".\bin\$_")) {
                [System.IO.File]::Delete(".\bin\$_")
            }
        }
    }
    finally {
        $zip.Dispose()
    }

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zip_name, ".\bin")
    [System.IO.File]::Delete(".\$zip_name")
}

function check_for_runtime_env() {
    # if you are reading this: These are just some random files from all folders
    # that are checked, but many more need to be present, this list is not exhaustive
    $files = @(
    "bin\libcairo-gobject-2.dll",
    "bin\libffi-6.dll",
    "bin\libgio-2.0-0.dll",
    "bin\libiconv-2.dll",
    "bin\libpango-1.0-0.dll",
    "bin\libpcre-1.dll",
    "bin\zlib1.dll",
    "lib\gdk-pixbuf-2.0\2.10.0\loaders.cache",
    "share\glib-2.0\schemas\\gschema.dtd",
    "share\gtk-3.0\gtkbuilder.rng",
    "share\gtksourceview-3.0\language-specs\lua.lang",
    "share\icons\\Adwaita\8x8\emblems\emblem-default.png",
    "share\mime\application\gzip.xml"
    )

    $need_download = $false
    foreach ($value in $files) {
        if (!([System.IO.File]::Exists($value))) {
            Write-Host "Essential file $value not found, downloading runtime environment"
            $need_download = $true
            break
        }
    }

    Write-Host "Download required: $need_download"
    if (!$need_download) {
        return
    }

    $runtime_url = "$BASE_URL/ws-studio-env.zip"
    Write-Host "Downloading runtime from $runtime_url"
    try {
        (New-Object Net.WebClient).DownloadFile($runtime_url, ".\ws-studio-env.zip")
    }
    catch [Exception] {
        Write-Host $_.Exception.InnerException
        Write-Host "Download failed, abort runtime setup: ${_.Exception.Message}"
        return
    }

    Write-Host "Extracting ws-studio-env.zip"
    $zip = [System.IO.Compression.ZipFile]::OpenRead(".\ws-studio-env.zip")
    try {
        $entries = $zip.Entries
        $entries | ForEach-Object {
            if ( [System.IO.File]::Exists(".\$_")) {
                [System.IO.File]::Delete(".\$_")
            }
        }
    }
    finally {
        $zip.Dispose()
    }

    [System.IO.Compression.ZipFile]::ExtractToDirectory(".\ws-studio-env.zip", ".\")
    [System.IO.File]::Delete(".\ws-studio-env.zip")
}

[Net.ServicePointManager]::SecurityProtocol = "tls12, tls11, tls"
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = { $true };


$scriptpath = $MyInvocation.MyCommand.Path
$dir = Split-Path $scriptpath
Push-Location $dir
[Environment]::CurrentDirectory = $dir

if (check_for_updates) {
    download_version
}

if (!(Test-Path ".\wildstar-studio.exe.lnk")) {
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut(".\wildstar-studio.exe.lnk")
    $Shortcut.TargetPath = "$dir\bin\wildstar_studio.exe"
    $Shortcut.Save()
}

check_for_runtime_env

.\wildstar-studio.exe.lnk

Pop-Location