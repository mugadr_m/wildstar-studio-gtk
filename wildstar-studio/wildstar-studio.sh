#!/usr/bin/env bash

BASE_PATH=https://bitbucket.org/mugadr_m/wildstar-studio-gtk/downloads
if [[ $(uname -m) == "x86_64" ]]
then
    IS_X64=1
else
    IS_X64=0
fi

function check_for_package() {
    package=$1
    echo Checking status of package ${package}
    install_status=$(dpkg -l ${package} | tail -n +6)
    if [[ ${install_status} == ii* ]]
    then
        return
    fi

    sudo apt-get -y install ${package}
}

function check_internet() {
    echo "Checking internet connection by pinging default gateway..."
    ping -q -w 1 -c 1 `ip r | grep default | cut -d ' ' -f 3` > /dev/null && return 0 || return 1
}

function check_for_update() {
    if [[ ${use_debug} == 1 ]]
    then
        version_path=${BASE_PATH}/version_unix_dbg.txt
    else
        version_path=${BASE_PATH}/version_unix.txt
    fi

    echo Downloading ${version_path}

    check_for_package curl
    download_result=$(curl -kL --silent --show-error --fail ${version_path})

    if [[ $? != 0 ]]
    then
        echo "Unable to download version information, not updating"
        need_update=0
        return
    fi

    if [[ ! -f ./wildstar_studio ]]
    then
        echo Executable $(realpath ./wildstar_studio) not found, updating
        export need_update=1
        export target_version=${download_result}
        return
    fi

    cur_version=$(./wildstar_studio --version)
    echo Current version: ${cur_version}
    echo Remote version: ${download_result}

    if [[ ${cur_version} != ${download_result} ]]
    then
        echo Update required. Updating to version ${download_result}
        export need_update=1
        export target_version=${download_result}
    else
        echo No update required, ${cur_version} is already latest
        export need_update=0
    fi
}

do_update=1
use_debug=0
need_update=0

for arg in "$@"
do
    case ${arg} in
    --no-update)
        do_update=0
        ;;
    --debug-mode)
        use_debug=1
        ;;
    esac
done

if [[ ${do_update} != 0 ]]
then
    do_update=call check_internet
fi

if [[ ${do_update} != 0 ]]
then
    check_for_update
    if [[ ${need_update} != 0 ]]
    then
        zip_file=wildstar_studio_unix_x64
        if [[ ${use_debug} != 0 ]]
        then zip_file=${zip_file}_dbg
        fi

        zip_file=${zip_file}_${target_version}.zip
        zip_url=${BASE_PATH}/${zip_file}

        echo Downloading from ${zip_url}
        curl -kLO --silent --show-error --fail ${zip_url}

        if [[ $? != 0 ]]
        then echo Error downloading application
        else
            echo Unzipping ${zip_file}
            unzip -o ${zip_file}
            if [[ $? != 0 ]]
            then exit $?
            fi
            rm ${zip_file}
            chmod +x ./wildstar_studio
        fi
    fi
fi

check_for_package libgtk-3-0
check_for_package libgtksourceview-3.0-1
check_for_package mesa-utils

./wildstar_studio --exec