#include <utility>

#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <iomanip>
#include <memory.h>
#include <chrono>
#include <algorithm>
#include <utils/RuntimeError.h>
#include <glm/vec3.hpp>
#include <glm/gtx/color_space_YCoCg.inl>
#include "log.h"

namespace libtex {
    static constexpr uint32_t MAX_DC_LUMINANCE_LEN = 16;
    static constexpr uint32_t MAX_AC_LUMINANCE_LEN = 16;
    static constexpr uint32_t MAX_DC_CHROMINANCE_LEN = 16;
    static constexpr uint32_t MAX_AC_CHROMINANCE_LEN = 16;

    uint8_t dc_luminance_lengths[MAX_DC_LUMINANCE_LEN] = {0, 1, 5, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0};
    uint8_t ac_luminance_lengths[MAX_AC_LUMINANCE_LEN] = {0, 2, 1, 3, 3, 2, 4, 3, 5, 5, 4, 4, 0, 0, 1, 125};
    uint8_t dc_chrominance_lenghts[MAX_DC_CHROMINANCE_LEN] = {0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0};
    uint8_t ac_chrominance_lenghts[MAX_AC_CHROMINANCE_LEN] = {0, 2, 1, 2, 4, 4, 3, 4, 7, 5, 4, 4, 0, 1, 2, 119};

    uint8_t dc_luminance_vals[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

    uint8_t ac_luminance_vals[] = {0x01, 0x02, 0x03, 0x00, 0x04, 0x11, 0x05, 0x12, 0x21, 0x31, 0x41, 0x06, 0x13, 0x51,
                                   0x61, 0x07, 0x22, 0x71, 0x14, 0x32, 0x81, 0x91, 0xA1, 0x08, 0x23, 0x42, 0xB1, 0xC1,
                                   0x15, 0x52, 0xD1, 0xf0, 0x24, 0x33, 0x62, 0x72, 0x82, 0x09, 0x0A, 0x16, 0x17, 0x18,
                                   0x19, 0x1A, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39,
                                   0x3A, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x53, 0x54, 0x55, 0x56, 0x57,
                                   0x58, 0x59, 0x5A, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x73, 0x74, 0x75,
                                   0x76, 0x77, 0x78, 0x79, 0x7A, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x92,
                                   0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9A, 0xA2, 0xA3, 0xA4, 0xa5, 0xA6, 0xA7,
                                   0xA8, 0xA9, 0xAA, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA, 0xC2, 0xC3,
                                   0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6, 0xD7, 0xD8,
                                   0xD9, 0xDA, 0xE1, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEa, 0xF1, 0xF2,
                                   0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFa};

    uint8_t dc_chrominance_vals[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

    uint8_t ac_chrominance_vals[] = {0x00, 0x01, 0x02, 0x03, 0x11, 0x04, 0x05, 0x21, 0x31, 0x06, 0x12, 0x41, 0x51, 0x07,
                                     0x61, 0x71, 0x13, 0x22, 0x32, 0x81, 0x08, 0x14, 0x42, 0x91, 0xA1, 0xB1, 0xC1, 0x09,
                                     0x23, 0x33, 0x52, 0xF0, 0x15, 0x62, 0x72, 0xD1, 0x0A, 0x16, 0x24, 0x34, 0xE1, 0x25,
                                     0xF1, 0x17, 0x18, 0x19, 0x1A, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x35, 0x36, 0x37, 0x38,
                                     0x39, 0x3A, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x53, 0x54, 0x55, 0x56,
                                     0x57, 0x58, 0x59, 0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6A, 0x73, 0x74,
                                     0x75, 0x76, 0x77, 0x78, 0x79, 0x7a, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89,
                                     0x8A, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9A, 0xa2, 0xA3, 0xA4, 0xA5,
                                     0xA6, 0xA7, 0xA8, 0xA9, 0xAA, 0xB2, 0xB3, 0xB4, 0xB5, 0xB6, 0xB7, 0xB8, 0xB9, 0xBA,
                                     0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xD2, 0xD3, 0xD4, 0xD5, 0xD6,
                                     0xD7, 0xD8, 0xD9, 0xDA, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xF2,
                                     0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFA};

    uint8_t zigzag_mapping[] = {0, 1, 5, 6, 14, 15, 27, 28, 2, 4, 7, 13, 16, 26, 29, 42, 3, 8, 12, 17, 25, 30, 41, 43,
                                9,
                                11, 18, 24, 31, 40, 44, 53, 10, 19, 23, 32, 39, 45, 52, 54, 20, 22, 33, 38, 46, 51, 55,
                                60,
                                21, 34, 37, 47, 50, 56, 59, 61, 35, 36, 48, 49, 57, 58, 62, 63};

    uint8_t lum_quant_template[] = {16, 11, 10, 16, 24, 40, 51, 61,
                                    12, 12, 14, 19, 26, 58, 60, 55,
                                    14, 13, 16, 24, 40, 57, 69, 56,
                                    14, 17, 22, 29, 51, 87, 80, 62,
                                    18, 22, 37, 56, 68, 109, 103, 77,
                                    24, 35, 55, 64, 81, 104, 113, 92,
                                    49, 64, 78, 87, 103, 121, 120, 101,
                                    72, 92, 95, 98, 112, 100, 103, 99};

    uint8_t chroma_quant_template[] = {17, 18, 24, 47, 99, 99, 99, 99,
                                       18, 21, 26, 66, 99, 99, 99, 99,
                                       24, 26, 56, 99, 99, 99, 99, 99,
                                       47, 66, 99, 99, 99, 99, 99, 99,
                                       99, 99, 99, 99, 99, 99, 99, 99,
                                       99, 99, 99, 99, 99, 99, 99, 99,
                                       99, 99, 99, 99, 99, 99, 99, 99,
                                       99, 99, 99, 99, 99, 99, 99, 99};

    float luminanceQuantTables[100][64]{};
    float chrominanceQuantTables[100][64]{};

    struct LayerInfo {
        uint8_t hasDefault;
        uint8_t defaultVal;
    };

#ifndef M_PI
#define M_PI 3.1415926
#endif

#define W1 2841
#define W2 2676
#define W3 2408
#define W5 1609
#define W6 1108
#define W7 565

    static int16_t iclip[1024];
    static int16_t *iclp;

    static void idctrow(int16_t *blk);

    static void idctcol(int16_t *blk);

    static void idctrow(int16_t *blk) {
        int x0, x1, x2, x3, x4, x5, x6, x7, x8;

        if (!((x1 = blk[4] << 11) | (x2 = blk[6]) | (x3 = blk[2]) |
              (x4 = blk[1]) | (x5 = blk[7]) | (x6 = blk[5]) | (x7 = blk[3]))) {
            blk[0] = blk[1] = blk[2] = blk[3] = blk[4] = blk[5] = blk[6] = blk[7] = blk[0] << 3;
            return;
        }

        x0 = (blk[0] << 11) + 128;

        x8 = W7 * (x4 + x5);
        x4 = x8 + (W1 - W7) * x4;
        x5 = x8 - (W1 + W7) * x5;
        x8 = W3 * (x6 + x7);
        x6 = x8 - (W3 - W5) * x6;
        x7 = x8 - (W3 + W5) * x7;

        x8 = x0 + x1;
        x0 -= x1;
        x1 = W6 * (x3 + x2);
        x2 = x1 - (W2 + W6) * x2;
        x3 = x1 + (W2 - W6) * x3;
        x1 = x4 + x6;
        x4 -= x6;
        x6 = x5 + x7;
        x5 -= x7;

        x7 = x8 + x3;
        x8 -= x3;
        x3 = x0 + x2;
        x0 -= x2;
        x2 = (181 * (x4 + x5) + 128) >> 8;
        x4 = (181 * (x4 - x5) + 128) >> 8;

        blk[0] = static_cast<int16_t>((x7 + x1) >> 8);
        blk[1] = static_cast<int16_t>((x3 + x2) >> 8);
        blk[2] = static_cast<int16_t>((x0 + x4) >> 8);
        blk[3] = static_cast<int16_t>((x8 + x6) >> 8);
        blk[4] = static_cast<int16_t>((x8 - x6) >> 8);
        blk[5] = static_cast<int16_t>((x0 - x4) >> 8);
        blk[6] = static_cast<int16_t>((x3 - x2) >> 8);
        blk[7] = static_cast<int16_t>((x7 - x1) >> 8);
    }


    static void idctcol(int16_t *blk) {
        int x0, x1, x2, x3, x4, x5, x6, x7, x8;

        if (!((x1 = (blk[8 * 4] << 8)) | (x2 = blk[8 * 6]) | (x3 = blk[8 * 2]) |
              (x4 = blk[8 * 1]) | (x5 = blk[8 * 7]) | (x6 = blk[8 * 5]) | (x7 = blk[8 * 3]))) {
            blk[8 * 0] = blk[8 * 1] = blk[8 * 2] = blk[8 * 3] = blk[8 * 4] = blk[8 * 5] = blk[8 * 6] = blk[8 * 7] =
                    iclp[(blk[8 * 0] + 32) >> 6];
            return;
        }

        x0 = (blk[8 * 0] << 8) + 8192;

        x8 = W7 * (x4 + x5) + 4;
        x4 = (x8 + (W1 - W7) * x4) >> 3;
        x5 = (x8 - (W1 + W7) * x5) >> 3;
        x8 = W3 * (x6 + x7) + 4;
        x6 = (x8 - (W3 - W5) * x6) >> 3;
        x7 = (x8 - (W3 + W5) * x7) >> 3;

        x8 = x0 + x1;
        x0 -= x1;
        x1 = W6 * (x3 + x2) + 4;
        x2 = (x1 - (W2 + W6) * x2) >> 3;
        x3 = (x1 + (W2 - W6) * x3) >> 3;
        x1 = x4 + x6;
        x4 -= x6;
        x6 = x5 + x7;
        x5 -= x7;

        x7 = x8 + x3;
        x8 -= x3;
        x3 = x0 + x2;
        x0 -= x2;
        x2 = (181 * (x4 + x5) + 128) >> 8;
        x4 = (181 * (x4 - x5) + 128) >> 8;

        blk[8 * 0] = iclp[(x7 + x1) >> 14];
        blk[8 * 1] = iclp[(x3 + x2) >> 14];
        blk[8 * 2] = iclp[(x0 + x4) >> 14];
        blk[8 * 3] = iclp[(x8 + x6) >> 14];
        blk[8 * 4] = iclp[(x8 - x6) >> 14];
        blk[8 * 5] = iclp[(x0 - x4) >> 14];
        blk[8 * 6] = iclp[(x3 - x2) >> 14];
        blk[8 * 7] = iclp[(x7 - x1) >> 14];
    }

    void idct(int16_t *block) {
        int i;

        for (i = 0; i < 8; i++)
            idctrow(block + 8 * i);

        for (i = 0; i < 8; i++)
            idctcol(block + i);
    }

    void init_idct() {
        int i;

        iclp = iclip + 512;
        for (i = -512; i < 512; i++)
            iclp[i] = static_cast<int16_t>((i < -256) ? -256 : ((i > 255) ? 255 : i));
    }

    class BitStream {
        std::vector<uint8_t> mData;
        std::size_t mBitPosition = 0;

    public:
        explicit BitStream(std::vector<uint8_t> data) : mData(std::move(data)) {

        }

        uint8_t readBit() {
            std::size_t bytePosition = mBitPosition / 8;
            std::size_t bitPosition = mBitPosition % 8;
            bitPosition = 7 - bitPosition;

            const auto ret = static_cast<const uint8_t>((mData[bytePosition] & (1 << bitPosition)) >> bitPosition);
            ++mBitPosition;
            return ret;
        }

        uint16_t readBits(uint8_t numBits) {
            if (numBits >= 16) {
                log_console->warn("Attempted to read {} bits which is more than maximum 16", numBits);
                throw ::utils::RuntimeError("Invalid bit count");
            }

            uint16_t ret = 0u;
            for (uint8_t i = 0u; i < numBits; ++i) {
                ret <<= 1;
                ret |= readBit();
            }

            return ret;
        }
    };

    typedef std::pair<uint32_t, uint16_t> tHuffKey;
    typedef std::map<tHuffKey, uint8_t> tHuffTable;

    void loadHuffTable(const uint8_t *lengths, const uint8_t numLengths, const uint8_t *values, tHuffTable &outTable) {
        uint32_t curIndex = 0;
        uint16_t code = 0;
        for (uint32_t length = 0; length < numLengths; ++length) {
            uint32_t numValues = lengths[length];
            for (uint32_t i = 0; i < numValues; ++i) {
                outTable[tHuffKey(length + 1, code)] = values[curIndex++];
                ++code;
            }
            code <<= 1;
        }
    }

    void loadQuantTable(uint8_t qualityFactor, const uint8_t *quantTemplate, float *quantTable) {
        float multiplier = (200.0f - qualityFactor * 2.0f) * 0.01f;
        for (auto i = 0u; i < 64; ++i) {
            quantTable[i] = quantTemplate[i] * multiplier;
        }
    }

    int16_t extend(uint16_t value, uint16_t length) {
        if (value < (1 << (length - 1))) {
            return static_cast<int16_t>(value + (-1 << length) + 1);
        } else {
            return value;
        }
    }

    uint8_t decodeValue(BitStream &stream, const tHuffTable &table) {
        uint16_t word = 0;
        uint8_t wordLength = 0;
        do {
            word <<= 1;
            word |= stream.readBit();
            ++wordLength;
            const auto itr = table.find(tHuffKey(wordLength, word));
            if (itr != table.end()) {
                return itr->second;
            }
        } while (wordLength < 16);
        log_console->warn("Value not found in huffman tree");
        throw ::utils::RuntimeError("Did not find value in huffman tree");
    }

    int16_t decodeBlock(BitStream &stream, int16_t *blockPtr, const tHuffTable &dcTable, const tHuffTable &acTable,
                        int16_t prevDC = 0) {
        const auto dcLen = decodeValue(stream, dcTable);
        auto epsilon = stream.readBits(dcLen);
        const auto deltaDC = extend(epsilon, dcLen);
        const auto curDC = deltaDC + prevDC;
        blockPtr[0] = curDC;

        for (auto idx = 1u; idx < 64;) {
            const auto acCodedValue = decodeValue(stream, acTable);
            if (acCodedValue == 0) {
                break;
            }

            if (acCodedValue == 0xF0) {
                idx += 16;
                continue;
            }

            idx += (acCodedValue >> 4) & 0xF;
            const auto acLen = static_cast<const uint8_t>(acCodedValue & 0xF);
            epsilon = stream.readBits(acLen);
            const auto acValue = extend(epsilon, acLen);
            blockPtr[idx] = acValue;
            ++idx;
        }

        return curDC;
    }

    void unzigzag(int16_t *block) {
        int16_t tmpBuffer[64];
        for (auto i = 0u; i < 64; ++i) {
            tmpBuffer[i] = block[zigzag_mapping[i]];
        }

        memcpy(block, tmpBuffer, sizeof tmpBuffer);
    }

    void dequantize(int16_t *block, const float *quantizationTable) {
        for (auto i = 0u; i < 64; ++i) {
            block[i] = int16_t(std::round(block[i] * quantizationTable[i]));
        }
    }

    template<typename T, typename U = T>
    U clamp(T value, T min, T max) {
        return static_cast<U>(std::min<T>(std::max<T>(value, min), max));
    }

    uint32_t toColor(int32_t y, int32_t cb, int32_t cr) {
        int32_t alpha = y - (cr >> 1);
        const auto beta = clamp<int32_t, uint32_t>(alpha + cr, 0, 255);
        const auto gamma = clamp<int32_t, uint32_t>(alpha - (cb >> 1), 0, 255);
        const auto delta = clamp<int32_t, uint32_t>(gamma + cb, 0, 255);
        return (gamma & 0xFF) | ((beta & 0xFF) << 8) | ((delta & 0xFF) << 16) | (0xFF << 24);
    }

    int16_t processBlock(int16_t prevDc, BitStream &stream, const tHuffTable &dcTable, const tHuffTable &acTable,
                         float *quantTable, bool isLuminance, int16_t *outBlock) {
        int16_t workBlock[64] = {0};
        int16_t curDc = decodeBlock(stream, workBlock, dcTable, acTable, prevDc);
        unzigzag(workBlock);
        dequantize(workBlock, quantTable);
        idct(workBlock);
        for (auto i = 0; i < 64; ++i) {
            int16_t value = workBlock[i];
            if (isLuminance) {
                value = clamp<int16_t>(static_cast<int16_t>(value + 128), 0, 255);
            } else {
                value = clamp<int16_t>(value, -256, 255);
            }
            outBlock[i] = value;
        }
        return curDc;
    }

    void decodeColorBlockType0(int16_t lum0[4][64], int16_t crom0[64], int16_t crom1[64], uint32_t *colors) {
        for (uint32_t y = 0; y < 16; ++y) {
            for (uint32_t x = 0; x < 16; ++x) {
                uint32_t cy = (y >= 8) ? 1 : 0;
                uint32_t cx = (x >= 8) ? 1 : 0;
                uint32_t by = y % 8;
                uint32_t bx = x % 8;

                uint32_t block = cy * 2 + cx;
                uint32_t lumIdx = by * 8 + bx;
                uint32_t crmIdx = (y / 2) * 8 + (x / 2);
                colors[y * 16 + x] = toColor(lum0[block][lumIdx], crom0[crmIdx], crom1[crmIdx]);
            }
        }
    }

    void decodeColorBlockType2(int16_t lum0[64], int16_t crom0[64], int16_t crom1[64], uint32_t *colors) {
        for (uint32_t y = 0; y < 8; ++y) {
            for (uint32_t x = 0; x < 8; ++x) {
                uint32_t idx = y * 8 + x;
                colors[idx] = toColor(lum0[idx], crom0[idx], crom1[idx]);
            }
        }
    }

    void decodeImageType0(
            uint32_t width,
            uint32_t height,
            std::vector<uint8_t> &outData,
            BitStream &stream,
            const tHuffTable &dcLumTable,
            const tHuffTable &acLumTable,
            const tHuffTable &dcCromTable,
            const tHuffTable &acCromTable,
            const std::vector<LayerInfo> &layerInfo,
            float *lumQuant[4],
            float *cromQuant[4]) {
        int16_t lum0[4][64]{};
        int16_t lum1[4][64]{};
        int16_t crom0[64]{};
        int16_t crom1[64]{};
        uint32_t colorBlock[16 * 16]{};

        const auto actualHeight = ((height + 15) / 16) * 16;
        const auto actualWidth = ((width + 15) / 16) * 16;

        int16_t prevDc[4] = {0};
        for (auto y = 0; y < (actualHeight / 16); ++y) {
            for (auto x = 0; x < (actualWidth / 16); ++x) {
                for (auto &lum : lum0) {
                    prevDc[0] = processBlock(prevDc[0], stream, dcLumTable, acLumTable, lumQuant[0], true, lum);
                }
                prevDc[1] = processBlock(prevDc[1], stream, dcCromTable, acCromTable, cromQuant[1], false, crom0);
                prevDc[2] = processBlock(prevDc[2], stream, dcCromTable, acCromTable, cromQuant[2], false, crom1);
                for (auto &lum : lum1) {
                    prevDc[3] = processBlock(prevDc[3], stream, dcLumTable, acLumTable, lumQuant[3], true, lum);
                }

                decodeColorBlockType0(lum0, crom0, crom1, colorBlock);
                for (auto row = 0; row < 16; ++row) {
                    if (y * 16 + row >= height || x * 16 >= width) {
                        continue;
                    }

                    const auto numPixels = std::min(16u, width - x * 16);
                    memcpy(outData.data() + (y * 16 + row) * width * 4 + x * 16 * 4, &colorBlock[row * 16], numPixels * 4);
                }
            }
        }
    }

    void decodeImageType1(
            uint32_t width,
            uint32_t height,
            std::vector<uint8_t> &outData,
            BitStream &stream,
            const tHuffTable &dcLumTable,
            const tHuffTable &acLumTable,
            const std::vector<LayerInfo> &layerInfo,
            float *lumQuant[4]) {
        int16_t lum[4][64]{};

        const auto actualWidth = ((width + 7) / 8) * 8;
        const auto actualHeight = ((height + 7) / 8) * 8;

        int16_t prevDc[4] = {0};
        for (auto y = 0; y < (actualHeight / 8); ++y) {
            for (auto x = 0; x < (actualWidth / 8); ++x) {
                prevDc[0] = processBlock(prevDc[0], stream, dcLumTable, acLumTable, lumQuant[0], true, lum[0]);
                prevDc[1] = processBlock(prevDc[1], stream, dcLumTable, acLumTable, lumQuant[1], true, lum[1]);
                if (layerInfo[2].hasDefault == 0) {
                    prevDc[2] = processBlock(prevDc[2], stream, dcLumTable, acLumTable, lumQuant[2], true, lum[2]);
                }
                if (layerInfo[3].hasDefault == 0) {
                    prevDc[3] = processBlock(prevDc[3], stream, dcLumTable, acLumTable, lumQuant[3], true, lum[3]);
                }

                for (auto iy = 0; iy < 8; ++iy) {
                    for (auto ix = 0; ix < 8; ++ix) {
                        if (y * 8 + iy >= height || x * 8 + ix >= width) {
                            continue;
                        }

                        const auto r = static_cast<uint8_t>(lum[0][iy * 8 + ix]);
                        const auto g = static_cast<uint8_t>(lum[1][iy * 8 + ix]);
                        const auto b = clamp<int32_t, uint8_t>(255 - r - g, 0, 255);
                        outData[((y * 8 + iy) * width + (x * 8 + ix)) * 4] = r;
                        outData[((y * 8 + iy) * width + (x * 8 + ix)) * 4 + 1] = g;
                        outData[((y * 8 + iy) * width + (x * 8 + ix)) * 4 + 2] = b;
                        outData[((y * 8 + iy) * width + (x * 8 + ix)) * 4 + 3] = 0xFF;
                    }
                }
            }
        }
    }

    void decodeImageType2(
            uint32_t width,
            uint32_t height,
            std::vector<uint8_t> &outData,
            BitStream &stream,
            const tHuffTable &dcLumTable,
            const tHuffTable &acLumTable,
            const tHuffTable &dcCromTable,
            const tHuffTable &acCromTable,
            const std::vector<LayerInfo> &layerInfo,
            float *lumQuant[4],
            float *cromQuant[4]) {
        int16_t lum0[64]{};
        int16_t lum1[64]{};
        int16_t crom0[64]{};
        int16_t crom1[64]{};
        uint32_t colorBlock[8 * 8]{};

        const auto actualWidth = ((width + 7) / 8) * 8;
        const auto actualHeight = ((height + 7) / 8) * 8;

        int16_t prevDc[4] = {0};
        for (auto y = 0; y < (actualHeight / 8); ++y) {
            for (auto x = 0; x < (actualWidth / 8); ++x) {
                prevDc[0] = processBlock(prevDc[0], stream, dcLumTable, acLumTable, lumQuant[0], true, lum0);
                prevDc[1] = processBlock(prevDc[1], stream, dcCromTable, acCromTable, cromQuant[1], false, crom0);
                prevDc[2] = processBlock(prevDc[2], stream, dcCromTable, acCromTable, cromQuant[2], false, crom1);

                if (layerInfo[3].hasDefault == 0) {
                    prevDc[3] = processBlock(prevDc[3], stream, dcLumTable, acLumTable, lumQuant[3], true, lum1);
                }

                decodeColorBlockType2(lum0, crom0, crom1, colorBlock);
                for (auto row = 0; row < 8; ++row) {
                    if ((y * 8 + row) >= height) {
                        continue;
                    }

                    if (x * 8 >= width) {
                        continue;
                    }

                    const auto numPixels = std::min(8u, width - x * 8);
                    memcpy(outData.data() + (y * 8 + row) * width * 4 + x * 8 * 4, &colorBlock[row * 8], numPixels * 4);
                }
            }
        }
    }

    void decodeImage(
            uint32_t imageWidth,
            uint32_t imageHeight,
            BitStream &stream,
            uint32_t imageType,
            const tHuffTable &dcLumTable,
            const tHuffTable &acLumTable,
            const tHuffTable &dcCromTable,
            const tHuffTable &acCromTable,
            const std::vector<LayerInfo> &layerInfo,
            float *lumQuant[4],
            float *cromQuant[4],
            uint8_t *outData) {

        std::vector<uint8_t> imageData(imageWidth * imageHeight * 4);

        if (imageType == 0) {
            decodeImageType0(imageWidth, imageHeight, imageData, stream, dcLumTable, acLumTable, dcCromTable,
                             acCromTable, layerInfo,
                             lumQuant, cromQuant);
        } else if (imageType == 1) {
            decodeImageType1(imageWidth, imageHeight, imageData, stream, dcLumTable, acLumTable, layerInfo, lumQuant);
        } else if (imageType == 2) {
            decodeImageType2(imageWidth, imageHeight, imageData, stream, dcLumTable, acLumTable, dcCromTable,
                             acCromTable, layerInfo,
                             lumQuant, cromQuant);
        } else {
            log_console->warn("Image type must be one of [0, 1, 2], was {}", imageType);
            throw ::utils::RuntimeError("Image type not supported");
        }

        for (auto i = 0; i < imageData.size(); i += 4) {
            auto &r = imageData[i];
            auto &b = imageData[i + 2];
            auto rt = r;
            r = b;
            b = rt;
        }

        memcpy(outData, imageData.data(), imageData.size());
    }

    tHuffTable dcLumTable;
    tHuffTable acLumTable;
    tHuffTable dcChromaTable;
    tHuffTable acChromaTable;

    class StaticInitializer {
    public:
        StaticInitializer() {
            init_idct();

            loadHuffTable(dc_luminance_lengths, MAX_DC_LUMINANCE_LEN, dc_luminance_vals, dcLumTable);
            loadHuffTable(ac_luminance_lengths, MAX_AC_LUMINANCE_LEN, ac_luminance_vals, acLumTable);
            loadHuffTable(dc_chrominance_lenghts, MAX_DC_CHROMINANCE_LEN, dc_chrominance_vals, dcChromaTable);
            loadHuffTable(ac_chrominance_lenghts, MAX_AC_CHROMINANCE_LEN, ac_chrominance_vals, acChromaTable);

            for (auto i = 0; i < 100; ++i) {
                loadQuantTable(static_cast<uint8_t>(i), lum_quant_template, luminanceQuantTables[i]);
                loadQuantTable(static_cast<uint8_t>(i), chroma_quant_template, chrominanceQuantTables[i]);
            }
        }
    };

    extern "C" void decompressImageLayer(
            uint32_t imageType,
            const uint8_t *imageData,
            uint32_t imageSize,
            uint32_t width,
            uint32_t height,
            uint8_t qualityLevel0,
            uint8_t hasDefault0,
            uint8_t default0,
            uint8_t qualityLevel1,
            uint8_t hasDefault1,
            uint8_t default1,
            uint8_t qualityLevel2,
            uint8_t hasDefault2,
            uint8_t default2,
            uint8_t qualityLevel3,
            uint8_t hasDefault3,
            uint8_t default3,
            uint8_t *colorData) {
        StaticInitializer{};

        float *luminanceQuantTable[4]{};
        float *chrominanceQuantTable[4]{};

        uint8_t qualityLevels[] = {
                qualityLevel0,
                qualityLevel1,
                qualityLevel2,
                qualityLevel3
        };

        for (auto i = 0u; i < 4; ++i) {
            if (qualityLevels[i] > 100) {
                log_console->warn("Invalid quality level for component {}: {} is not in range [0, 100]", i, qualityLevels[i]);
                throw ::utils::RuntimeError("Quality level must be <= 100");
            }

            luminanceQuantTable[i] = luminanceQuantTables[qualityLevels[i]];
            chrominanceQuantTable[i] = chrominanceQuantTables[qualityLevels[i]];
        }

        BitStream data(std::vector<uint8_t>(imageData, imageData + imageSize));

        decodeImage(width, height, data, imageType, dcLumTable, acLumTable, dcChromaTable, acChromaTable,
                    {{hasDefault0, default0},
                     {hasDefault1, default1},
                     {hasDefault2, default2},
                     {hasDefault3, default3}},
                    luminanceQuantTable, chrominanceQuantTable, colorData);
    }
};