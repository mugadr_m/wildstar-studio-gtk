
#ifndef WILDSTAR_STUDIO_LIBTEX_H
#define WILDSTAR_STUDIO_LIBTEX_H

#include <stdint.h>

namespace libtex {
    extern "C" void decompressImageLayer(
            uint32_t imageType,
            const uint8_t *imageData,
            uint32_t imageSize,
            uint32_t width,
            uint32_t height,
            uint8_t qualityLevel0,
            uint8_t hasDefault0,
            uint8_t default0,
            uint8_t qualityLevel1,
            uint8_t hasDefault1,
            uint8_t default1,
            uint8_t qualityLevel2,
            uint8_t hasDefault2,
            uint8_t default2,
            uint8_t qualityLevel3,
            uint8_t hasDefault3,
            uint8_t default3,
            uint8_t *colorData);
}

#endif //WILDSTAR_STUDIO_LIBTEX_H
