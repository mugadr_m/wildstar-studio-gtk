#ifndef WILDSTAR_STUDIO_BUFFERS_H
#define WILDSTAR_STUDIO_BUFFERS_H

#include "glad/glad.h"
#include <cstdint>
#include <vector>
#include <array>
#include <memory>

namespace gl {
    class Buffer {
        GLuint mBuffer;
        GLenum mType;

    protected:
        explicit Buffer(GLenum type);
    public:
        virtual ~Buffer();

        void bind();
        void setData(const void* data, std::size_t size);

        template<typename T>
        void setData(const T& value) {
            setData(&value, sizeof(T));
        }

        template<typename T>
        void setData(const std::vector<T>& value) {
            setData(value.data(), value.size() * sizeof(T));
        }

        template<typename T, std::size_t size>
        void setData(const std::array<T, size>& value) {
            setData(value.data(), size * sizeof(T));
        }
    };

    class VertexBuffer : public Buffer {
    public:
        VertexBuffer() : Buffer(GL_ARRAY_BUFFER) { }
    };

    enum class IndexType {
        BYTE = GL_UNSIGNED_BYTE,
        SHORT = GL_UNSIGNED_SHORT,
        INT = GL_UNSIGNED_INT
    };

    class IndexBuffer : public Buffer {
        IndexType mIndexType;

    public:
        IndexBuffer(IndexType indexType) : Buffer(GL_ELEMENT_ARRAY_BUFFER), mIndexType(indexType) {

        }

        const IndexType& getIndexType() const {
            return mIndexType;
        }

        void setIndexType(const IndexType& indexType) {
            mIndexType = indexType;
        }
    };

    typedef std::shared_ptr<VertexBuffer> VertexBufferPtr;
    typedef std::shared_ptr<IndexBuffer> IndexBufferPtr;
}


#endif //WILDSTAR_STUDIO_BUFFERS_H
