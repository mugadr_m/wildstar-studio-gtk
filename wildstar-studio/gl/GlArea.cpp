#include "GlArea.h"
#include "log.h"
#include <stdexcept>
#include <glm/gtc/matrix_transform.hpp>
#include <utils/RuntimeError.h>
#include "glad/glad.h"

namespace gl {
    namespace callbacks {
        void onRealize(GtkGLArea *area, std::function<void(GtkGLArea *)> *callback) {
            (*callback)(area);
        }

        gboolean onFrame(GtkGLArea *area,
                         GdkGLContext *context,
                         std::function<void(GtkGLArea *, GdkGLContext *)> *callback) {
            (*callback)(area, context);
            return TRUE;
        }

        void onResize(GtkGLArea *area, gint width, gint height,
                      std::function<void(uint32_t, uint32_t)> *callback) {
            (*callback)(static_cast<uint32_t>(width), static_cast<uint32_t>(height));
        }

        gboolean onWidgetTick(GtkWidget *widget,
                              GdkFrameClock *frame_clock,
                              gpointer data) {
            ((GlArea*) data)->onTick();
            return G_SOURCE_CONTINUE;
        }
    }

    void GlArea::onRealize(GtkGLArea *area) {
        gtk_gl_area_make_current(area);

        gladLoadGL();

        const auto error = gtk_gl_area_get_error(area);
        if (error != nullptr) {
            log_console->error("Unable to initialize OpenGL: {}", error->message);
            throw ::utils::RuntimeError(std::string(error->message));
        }

        std::string vendor = (const char *) glGetString(GL_VENDOR);
        std::string renderer = (const char *) glGetString(GL_RENDERER);
        std::string version = (const char *) glGetString(GL_VERSION);

        log_console->info("OpenGL version {} by {} on {}", version, vendor, renderer);

        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClearDepth(1.0f);

        if(mOnLoadCallback) {
            mOnLoadCallback();
        }
    }

    GlArea::GlArea(GtkWidget *glArea) : mControl(glArea) {
        g_signal_connect(glArea, "realize", G_CALLBACK(callbacks::onRealize),
                         new std::function<void(GtkGLArea *)>{
                                 [this](GtkGLArea *ptr) { onRealize(ptr); }
                         }
        );

        g_signal_connect(glArea, "render", G_CALLBACK(callbacks::onFrame),
                         new std::function<void(GtkGLArea *, GdkGLContext *)>{
                                 [this](GtkGLArea *area, GdkGLContext *context) { onFrame(context); }
                         }
        );

        g_signal_connect(glArea, "resize", G_CALLBACK(callbacks::onResize),
                         new std::function<void(uint32_t, uint32_t)>{
                                 [this](uint32_t w, uint32_t h) { onResize(w, h); }
                         }
        );

        gtk_gl_area_set_auto_render(GTK_GL_AREA(glArea), TRUE);
        gtk_widget_add_tick_callback(glArea, callbacks::onWidgetTick, this, nullptr);
    }

    void GlArea::onFrame(GdkGLContext *context) {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        if (mOnFrameCallback) {
            mOnFrameCallback();
        }
    }

    void GlArea::onResize(uint32_t width, uint32_t height) {
        mOrthoMatrix = glm::ortho(0.0f, static_cast<float>(width), static_cast<float>(height), 0.0f, 0.0f, 1.0f);
        mPerspectiveMatrix = glm::perspective(glm::radians(60.0), (double) width / (double) height, 1.0, 2000.0);
    }

    void GlArea::onTick() {
        const auto now = std::chrono::steady_clock::now();
        const auto msPass = std::chrono::duration_cast<std::chrono::milliseconds>(now - mLastFrame);
        if(msPass.count() < 10) {
            return;
        }

        if(mIsVisible) {
            gtk_widget_queue_draw(mControl);
        }

        mLastFrame = now;
    }
}