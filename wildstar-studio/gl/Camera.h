
#ifndef WILDSTAR_STUDIO_CAMERA_H
#define WILDSTAR_STUDIO_CAMERA_H

#include <memory>
#include "glm/glm.hpp"

namespace gl {
    class Camera {
        glm::mat4 mViewMatrix;
        glm::vec3 mUp = glm::vec3(0, 0, 1);
        glm::vec3 mRight = glm::vec3(-1, 0, 0);
        glm::vec3 mForward = glm::vec3(0, -1, 0);
        glm::vec3 mPosition = glm::vec3(0, 0, 0);

        void updateMatrix();

        static glm::vec3 rotate(const glm::mat4& matrix, const glm::vec3& vec);

    public:
        Camera();

        void moveLeft(float amount);
        void moveRight(float amount);
        void moveUp(float amount);
        void moveDown(float amount);
        void moveForward(float amount);
        void moveBackward(float amount);

        void pitch(float amount);
        void yaw(float amount);
        void roll(float amount);

        void setPosition(const glm::vec3& position) {
            mPosition = position;
            updateMatrix();
        }

        void setTarget(const glm::vec3& target);
        void setUp(const glm::vec3& up);

        const glm::mat4& getViewMatrix() const {
            return mViewMatrix;
        }
    };

    typedef std::shared_ptr<Camera> CameraPtr;
}


#endif //WILDSTAR_STUDIO_CAMERA_H
