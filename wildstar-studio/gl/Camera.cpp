
#include <glm/ext/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/vec_swizzle.hpp>
#include "Camera.h"

namespace gl {

    Camera::Camera() {
        updateMatrix();
    }

    void Camera::updateMatrix() {
        mViewMatrix = glm::lookAt(mPosition, mPosition + mForward, mUp);
    }

    void Camera::moveLeft(float amount) {
        mPosition -= amount * mRight;
        updateMatrix();
    }

    void Camera::moveRight(float amount) {
        mPosition += amount * mRight;
        updateMatrix();
    }

    void Camera::moveUp(float amount) {
        mPosition += amount * mUp;
        updateMatrix();
    }

    void Camera::moveDown(float amount) {
        mPosition -= amount * mUp;
        updateMatrix();
    }

    void Camera::moveForward(float amount) {
        mPosition += amount * mForward;
        updateMatrix();
    }

    void Camera::moveBackward(float amount) {
        mPosition -= amount * mForward;
        updateMatrix();
    }

    void Camera::pitch(float amount) {
        const auto mat = glm::rotate(glm::identity<glm::mat4>(), glm::radians(amount), mRight);
        mForward = glm::normalize(rotate(mat, mForward));
        mUp = glm::normalize(rotate(mat, mUp));
        updateMatrix();
    }

    void Camera::yaw(float amount) {
        const auto mat = glm::rotate(glm::identity<glm::mat4>(), glm::radians(amount), glm::vec3(0, 0, 1));
        mForward = glm::normalize(rotate(mat, mForward));
        mUp = glm::normalize(rotate(mat, mUp));
        mRight = glm::normalize(rotate(mat, mRight));
        updateMatrix();
    }

    void Camera::roll(float amount) {
        const auto mat = glm::rotate(glm::mat4(), glm::radians(amount), mForward);
        mUp = glm::normalize(glm::xyz(mat * glm::vec4(mUp, 1.0)));
        mRight = glm::normalize(glm::xyz(mat * glm::vec4(mRight, 1.0)));
        updateMatrix();
    }

    void Camera::setTarget(const glm::vec3 &target) {
        mForward = glm::normalize(target - mPosition);
        mRight = glm::normalize(glm::cross(mForward, mUp));
        updateMatrix();
    }

    glm::vec3 Camera::rotate(const glm::mat4 &matrix, const glm::vec3 &vec) {
        const auto res = matrix * glm::vec4(vec, 1.0);
        return glm::xyz(res) / res.w;
    }

    void Camera::setUp(const glm::vec3 &up) {
        mUp = glm::normalize(up);
        mForward = glm::cross(mUp, mRight);
        updateMatrix();
    }
}