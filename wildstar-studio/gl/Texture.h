
#ifndef WILDSTAR_STUDIO_TEXTURE_H
#define WILDSTAR_STUDIO_TEXTURE_H

#include <glad/glad.h>
#include <vector>
#include <cstdint>
#include <memory>
#include <cstring>

namespace gl {
    enum class DxtImageFormat {
        BC1,
        BC2,
        BC3
    };

    enum class MemoryFormat {
        R8,
        R16,
        R16G16,
        R16G16B16A16,
        R8G8_SNORM,
        RGBA8_SNORM,
        R16G16_SNORM,
        R16_FLOAT,
        R16G16_FLOAT,
        R16G16B16A16_FLOAT,
        R32_FLOAT,
        R32G32B32A32_FLOAT,
        D16,
        D24_S8,
        R10G10B10A2
    };

    enum class AddressingMode {
        CLAMP = GL_CLAMP_TO_EDGE,
        REPEAT = GL_REPEAT
    };

    class Texture {
        GLuint mTexture;

        void initParameters(bool withLayers);

        void loadFromMemoryWithLayersRaw(const MemoryFormat& format, uint32_t width, uint32_t height, const std::vector<std::vector<uint8_t>>& data);

    public:
        Texture();

        ~Texture();

        void loadFromMemoryARGB(uint32_t width, uint32_t height, const std::vector<uint8_t> &data);

        void loadFromMemoryARGBWithLayers(uint32_t width, uint32_t height,
                                          const std::vector<std::vector<uint8_t>> &dataLayers);

        void loadFromMemoryDXT(const DxtImageFormat &format, uint32_t width, uint32_t height,
                               const std::vector<uint8_t> &data);

        void loadFromMemoryDXTWithLayers(const DxtImageFormat &format, uint32_t width, uint32_t height,
                                         const std::vector<std::vector<uint8_t>> &data);

        template<typename T>
        void loadFromMemoryWithLayers(const MemoryFormat& format, uint32_t width, uint32_t height,
                const std::vector<std::vector<T>>& data) {
            std::vector<std::vector<uint8_t>> layers;
            for(const auto& layer : data) {
                std::vector<uint8_t> tmp(layer.size() * sizeof(T));
                memcpy(tmp.data(), layer.data(), tmp.size());
                layers.emplace_back(tmp);
            }

            loadFromMemoryWithLayersRaw(format, width, height, layers);
        }

        void setLinearFiltering(bool withLayers);
        void setAddressingMode(const AddressingMode& mode);

        void bind();
    };

    typedef std::shared_ptr<Texture> TexturePtr;
}


#endif //WILDSTAR_STUDIO_TEXTURE_H
