#include "Buffers.h"

namespace gl {
    Buffer::Buffer(GLenum type) : mType(type) {
        glGenBuffers(1, &mBuffer);
    }

    Buffer::~Buffer() {
        glDeleteBuffers(1, &mBuffer);
    }

    void Buffer::bind() {
        glBindBuffer(mType, mBuffer);
    }

    void Buffer::setData(const void *data, const std::size_t size) {
        bind();
        glBufferData(mType, size, data, GL_STATIC_DRAW);
    }
}