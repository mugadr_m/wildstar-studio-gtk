
#include "Texture.h"

namespace gl {

    Texture::Texture() {
        glGenTextures(1, &mTexture);
    }

    Texture::~Texture() {
        glDeleteTextures(1, &mTexture);
    }

    void Texture::loadFromMemoryARGB(uint32_t width, uint32_t height, const std::vector<uint8_t> &data) {
        bind();
        initParameters(false);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data.data());
    }

    void Texture::initParameters(bool withLayers) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        if (withLayers) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        } else {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        }
    }

    void Texture::bind() {
        glBindTexture(GL_TEXTURE_2D, mTexture);
    }

    void Texture::loadFromMemoryARGBWithLayers(uint32_t width, uint32_t height,
                                               const std::vector<std::vector<uint8_t>> &dataLayers) {
        bind();
        initParameters(true);

        for (auto i = 0u; i < dataLayers.size(); ++i) {
            const auto w = std::max(1u, width >> i);
            const auto h = std::max(1u, height >> i);

            glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, dataLayers[i].data());
        }
    }

    const GLenum dxt_texture_format_mapping[] = {
            GL_COMPRESSED_RGB_S3TC_DXT1_EXT,
            GL_COMPRESSED_RGBA_S3TC_DXT3_EXT,
            GL_COMPRESSED_RGBA_S3TC_DXT5_EXT
    };

    void Texture::loadFromMemoryDXT(const DxtImageFormat &format, uint32_t width, uint32_t height,
                                    const std::vector<uint8_t> &data) {
        bind();
        initParameters(false);

        glCompressedTexImage2D(GL_TEXTURE_2D, 0, dxt_texture_format_mapping[(uint32_t) format], width, height,
                               0, static_cast<GLsizei>(data.size()), data.data());
    }

    void Texture::loadFromMemoryDXTWithLayers(const DxtImageFormat &format, uint32_t width, uint32_t height,
                                              const std::vector<std::vector<uint8_t>> &data) {
        bind();
        initParameters(true);

        const auto texFormat = dxt_texture_format_mapping[(uint32_t) format];

        for (auto i = 0u; i < data.size(); ++i) {
            const auto w = std::max(1u, width >> i);
            const auto h = std::max(1u, height >> i);

            glCompressedTexImage2D(GL_TEXTURE_2D, 0, texFormat, w, h, 0, static_cast<GLsizei>(data[i].size()),
                                   data[i].data());
        }

    }

    void Texture::setLinearFiltering(bool withLayers) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        if (withLayers) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        } else {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        }
    }

    void Texture::loadFromMemoryWithLayersRaw(const MemoryFormat &format, uint32_t width, uint32_t height, const std::vector<std::vector<uint8_t>>& data) {
        bind();
        initParameters(true);

        for(auto i = 0u; i < data.size(); ++i) {
            const auto w = std::max(1u, width >> i);
            const auto h = std::max(1u, height >> i);

            switch(format) {
                case MemoryFormat::R8: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RED, w, h, 0, GL_R8, GL_UNSIGNED_BYTE, data[i].data());
                    break;
                }

                case MemoryFormat::R16: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_R16, w, h, 0, GL_RED, GL_UNSIGNED_SHORT, data[i].data());
                    break;
                }

                case MemoryFormat::R16G16: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RG16, w, h, 0, GL_RG, GL_UNSIGNED_SHORT, data[i].data());
                    break;
                }

                case MemoryFormat::R16G16B16A16: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA16, w, h, 0, GL_RGBA, GL_UNSIGNED_SHORT, data[i].data());
                    break;
                }

                case MemoryFormat::R8G8_SNORM: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RG8_SNORM, w, h, 0, GL_RG, GL_UNSIGNED_BYTE, data[i].data());
                    break;
                }

                case MemoryFormat::RGBA8_SNORM: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA8_SNORM, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data[i].data());
                    break;
                }

                case MemoryFormat::R16G16_SNORM: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RG16_SNORM, w, h, 0, GL_RG, GL_UNSIGNED_SHORT, data[i].data());
                    break;
                }

                case MemoryFormat::R16_FLOAT: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_R16F, w, h, 0, GL_RED, GL_HALF_FLOAT, data[i].data());
                    break;
                }

                case MemoryFormat::R16G16_FLOAT: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RG16F, w, h, 0, GL_RG, GL_HALF_FLOAT, data[i].data());
                    break;
                }

                case MemoryFormat::R16G16B16A16_FLOAT: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA16F, w, h, 0, GL_RGBA, GL_HALF_FLOAT, data[i].data());
                    break;
                }

                case MemoryFormat::R32_FLOAT: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_R32F, w, h, 0, GL_RED, GL_FLOAT, data[i].data());
                    break;
                }

                case MemoryFormat::R32G32B32A32_FLOAT: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA32F, w, h, 0, GL_RGBA, GL_FLOAT, data[i].data());
                    break;
                }

                case MemoryFormat::R10G10B10A2: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_INT_10_10_10_2, data[i].data());
                    break;
                }

                case MemoryFormat::D16: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_DEPTH_COMPONENT16, w, h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_SHORT, data[i].data());
                    break;
                }

                case MemoryFormat::D24_S8: {
                    glTexImage2D(GL_TEXTURE_2D, i, GL_DEPTH24_STENCIL8, w, h, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, data[i].data());
                    break;
                }

                default: {
                    continue;
                }
            }
        }
    }

    void Texture::setAddressingMode(const AddressingMode &mode) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, (GLenum) mode);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, (GLenum) mode);
    }
}