
#include "Mesh.h"
#include "log.h"
#include <sstream>

namespace gl {
    VertexElement::VertexElement(const std::string &semantic, uint32_t index, uint32_t components, DataType dataType, bool doNormalize) :
            mSemantic(semantic), mIndex(index), mComponents(components), mDataType(dataType), mDoNormalize(doNormalize) {

    }

    std::size_t VertexElement::initialize(const std::shared_ptr<Program> &program, std::size_t offset) {
        std::stringstream nameStream;
        nameStream << mSemantic << mIndex;

        const auto attrib = program->getAttribute(nameStream.str());
        if (attrib < 0) {
            log_console->error("Unable to find attribute in program: {}", nameStream.str());
            throw ::utils::RuntimeError("Unable to find attribute");
        }

        mAttributeIndex = static_cast<uint32_t>(attrib);
        mOffset = offset;

        switch (mDataType) {
            case DataType::BYTE: {
                return mComponents;
            }

            case DataType::FLOAT: {
                return mComponents * sizeof(float);
            }

            default: {
                return mComponents * 4;
            }
        }
    }

    void VertexElement::bind(std::size_t stride) {
        glEnableVertexAttribArray(mAttributeIndex);
        glVertexAttribPointer(mAttributeIndex, mComponents,
                              static_cast<GLenum>(mDataType), static_cast<GLboolean>(mDoNormalize),
                              static_cast<GLsizei>(stride), reinterpret_cast<const void *>(mOffset));
    }

    void Mesh::finalize() {
        if (mProgram == nullptr) {
            log_console->error("Unable to finalize mesh without a program");
            throw ::utils::RuntimeError("Unable to finalize mesh without a program");
        }

        mStride = 0;
        for (auto &element : mElements) {
            mStride += element.initialize(mProgram, mStride);
        }
    }

    void Mesh::startDraw() {
        glBindVertexArray(mVertexAttribObject);
        mVertexBuffer->bind();
        mIndexBuffer->bind();

        mProgram->bind();

        for (auto &element : mElements) {
            element.bind(mStride);
        }
    }

    void Mesh::draw() {
        if(mStartVertex == 0) {
            glDrawElements(GL_TRIANGLES, mIndexCount, static_cast<GLenum>(mIndexBuffer->getIndexType()),
                           nullptr);
        } else {
            glDrawElementsBaseVertex(GL_TRIANGLES, mIndexCount, static_cast<GLenum>(mIndexBuffer->getIndexType()),
                    nullptr, mStartVertex);
        }
    }

    Mesh::Mesh() {
        glGenVertexArrays(1, &mVertexAttribObject);
    }
}
