
#ifndef WILDSTAR_STUDIO_PROGRAM_H
#define WILDSTAR_STUDIO_PROGRAM_H

#include <string>
#include <stdexcept>
#include <memory>
#include <glm/ext.hpp>
#include "glad/glad.h"
#include "glm/glm.hpp"
#include "utils/RuntimeError.h"

namespace gl {
    class Program {
        GLuint mProgram;
        GLuint mVertexShader;
        GLuint mFragmentShader;

        void compileShader(const std::string &code, GLuint shader);

    public:
        Program();

        void compileVertexShader(const std::string &code) {
            compileShader(code, mVertexShader);
        }

        void compileFragmentShader(const std::string &code) {
            compileShader(code, mFragmentShader);
        }

        void link();

        void bind();

        int32_t getUniform(const std::string &name) const;

        int32_t getAttribute(const std::string &name) const;

#define BIND_FUNCTION(typeName, paramType) \
        void set##typeName(const std::string& name, const paramType& value) { \
            set##typeName(getUniform(name), value); \
        } \
        \
        void set##typeName(int32_t index, const paramType& value)

#define VALID_INDEX \
    if(index < 0) { \
        throw ::utils::RuntimeError("Unable to set uniform, index is < 0");\
    }

#define UNI_PROLOG \
    VALID_INDEX \
    bind()

        BIND_FUNCTION(Int, int) {
            UNI_PROLOG;
            glUniform1i(index, value);
        }

        BIND_FUNCTION(Float, float) {
            UNI_PROLOG;
            glUniform1f(index, value);
        }

        BIND_FUNCTION(Vector2, glm::vec2) {
            UNI_PROLOG;
            glUniform2fv(index, 1, glm::value_ptr(value));
        }

        BIND_FUNCTION(Vector3, glm::vec3) {
            UNI_PROLOG;
            glUniform3fv(index, 1, glm::value_ptr(value));
        }

        BIND_FUNCTION(Vector4, glm::vec4) {
            UNI_PROLOG;
            glUniform4fv(index, 1, glm::value_ptr(value));
        }

        BIND_FUNCTION(Matrix, glm::mat4) {
            UNI_PROLOG;
            glUniformMatrix4fv(index, 1, GL_FALSE, glm::value_ptr(value));
        }

#undef UNI_PROLOG
#undef VALID_INDEX
#undef BIND_FUNCTION
    };

    typedef std::shared_ptr<Program> ProgramPtr;
}


#endif //WILDSTAR_STUDIO_PROGRAM_H
