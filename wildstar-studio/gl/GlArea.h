
#ifndef WILDSTAR_STUDIO_GLAREA_H
#define WILDSTAR_STUDIO_GLAREA_H

#include <gtk/gtk.h>
#include <functional>
#include <chrono>
#include "glm/glm.hpp"

namespace gl {
    class GlArea {
        GtkWidget* mControl;
        std::function<void()> mOnFrameCallback;
        std::function<void()> mOnLoadCallback;

        glm::mat4 mOrthoMatrix;
        glm::mat4 mPerspectiveMatrix;

        std::chrono::steady_clock::time_point mLastFrame = std::chrono::steady_clock::now();
        bool mIsVisible = false;

        void onFrame(GdkGLContext* context);
        void onRealize(GtkGLArea* area);
        void onResize(uint32_t width, uint32_t height);

    public:
        explicit GlArea(GtkWidget* glArea);

        void setOnFrameCallback(const std::function<void()>& callback) {
            mOnFrameCallback = callback;
        }

        void setOnLoadCallback(const std::function<void()>& callback) {
            mOnLoadCallback = callback;
        }

        const glm::mat4& getOrthoMatrix() const {
            return mOrthoMatrix;
        }

        const glm::mat4& getPerspectiveMatrix() const {
            return mPerspectiveMatrix;
        }

        void onTick();

        void setVisible(bool isVisible) {
            mIsVisible = isVisible;
        }
    };
}

#endif //WILDSTAR_STUDIO_GLAREA_H
