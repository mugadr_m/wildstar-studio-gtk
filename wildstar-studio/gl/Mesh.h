
#ifndef WILDSTAR_STUDIO_MESH_H
#define WILDSTAR_STUDIO_MESH_H

#include "glad/glad.h"
#include <string>
#include <memory>
#include <vector>
#include "Program.h"
#include "Buffers.h"

namespace gl {
    enum class DataType {
        FLOAT = GL_FLOAT,
        BYTE = GL_UNSIGNED_BYTE
    };

    class VertexElement {
        std::string mSemantic;
        uint32_t mIndex;
        uint32_t mComponents;
        DataType mDataType;
        bool mDoNormalize;

        uint32_t mAttributeIndex;
        std::size_t mOffset;

    public:
        VertexElement(const std::string &semantic, uint32_t index, uint32_t components, DataType dataType = DataType::FLOAT, bool doNormalize = false);

        std::size_t initialize(const std::shared_ptr<Program> &program, std::size_t offset);

        void bind(std::size_t stride);
    };

    class Mesh {
        std::vector<VertexElement> mElements;
        std::size_t mStride;

        ProgramPtr mProgram;

        VertexBufferPtr mVertexBuffer = std::make_shared<VertexBuffer>();
        IndexBufferPtr mIndexBuffer = std::make_shared<IndexBuffer>(IndexType::SHORT);

        GLuint mVertexAttribObject;

        uint32_t mIndexCount = 0;
        uint32_t mStartVertex = 0;

    public:
        Mesh();

        void addElement(const std::string &semantic, uint32_t index, uint32_t components, DataType dataType = DataType::FLOAT,
                        bool doNormalize = false) {
            addElement(VertexElement(semantic, index, components, dataType, doNormalize));
        }

        void addElement(const VertexElement &element) {
            mElements.emplace_back(element);
        }

        void setProgram(const ProgramPtr& program) {
            mProgram = program;
        }

        void setIndexCount(uint32_t indexCount) {
            mIndexCount = indexCount;
        }

        void setStartVertex(uint32_t startVertex) {
            mStartVertex = startVertex;
        }

        void setVertexBuffer(const VertexBufferPtr& vertexBuffer) {
            mVertexBuffer = vertexBuffer;
        }

        void setIndexBuffer(const IndexBufferPtr& indexBuffer) {
            mIndexBuffer = indexBuffer;
        }

        void finalize();

        void startDraw();
        void draw();

        IndexBufferPtr getIndexBuffer() const {
            return mIndexBuffer;
        }

        VertexBufferPtr getVertexBuffer() const {
            return mVertexBuffer;
        }

        ProgramPtr getProgram() const {
            return mProgram;
        }
    };

    typedef std::shared_ptr<Mesh> MeshPtr;
};


#endif //WILDSTAR_STUDIO_MESH_H
