
#include "Program.h"
#include <memory>
#include <cstring>
#include "log.h"

namespace gl {
    Program::Program() {
        mProgram = glCreateProgram();
        mVertexShader = glCreateShader(GL_VERTEX_SHADER);
        mFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        glAttachShader(mProgram, mVertexShader);
        glAttachShader(mProgram, mFragmentShader);
    }

    void Program::compileShader(const std::string &code, GLuint shader) {
        struct CharFree {
            void operator()(char *ptr) {
                free(ptr);
            }
        };

        std::unique_ptr<char, CharFree> codePtr(strdup(code.c_str()));
        const auto strPtr = codePtr.get();

        auto codeLen = static_cast<GLint>(code.length());
        glShaderSource(shader, 1, &strPtr, &codeLen);
        glCompileShader(shader);

        GLint compileStatus = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compileStatus);
        if (compileStatus != GL_TRUE) {
            std::string compileError = "<unknown error>";
            GLint shaderLogSize = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &shaderLogSize);
            if(shaderLogSize > 0) {
                std::vector<char> shaderLog(static_cast<unsigned long>(shaderLogSize));
                glGetShaderInfoLog(shader, shaderLogSize, &shaderLogSize, shaderLog.data());
                if(shaderLogSize > 0) {
                    shaderLog.push_back('\0');
                    compileError = std::string(shaderLog.data());
                }
            }

            log_console->error("Unable to compile shader: {}", compileError);
            throw ::utils::RuntimeError("Unable to compile shader");
        }
    }

    void Program::link() {
        glLinkProgram(mProgram);
        GLint linkStatus = 0;
        glGetProgramiv(mProgram, GL_LINK_STATUS, &linkStatus);
        if(linkStatus != GL_TRUE) {
            std::string linkError = "<unknown error>";
            GLint programLogSize = 0;
            glGetProgramiv(mProgram, GL_INFO_LOG_LENGTH, &programLogSize);
            if(programLogSize > 0) {
                std::vector<char> programLog(static_cast<unsigned long>(programLogSize));
                glGetProgramInfoLog(mProgram, programLogSize, &programLogSize, programLog.data());
                if(programLogSize > 0) {
                    programLog.push_back('\0');
                    linkError = std::string(programLog.data());
                }
            }

            log_console->error("Unable to link program: {}", linkError);
            throw ::utils::RuntimeError("Unable to link program");
        }
    }

    void Program::bind() {
        static auto activeProgram = static_cast<GLuint>(-1);
        if(mProgram == activeProgram) {
            return;
        }

        glUseProgram(mProgram);
        activeProgram = mProgram;
    }

    int32_t Program::getUniform(const std::string &name) const {
        return glGetUniformLocation(mProgram, name.c_str());
    }

    int32_t Program::getAttribute(const std::string &name) const {
        return glGetAttribLocation(mProgram, name.c_str());
    }

}
