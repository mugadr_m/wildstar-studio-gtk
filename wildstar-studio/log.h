
#ifndef WILDSTAR_STUDIO_LOG_H
#define WILDSTAR_STUDIO_LOG_H

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

extern std::shared_ptr<spdlog::logger> log_console;

#endif //WILDSTAR_STUDIO_LOG_H
