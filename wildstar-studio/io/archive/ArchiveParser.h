
#ifndef WILDSTAR_STUDIO_ARCHIVEPARSER_H
#define WILDSTAR_STUDIO_ARCHIVEPARSER_H

#include "io/utils/FileReader.h"
#include "Structures.h"
#include "io/utils/BinaryReader.h"

namespace io::archive {
    class ArchiveParser {
        utils::FileReader mReader;

        std::vector<PackDirectoryHeader> mDirectoryHeaders;
        std::vector<FileTableEntry> mFileTable;

    public:
        explicit ArchiveParser(utils::FileReader fileReader);

        void parse();

        utils::BinaryReader openFile(const uint8_t* fileHash, uint32_t flags, uint64_t uncompressedSize, uint64_t compressedSize);
    };
}

#endif //WILDSTAR_STUDIO_ARCHIVEPARSER_H
