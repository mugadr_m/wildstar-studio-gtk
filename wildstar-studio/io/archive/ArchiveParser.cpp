
#include "ArchiveParser.h"
#include "log.h"
#include "io/compression/LzmaStream.h"
#include "io/compression/ZlibStream.h"
#include <cstdint>
#include <utils/RuntimeError.h>

namespace io::archive {
    const uint32_t ARCHIVE_FILE_MAGIC = 0x5041434B;

    ArchiveParser::ArchiveParser(utils::FileReader reader) : mReader(std::move(reader)) {

    }

    void ArchiveParser::parse() {
        mReader.seek(0);
        const auto signature = mReader.read<uint32_t>();
        const auto version = mReader.read<uint32_t>();

        if (signature != ARCHIVE_FILE_MAGIC) {
            log_console->error("Invalid archive file. Expected signature {:x} but got {:x}", ARCHIVE_FILE_MAGIC, signature);
            throw ::utils::RuntimeError("Invalid archive file");
        }

        if (version != 1) {
            log_console->error("Invalid archive file. Expected version 1 but got {}", version);
            throw ::utils::RuntimeError("Invalid archive file");
        }

        mReader.seekMod(528);
        const auto packHeaderOffset = mReader.read<uint64_t>();
        const auto packHeaderCount = mReader.read<uint32_t>();
        mReader.seekMod(4);
        const auto rootBlock = mReader.read<uint32_t>();

        log_console->info("Reading {} table entries @ {}", packHeaderCount, packHeaderOffset);

        mReader.seek(packHeaderOffset);
        mDirectoryHeaders.resize(packHeaderCount);
        mReader.read(mDirectoryHeaders);

        log_console->info("Reading archive info from block {}", rootBlock);

        const auto &rootBlockInfo = mDirectoryHeaders[rootBlock];
        if (rootBlockInfo.blockSize != sizeof(AARC)) {
            log_console->error("Invalid archive file. Root block must be {} bytes, was {}", sizeof(AARC), rootBlockInfo.blockSize);
            throw ::utils::RuntimeError("Invalid archive file");
        }

        mReader.seek(rootBlockInfo.directoryOffset);
        const auto aarc = mReader.read<AARC>();
        mFileTable.resize(aarc.tableEntryCount);
        if (mFileTable.empty()) {
            return;
        }

        log_console->info("Reading file table with {} entries from block {}", aarc.tableEntryCount, aarc.tableBlock);
        const auto &tableBlock = mDirectoryHeaders[aarc.tableBlock];
        mReader.seek(tableBlock.directoryOffset);
        mReader.read(mFileTable);
    }

    utils::BinaryReader ArchiveParser::openFile(const uint8_t *fileHash, uint32_t flags, uint64_t uncompressedSize, uint64_t compressedSize) {
        for(const auto& entry : mFileTable) {
            if(memcmp(entry.fileHash, fileHash, 20) != 0) {
                continue;
            }

            const auto& block = mDirectoryHeaders[entry.blockIndex];
            std::vector<uint8_t> compressedData(block.blockSize);
            mReader.seek(block.directoryOffset);
            mReader.read(compressedData);

            if((flags & 0x04) != 0) {
                return compression::LzmaStream::decompress(compressedData, uncompressedSize);
            } else if((flags & 0x02) != 0) {
                return compression::ZlibStream::decompress(compressedData, uncompressedSize);
            } else {
                return utils::BinaryReader(compressedData);
            }
        }

        throw ::utils::RuntimeError("File not found");
    }
}