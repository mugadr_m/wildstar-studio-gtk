
#ifndef WILDSTAR_STUDIO_INDEXPARSER_H
#define WILDSTAR_STUDIO_INDEXPARSER_H


#include "io/utils/BinaryReader.h"
#include "Structures.h"
#include <string>
#include <memory>
#include <map>
#include <functional>
#include <gtk/gtk.h>
#include <sstream>
#include <utils/Utilities.h>
#include <utils/RuntimeError.h>

namespace io::archive {
    class DirectoryEntry;

    class FileSystemEntry : public std::enable_shared_from_this<FileSystemEntry> {
        bool mIsDirectory;
        std::string mName;
        std::shared_ptr<DirectoryEntry> mParent;

    protected:
        explicit FileSystemEntry(const std::string &name, std::shared_ptr<DirectoryEntry> parent, bool isDirectory = false);

        virtual ~FileSystemEntry() {}

    public:
        std::shared_ptr<DirectoryEntry> getParent() const {
            return mParent;
        }

        const std::string &getName() const {
            return mName;
        }

        std::string getFullPath() const;

        bool isDirectory() const {
            return mIsDirectory;
        }

        virtual uint32_t countChildren() const = 0;
    };

    typedef std::shared_ptr<FileSystemEntry> FileSystemEntryPtr;

    class FileEntry : public FileSystemEntry {
        uint32_t mFlags;
        uint64_t mUncompressedSize;
        uint64_t mCompressedSize;
        uint8_t mFileHash[20];

    public:
        FileEntry(const std::string &name, std::shared_ptr<DirectoryEntry> parent, std::vector<uint8_t> fileData);

        uint64_t getUncompressedSize() const {
            return mUncompressedSize;
        }

        uint64_t getCompressedSize() const {
            return mCompressedSize;
        }

        const uint8_t *getFileHash() const {
            return mFileHash;
        }

        const uint32_t getFlags() const {
            return mFlags;
        }

        uint32_t countChildren() const override {
            return 1;
        }
    };

    typedef std::shared_ptr<FileEntry> FileEntryPtr;

    class DirectoryEntry : public FileSystemEntry {
        uint32_t mEntryBlock;
        bool mIsRoot = false;

        std::map<std::string, FileSystemEntryPtr> mChildren;

        std::string getFileIconName(const std::string &fileName) const;

    public:
        DirectoryEntry(const std::string &name, std::shared_ptr<DirectoryEntry> parent, uint32_t entryBlock);

        void parseChildren(GtkTreeStore *model, GtkTreeIter *parentIter, utils::BinaryReader &reader, const std::vector<PackDirectoryHeader> &directories, const std::function<void()> &fileLoadedCallback);

        void makeRoot() {
            mIsRoot = true;
        }

        bool isRoot() const {
            return mIsRoot;
        }

        std::vector<FileSystemEntryPtr> getChildren() const;

        uint32_t countChildren() const override {
            uint32_t numChildren = 0;
            for(const auto& pair : mChildren) {
                numChildren += pair.second->countChildren();
            }

            return numChildren;
        }

        FileSystemEntryPtr openByPath(const std::vector<std::string>& pathParts) {
            if(pathParts.empty()) {
                return nullptr;
            }

            auto startItr = pathParts.begin();
            std::vector<std::string> rest(startItr + 1, pathParts.end());
            for(const auto& pair : mChildren) {
                if(::utils::isEqualIgnoreCase(pair.second->getName(), *startItr)) {
                    if(rest.empty()) {
                        return pair.second;
                    } else {
                        if(!pair.second->isDirectory()) {
                            throw ::utils::RuntimeError("Path does not terminate on a file");
                        }

                        return std::dynamic_pointer_cast<DirectoryEntry>(pair.second)->openByPath(rest);
                    }
                }
            }

            throw ::utils::RuntimeError("Path does not exist");
        }
    };

    typedef std::shared_ptr<DirectoryEntry> DirectoryEntryPtr;

    class IndexParser {
        utils::BinaryReader mReader;

        bool mIndexRootLoaded = false;
        AIDX mIndexRoot;
        uint32_t mBuild = 0;
        std::vector<PackDirectoryHeader> mDirectoryHeaders;

        std::shared_ptr<DirectoryEntry> mFileSystemRoot;

        void loadIndexRoot();

        void loadDirectoryHeaders(uint64_t tableStart, uint32_t numEntries);

        void parseHeader(uint64_t &tableStart, uint32_t &numEntries, uint32_t &indexBlock);

        uint64_t countDirectory(uint64_t dirBlock);

    public:
        explicit IndexParser(utils::BinaryReader reader);

        uint64_t countFiles();

        void parseFileTree(GtkTreeStore *store, const std::function<void()> &fileLoadedCallback);

        std::shared_ptr<DirectoryEntry> getRoot() const {
            return mFileSystemRoot;
        }
    };
}


#endif //WILDSTAR_STUDIO_INDEXPARSER_H
