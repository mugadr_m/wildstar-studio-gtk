#include "FileManager.h"
#include "IndexParser.h"
#include <iostream>
#include <thread>
#include <sstream>
#include <fstream>
#include <gtk/gtk.h>
#include <cstdio>
#include "log.h"

namespace io::archive {

    std::shared_ptr<FileManager> FileManager::instance() {
        static std::shared_ptr<FileManager> instance = std::make_shared<FileManager>();
        return instance;
    }

    void FileManager::initFromLocal(const std::string &folder,
                                    const std::function<void(const std::string &)> &messageCallback,
                                    const std::function<void(float)> &progressCallback,
                                    const std::function<void(bool, GtkTreeStore *)> &callback) {
        std::thread([this, folder, callback, messageCallback, progressCallback]() {
            messageCallback("Loading ClientData.index");

            std::stringstream strm;
            strm << folder << "/ClientData.index";
            log_console->info("Loading index file from {}", strm.str());

            std::vector<uint8_t> indexData;
            try {
                indexData = readFile(strm.str());
            } catch (::utils::RuntimeError &err) {
                log_console->warn("Unable to open file {}: {}", strm.str(), err.what());
                messageCallback("Unable to open file");
                callback(false, nullptr);
                return;
            }

            messageCallback("Parsing ClientData.index");
            utils::BinaryReader indexReader(indexData);
            GtkTreeStore *treeStore;
            if (!loadIndexFile(indexReader, messageCallback, progressCallback, treeStore)) {
                callback(false, nullptr);
                return;
            }

            strm.str("");
            strm << folder << "/ClientData.archive";
            log_console->info("Loading archive file from {}", strm.str());
            auto archiveFile = fopen64(strm.str().c_str(), "rb");
            if(archiveFile == nullptr) {
                log_console->warn("Unable to open file {}", strm.str());
                messageCallback("Unable to open file");
                callback(false, nullptr);
                return;
            }

            mArchiveParser = std::make_shared<ArchiveParser>(utils::FileReader(archiveFile));
            try {
                mArchiveParser->parse();
            } catch (::utils::RuntimeError& err) {
                log_console->warn("Unable to parse archive file: {}", std::string(err.what()));
                messageCallback("Unable to parse file");
                callback(false, nullptr);
                return;
            }

            mFileOpenFunction = [this](auto hash, auto flags, auto uncompressedSize, uint64_t compressedSize) {
                return mArchiveParser->openFile(hash, flags, uncompressedSize, compressedSize);
            };

            progressCallback(1.0f); // Looks much better, just in case :)
            messageCallback("Done");
            callback(true, treeStore);
        }).detach();
    }

    bool FileManager::loadIndexFile(utils::BinaryReader &reader,
                                    const std::function<void(const std::string &)> &messageCallback,
                                    const std::function<void(float)> &progressCallback, GtkTreeStore *&treeModel) {
        messageCallback("Counting files...");
        try {
            IndexParser indexParser(reader);
            const auto numFiles = indexParser.countFiles();

            messageCallback("Parsing file tree");
            uint64_t filesParsed = 0;

            const auto model = gtk_tree_store_new(3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER);
            indexParser.parseFileTree(model, [&filesParsed, numFiles, progressCallback]() {
                ++filesParsed;
                const auto pct = static_cast<float>(filesParsed) / numFiles;
                progressCallback(pct);
            });

            treeModel = model;
            mFileSystemRoot = indexParser.getRoot();
        } catch (::utils::RuntimeError &e) {
            log_console->warn("Unable to parse index file: {}", std::string(e.what()));
            messageCallback(e.what());
            return false;
        }

        return true;
    }

    std::vector<uint8_t> FileManager::readFile(const std::string &file) {
        const auto fp = fopen(file.c_str(), "rb");
        if(fp == nullptr) {
            throw ::utils::RuntimeError("Unable to open file");
        }

        std::unique_ptr<FILE, ::utils::FileCloser> ptr(fp);
        fseek(fp, 0, SEEK_END);

        const auto size = static_cast<std::size_t>(ftell(fp));
        fseek(fp, 0, SEEK_SET);
        std::vector<uint8_t> fileData(size);
        if (fread(fileData.data(), 1, size, fp) != size) {
            throw ::utils::RuntimeError("Unable to read index file");
        }

        return fileData;
    }

    utils::BinaryReader FileManager::openFile(const std::shared_ptr<FileEntry> &file) {
        std::lock_guard<std::mutex> l(mFileOpenLock);
        try {
            return mFileOpenFunction(file->getFileHash(), file->getFlags(), file->getUncompressedSize(), file->getCompressedSize());
        } catch (::utils::RuntimeError& err) {
            log_console->warn("Unable to open file in archive: {}", file->getFullPath());
            throw err;
        }
    }

    utils::BinaryReader FileManager::openFile(const std::string &path) {
        std::vector<std::string> pathParts;
        std::stringstream stream;
        stream << path;
        std::string part;

        const auto idxSlash = path.find('\\');
        const auto idxFwSlash = path.find('/');
        char delimiter;
        if(idxSlash != std::string::npos) {
            if(idxFwSlash == std::string::npos || idxSlash <= idxFwSlash) {
                delimiter = '\\';
            } else {
                delimiter = '/';
            }
        } else {
            delimiter = '/';
        }

        while(std::getline(stream, part, delimiter)) {
            pathParts.emplace_back(part);
        }

        auto entry = mFileSystemRoot->openByPath(pathParts);
        if(entry->isDirectory()) {
            throw ::utils::RuntimeError("Unable to open a directory");
        }

        return openFile(std::dynamic_pointer_cast<FileEntry>(entry));
    }
};