
#ifndef WILDSTAR_STUDIO_STRUCTURES_H
#define WILDSTAR_STUDIO_STRUCTURES_H


#pragma pack(push, 1)

#include <stdint.h>

namespace io::archive {
    struct PackDirectoryHeader {
        uint64_t directoryOffset;
        uint64_t blockSize;
    };

    struct AIDX {
        uint32_t magic;
        uint32_t version;
        uint32_t build;
        uint32_t rootBlock;
    };

    struct AARC {
        uint32_t magic;
        uint32_t version;
        uint32_t tableEntryCount;
        uint32_t tableBlock;
    };

    struct FileTableEntry {
        uint32_t blockIndex;
        uint8_t fileHash[20];
        uint64_t uncompressedSize;
    };

    struct AARCEntry {
        uint32_t blockIndex;
        uint8_t shaHash[20];
        uint64_t uncompressedSize;
    };

    struct DirectoryEntryHeader {
        uint32_t nameOffset;
        uint32_t directoryBlock;
    };
}

#pragma pack(pop)


#endif //WILDSTAR_STUDIO_STRUCTURES_H
