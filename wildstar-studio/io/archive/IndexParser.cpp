#include "IndexParser.h"
#include "log.h"
#include <iostream>
#include <iomanip>
#include <utils/Utilities.h>

namespace io::archive {
    static const uint32_t FILE_SIGNATURE = 0x5041434B;
    static const uint32_t AIDX_SIGNATURE = 0x41494458;

    IndexParser::IndexParser(utils::BinaryReader reader) : mReader(std::move(reader)) {

    }

    void IndexParser::loadDirectoryHeaders(uint64_t tableStart, uint32_t numEntries) {
        if (!mDirectoryHeaders.empty()) {
            return;
        }

        mReader.seek(tableStart);
        mDirectoryHeaders.resize(numEntries);
        mReader.read(mDirectoryHeaders);
    }

    void IndexParser::parseHeader(uint64_t &tableStart, uint32_t &numEntries, uint32_t &indexBlock) {
        mReader.seek(0);
        const auto signature = mReader.read<uint32_t>();
        const auto version = mReader.read<uint32_t>();

        if (signature != FILE_SIGNATURE) {
            log_console->error("Invalid index file. Signature mismatch: {:x} vs expected {:x}", signature,
                               FILE_SIGNATURE);
            throw ::utils::RuntimeError("Invalid file signature");
        }

        if (version != 1) {
            log_console->error("Invalid index file. Version mismatch: {} vs expected 1", version);
            throw ::utils::RuntimeError("Invalid file version");
        }

        mReader.seekMod(528);
        tableStart = mReader.read<uint64_t>();
        numEntries = mReader.read<uint32_t>();
        mReader.seekMod(4);
        indexBlock = mReader.read<uint32_t>();
    }

    uint64_t IndexParser::countFiles() {
        loadIndexRoot();
        return countDirectory(mIndexRoot.rootBlock);
    }

    uint64_t IndexParser::countDirectory(uint64_t dirBlock) {
        const auto &header = mDirectoryHeaders[dirBlock];
        mReader.seek(header.directoryOffset);
        const auto numDirs = mReader.read<uint32_t>();
        const auto numFiles = mReader.read<uint32_t>();

        std::vector<DirectoryEntryHeader> dirHeaders(numDirs);
        mReader.read(dirHeaders);
        uint64_t retVal = numFiles;
        for (const auto &dirHeader : dirHeaders) {
            retVal += countDirectory(dirHeader.directoryBlock);
        }

        return retVal;
    }

    void IndexParser::parseFileTree(GtkTreeStore *store, const std::function<void()> &fileLoadedCallback) {
        loadIndexRoot();
        mFileSystemRoot = std::make_shared<DirectoryEntry>("root", nullptr, mIndexRoot.rootBlock);
        mFileSystemRoot->makeRoot();
        mFileSystemRoot->parseChildren(store, nullptr, mReader, mDirectoryHeaders, fileLoadedCallback);
    }

    void IndexParser::loadIndexRoot() {
        if (mIndexRootLoaded) {
            return;
        }

        uint64_t tableStart = 0;
        uint32_t tableSize = 0, indexBlock = 0;
        parseHeader(tableStart, tableSize, indexBlock);
        log_console->info("Parsing index table at {}, {} entries in block {}", tableStart, tableSize, indexBlock);
        loadDirectoryHeaders(tableStart, tableSize);

        const auto &aidxHeader = mDirectoryHeaders.at(indexBlock);
        if (aidxHeader.blockSize != sizeof(AIDX)) {
            log_console->error(
                    "Invalid index file. Block pointed to by indexBlock field has wrong size: {} vs. expected {}",
                    aidxHeader.blockSize, sizeof(AIDX));
            throw ::utils::RuntimeError("Invalid index file");
        }

        mReader.seek(aidxHeader.directoryOffset);
        const auto aidx = mReader.read<AIDX>();
        if (aidx.magic != AIDX_SIGNATURE) {
            log_console->error("Invalid index file. Index block has wrong magic: {:x} vs expected {:x}", aidx.magic,
                               AIDX_SIGNATURE);
            throw ::utils::RuntimeError("Invalid index file");
        }

        mBuild = aidx.build;
        mIndexRoot = aidx;
        mIndexRootLoaded = true;
        log_console->info("Index root version {} @ block {}", mBuild, mIndexRoot.rootBlock);
    }

    FileSystemEntry::FileSystemEntry(const std::string &name, std::shared_ptr<DirectoryEntry> parent, bool isDirectory)
            : mName(name), mParent(std::move(parent)), mIsDirectory(isDirectory) {

    }

    std::string FileSystemEntry::getFullPath() const {
        if (mParent != nullptr && !mParent->isRoot()) {
            return mParent->getFullPath() + "\\" + mName;
        } else {
            return mName;
        }
    }

    DirectoryEntry::DirectoryEntry(const std::string &name, std::shared_ptr<DirectoryEntry> parent, uint32_t entryBlock)
            : FileSystemEntry(name, std::move(parent), true), mEntryBlock(entryBlock) {

    }

    void DirectoryEntry::parseChildren(GtkTreeStore *model, GtkTreeIter *parentIter, utils::BinaryReader &reader,
                                       const std::vector<PackDirectoryHeader> &directories,
                                       const std::function<void()> &fileLoadedCallback) {
        const auto &header = directories[mEntryBlock];
        reader.seek(header.directoryOffset);

        const auto numDirectories = reader.read<uint32_t>();
        const auto numFiles = reader.read<uint32_t>();
        const auto dataSize = numDirectories * 8 + numFiles * 56;

        const auto curPos = reader.tell();
        reader.seekMod(dataSize);
        const auto stringSize = header.blockSize - 8 - dataSize;
        std::vector<char> stringData(stringSize);
        reader.read(stringData);
        stringData.push_back('\0');
        reader.seek(curPos);

        std::vector<DirectoryEntryHeader> dirHeaders(numDirectories);
        reader.read(dirHeaders);

        std::vector<FileSystemEntryPtr> fileTree;

        auto _thisPtr = std::dynamic_pointer_cast<DirectoryEntry>(shared_from_this());

        for (auto i = 0u; i < numFiles; ++i) {
            const auto nameOffset = reader.read<uint32_t>();
            const auto fileName = std::string(&stringData[nameOffset]);
            std::vector<uint8_t> fileData(52);
            reader.read(fileData);
            const auto file = std::make_shared<FileEntry>(fileName, _thisPtr, fileData);
            mChildren.emplace(fileName, file);
            fileTree.push_back(file);
            fileLoadedCallback();
        }

        for (const auto &dirHeader : dirHeaders) {
            const auto &nameOffset = dirHeader.nameOffset;
            const auto dirName = std::string(&stringData[nameOffset]);
            const auto directory = std::make_shared<DirectoryEntry>(dirName, _thisPtr, dirHeader.directoryBlock);

            GtkTreeIter subDir;
            gtk_tree_store_append(model, &subDir, parentIter);
            gtk_tree_store_set(model, &subDir, 0, "folder", 1, dirName.c_str(), 2, directory.get(), -1);
            mChildren.emplace(dirName, directory);
            directory->parseChildren(model, &subDir, reader, directories, fileLoadedCallback);
        }

        for (const auto &file : fileTree) {
            GtkTreeIter fileItr;
            gtk_tree_store_append(model, &fileItr, parentIter);
            gtk_tree_store_set(model, &fileItr, 0, getFileIconName(file->getName()).c_str(), 1, file->getName().c_str(),
                               2, file.get(), -1);
        }
    }

    std::string DirectoryEntry::getFileIconName(const std::string &fileName) const {
        const auto extension = ::utils::getExtension(fileName);

        if (extension == ".wem") {
            return "audio-x-generic";
        } else if (extension == ".form" || extension == ".xml") {
            return "text-html";
        } else if (extension == ".lua") {
            return "text-x-script";
        } else if (extension == ".tex" || extension == ".jpg" || extension == ".jpeg" || extension == ".png" ||
                   extension == ".psd" || extension == ".txd") {
            return "image-x-generic";
        } else if (extension == ".tbl") {
            return "x-office-spreadsheet";
        } else if (extension == ".m3") {
            return "package-x-generic";
        } else if (extension == ".i3") {
            return "go-home";
        } else if (extension == ".area") {
            return "network-workgroup";
        } else if (extension == ".sky") {
            return "weather-few-clouds";
        } else {
            return "text-x-generic";
        }
    }

    std::vector<FileSystemEntryPtr> DirectoryEntry::getChildren() const {
        std::vector<FileSystemEntryPtr> ret;
        ret.reserve(mChildren.size());

        for(const auto& pair : mChildren) {
            ret.emplace_back(pair.second);
        }

        return ret;
    }

    FileEntry::FileEntry(const std::string &name, std::shared_ptr<DirectoryEntry> parent,
                         std::vector<uint8_t> fileData) : FileSystemEntry(name, parent, false) {
        utils::BinaryReader reader(std::move(fileData));
        mFlags = reader.read<uint32_t>();
        reader.seekMod(8);
        mUncompressedSize = reader.read<uint64_t>();
        mCompressedSize = reader.read<uint64_t>();
        reader.read(mFileHash, sizeof mFileHash);
    }
}


