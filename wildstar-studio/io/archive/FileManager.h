
#ifndef WILDSTAR_STUDIO_FILEMANAGER_H
#define WILDSTAR_STUDIO_FILEMANAGER_H

#include <memory>
#include <functional>
#include "io/utils/BinaryReader.h"
#include "io/archive/ArchiveParser.h"
#include "IndexParser.h"
#include <gtk/gtk.h>

#ifdef USE_CROSS_COMPILE
#include "cross_compile/mingw.mutex.h"
#else
#include <mutex>
#endif

namespace io::archive {
    class FileManager {
        std::shared_ptr<ArchiveParser> mArchiveParser;

        std::shared_ptr<DirectoryEntry> mFileSystemRoot;

        std::mutex mFileOpenLock;

        bool loadIndexFile(utils::BinaryReader &reader,
                           const std::function<void(const std::string &)> &messageCallback,
                           const std::function<void(float)> &progressCallback,
                           GtkTreeStore*& treeModel);

        std::vector<uint8_t> readFile(const std::string &file);

        std::function<utils::BinaryReader (const uint8_t*, uint32_t, uint64_t, uint64_t)> mFileOpenFunction;

    public:
        void initFromLocal(const std::string &folder,
                           const std::function<void(const std::string &)> &messageCallback,
                           const std::function<void(float)> &progressCallback,
                           const std::function<void(bool, GtkTreeStore*)> &callback);

        utils::BinaryReader openFile(const std::shared_ptr<FileEntry>& file);
        utils::BinaryReader openFile(const std::string& path);

        static std::shared_ptr<FileManager> instance();
    };

    typedef std::shared_ptr<FileManager> FileManagerPtr;
};


#endif //WILDSTAR_STUDIO_FILEMANAGER_H
