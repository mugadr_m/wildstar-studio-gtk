
#ifndef WILDSTAR_STUDIO_SETTINGSMANAGER_H
#define WILDSTAR_STUDIO_SETTINGSMANAGER_H

#include <memory>
#include <map>

namespace io::settings {
    enum class TableSaveMode {
        TBL = 0,
        CSV = 1,
        XLSX = 2,
        SQL = 3
    };

    class SettingsKeys {
    public:
        SettingsKeys() = delete;

        static const std::string OUTPUT_PATH;
        static const std::string HIGHLIGHT_CODE;
        static const std::string SAVE_TEXTURE_AS_PNG;
        static const std::string TBL_SAVE_MODE;
    };

    class SettingsManager {
        std::map<std::string, std::string> mSettings;

        void save();
        void load();

        void initDefault();

        void onBeforeLoad();

    public:
        SettingsManager() {
            onBeforeLoad();
        }

        ~SettingsManager();

        static std::shared_ptr<SettingsManager> instance();

        void validate();

        bool getBoolean(const std::string& key, bool defaultValue = false) const;
        std::string getString(const std::string& key, const std::string& defaultValue = "") const;
        int32_t getInt(const std::string& key, int32_t defaultValue = 0) const;

        template<typename T>
        T getEnum(const std::string& key, T defaultValue) const {
            return static_cast<T>(getInt(key, static_cast<int32_t>(defaultValue)));
        }

        void setString(const std::string& key, const std::string& value);
        void setBoolean(const std::string& key, bool value);
        void setInt(const std::string& key, int32_t value);

        template<typename T>
        void setEnum(const std::string& key, const T& value) {
            setInt(key, static_cast<int32_t>(value));
        }

        void saveAll() {
            save();
        }
    };
}


#endif //WILDSTAR_STUDIO_SETTINGSMANAGER_H
