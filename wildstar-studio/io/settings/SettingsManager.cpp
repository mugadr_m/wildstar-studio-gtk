
#include "SettingsManager.h"
#include <fstream>
#include "log.h"
#ifdef _WIN32
#include <direct.h>
#else
#include <unistd.h>
#endif
#include <iostream>
#include <utils/Utilities.h>

namespace io::settings {

    const std::string SettingsKeys::OUTPUT_PATH = "output.path";
    const std::string SettingsKeys::HIGHLIGHT_CODE = "code.highlight";
    const std::string SettingsKeys::SAVE_TEXTURE_AS_PNG = "output.save.texture.png";
    const std::string SettingsKeys::TBL_SAVE_MODE = "output.save.tbl.format";

    std::shared_ptr<SettingsManager> SettingsManager::instance() {
        static std::shared_ptr<SettingsManager> instance = std::make_shared<SettingsManager>();
        return instance;
    }

    SettingsManager::~SettingsManager() {
        saveAll();
    }

    void SettingsManager::load() {
        const auto fp = fopen("settings.conf", "r");
        if(fp == nullptr) {
            log_console->info("Settings.conf not found, generating default");
            return initDefault();
        }

        std::unique_ptr<FILE, ::utils::FileCloser> ptr(fp);
        char lineBuffer[10000]{};

        while(!feof(fp) && fgets(lineBuffer, sizeof lineBuffer, fp) != nullptr) {
            std::string line = lineBuffer;
            if(line.empty()) {
                continue;
            }

            line = utils::trim(line);

            if(line[0] == '#') {
                continue;
            }

            const auto idxEq = line.find('=');
            if(idxEq == std::string::npos) {
                log_console->warn("Ignoring line in settings.conf, does not have a '=' sign: {}", line);
                continue;
            }

            const auto key = line.substr(0, idxEq);
            const auto value = line.substr(idxEq + 1);
            mSettings[key] = value;
        }
    }

    void SettingsManager::initDefault() {
        std::string curDir;
        char curDirBuffer[2048]{};
#ifdef _WIN32
        curDir = _getcwd(curDirBuffer, sizeof curDirBuffer);
#else
        curDir = getcwd(curDirBuffer, sizeof curDirBuffer);
#endif
        mSettings[SettingsKeys::OUTPUT_PATH] = curDir;
        mSettings[SettingsKeys::HIGHLIGHT_CODE] = "1";
        mSettings[SettingsKeys::SAVE_TEXTURE_AS_PNG] = "1";

        save();
    }

    void SettingsManager::save() {
        const auto fp = fopen("settings.conf", "w");
        if(fp == nullptr) {
            log_console->warn("Unable to write settings.conf, could not open file: {}", errno);
            return;
        }

        std::unique_ptr<FILE, ::utils::FileCloser> ptr(fp);

        for(const auto& entry : mSettings) {
            fprintf(fp, "%s=%s\n", entry.first.c_str(), entry.second.c_str());
        }
    }

    void SettingsManager::validate() {
        load();
    }

    bool SettingsManager::getBoolean(const std::string &key, bool defaultValue) const {
        const auto itr = mSettings.find(key);
        if(itr == mSettings.end()) {
            return defaultValue;
        }
        return itr->second == "1";
    }

    std::string SettingsManager::getString(const std::string &key, const std::string &defaultValue) const {
        const auto itr = mSettings.find(key);
        if(itr == mSettings.end()) {
            return defaultValue;
        }

        return itr->second;
    }

    void SettingsManager::setString(const std::string &key, const std::string &value) {
        mSettings[key] = value;
    }

    void SettingsManager::setBoolean(const std::string &key, bool value) {
        mSettings[key] = value ? "1" : "0";
    }

    void SettingsManager::onBeforeLoad() {
        setString(SettingsKeys::OUTPUT_PATH, ".");
        setBoolean(SettingsKeys::HIGHLIGHT_CODE, true);
        setBoolean(SettingsKeys::SAVE_TEXTURE_AS_PNG, true);
        setEnum(SettingsKeys::TBL_SAVE_MODE, TableSaveMode::TBL);
    }

    void SettingsManager::setInt(const std::string &key, int32_t value) {
        mSettings[key] = std::to_string(value);
    }

    int32_t SettingsManager::getInt(const std::string &key, int32_t defaultValue) const {
        const auto itr = mSettings.find(key);
        if(itr == mSettings.end()) {
            return defaultValue;
        }

        return std::stoi(itr->second);
    }
}