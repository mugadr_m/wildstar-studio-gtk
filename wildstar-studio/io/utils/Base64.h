
#ifndef WILDSTAR_STUDIO_BASE64_H
#define WILDSTAR_STUDIO_BASE64_H

#include <string>

std::string base64_encode(const unsigned char *src, size_t len);


#endif //WILDSTAR_STUDIO_BASE64_H
