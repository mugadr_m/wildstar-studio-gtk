
#ifndef WILDSTAR_STUDIO_BINARYREADER_H
#define WILDSTAR_STUDIO_BINARYREADER_H

#include <vector>
#include <cstdint>
#include <string>

namespace io::utils {
    class BinaryReader {
        std::vector<uint8_t> mData;
        std::size_t mPosition = 0;

    public:
        BinaryReader() = default;
        explicit BinaryReader(std::vector<uint8_t> data);
        BinaryReader(const BinaryReader& other);
        BinaryReader(BinaryReader&& other) noexcept;

        BinaryReader& operator = (const BinaryReader& other);
        BinaryReader& operator = (BinaryReader&& other);

        std::size_t getSize() const {
            return mData.size();
        }

        void read(void* data, std::size_t numBytes);

        template<typename T>
        T read() {
            T ret;
            read(&ret, sizeof(T));
            return ret;
        }

        template<typename T>
        void read(T& value) {
            read(&value, sizeof(T));
        }

        template<typename T>
        void read(std::vector<T>& data) {
            read(data.data(), data.size() * sizeof(T));
        }

        std::string readString();
        std::string readStringUtf16();

        std::vector<uint8_t> readBytes(std::size_t numBytes) {
            std::vector<uint8_t> ret(numBytes);
            read(ret);
            return ret;
        }

        void seek(std::size_t position) {
            mPosition = position;
        }

        void seekMod(int32_t modSize) {
            mPosition += modSize;
        }

        std::size_t tell() const {
            return mPosition;
        }

        const std::vector<uint8_t>& getData() const {
            return mData;
        }

        uint32_t available() const {
            return static_cast<uint32_t>((mPosition > mData.size()) ? 0 : (mData.size() - mPosition));
        }
    };
}


#endif //WILDSTAR_STUDIO_BINARYREADER_H
