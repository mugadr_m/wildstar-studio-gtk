
#ifndef WILDSTAR_STUDIO_BINARYWRITER_H
#define WILDSTAR_STUDIO_BINARYWRITER_H

#include <vector>
#include <cstdint>
#include <cstring>
#include <string>

namespace io::utils {
    class BinaryWriter {
        std::vector<uint8_t> mData;
        std::size_t mPosition = 0;

        void needsUntil(const std::size_t& position) {
            if(position <= mData.size()) {
                return;
            }

            mData.resize(position);
        }

    public:
        BinaryWriter() = default;
        explicit BinaryWriter(std::vector<uint8_t> fullData);

        BinaryWriter& write(const void* data, std::size_t numBytes) {
            needsUntil(mPosition + numBytes);
            memcpy(mData.data() + mPosition, data, numBytes);
            mPosition += numBytes;
            return *this;
        }

        template<typename T>
        BinaryWriter& write(const T& value) {
            return write(&value, sizeof(T));
        }

        template<typename T>
        BinaryWriter& write(const std::vector<T>& values) {
            return write(values.data(), values.size() * sizeof(T));
        }

        void seek(const std::size_t& position) {
            mPosition = position;
        }

        template<typename T>
        BinaryWriter& operator << (const T& data) {
            return write(data);
        }

        template<typename T>
        BinaryWriter& operator << (const std::vector<T>& data) {
            return write(data);
        }

        BinaryWriter& writeText(const std::string& text) {
            write(text.c_str(), text.size());
            return *this;
        }

        const std::vector<uint8_t>& getFullData() const {
            return mData;
        }
    };
}

#endif //WILDSTAR_STUDIO_BINARYWRITER_H
