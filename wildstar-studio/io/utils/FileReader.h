
#ifndef WILDSTAR_STUDIO_FILEREADER_H
#define WILDSTAR_STUDIO_FILEREADER_H

#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string>

namespace io::utils {
    class FileReader {
        FILE* mFile;
        std::size_t mPosition;
        std::size_t mSize;

    public:
        explicit FileReader(FILE* file);
        FileReader(FileReader&) = delete;
        FileReader(FileReader&& other);

        ~FileReader();

        void operator = (FileReader&) = delete;
        FileReader& operator = (FileReader&& other);

        void read(void* buffer, std::size_t numBytes);

        void seek(std::size_t position);
        void seekMod(int32_t amount);

        std::size_t tell() const {
            return mPosition;
        }

        template<typename T>
        T read() {
            T ret;
            read(&ret, sizeof(T));
            return ret;
        }

        template<typename T>
        void read(T& value) {
            read(&value, sizeof(T));
        }

        template<typename T>
        void read(std::vector<T>& data) {
            read(data.data(), data.size() * sizeof(T));
        }

        static std::string readToString(const std::string& fileName);
    };
}

#endif //WILDSTAR_STUDIO_FILEREADER_H
