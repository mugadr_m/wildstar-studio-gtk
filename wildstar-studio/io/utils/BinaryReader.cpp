#include "BinaryReader.h"
#include <stdexcept>
#include <cstring>
#include <codecvt>
#include <utils/RuntimeError.h>
#include "log.h"

namespace io::utils {
    BinaryReader::BinaryReader(std::vector<uint8_t> data) : mData(std::move(data)) {

    }

    void BinaryReader::read(void *data, std::size_t numBytes) {
        if (mPosition + numBytes > mData.size()) {
            log_console->error("Attempted to read past the end of memory stream: {} + {} (={}) > {}", mPosition, numBytes, mPosition + numBytes, mData.size());
            throw ::utils::RuntimeError("Attempted to read past the end of the file");
        }

        memcpy(data, mData.data() + mPosition, numBytes);
        mPosition += numBytes;
    }

    BinaryReader::BinaryReader(const BinaryReader &other) : mData(other.mData), mPosition(other.mPosition) {

    }

    BinaryReader::BinaryReader(BinaryReader &&other) noexcept : mData(std::move(other.mData)), mPosition(other.mPosition) {

    }

    BinaryReader &BinaryReader::operator=(const BinaryReader &other) {
        mData = other.mData;
        mPosition = other.mPosition;
        return *this;
    }

    BinaryReader &BinaryReader::operator=(BinaryReader &&other) {
        mData = std::move(other.mData);
        mPosition = other.mPosition;
        return *this;
    }

    std::string BinaryReader::readString() {
        std::vector<char> chars;
        while (available() > 0) {
            const char cur = mData[mPosition++];
            if (cur == 0) {
                break;
            }

            chars.push_back(cur);
        }

        return std::string(chars.begin(), chars.end());
    }

    std::string BinaryReader::readStringUtf16() {
        static std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> converter;

        std::vector<char16_t> chars;
        while (available() > 0) {
            const auto cur = read < char16_t > ();
            if (cur == 0) {
                break;
            }

            chars.push_back(cur);
        }

        std::u16string utf16String(chars.begin(), chars.end());
        return converter.to_bytes(utf16String);
    }
}
