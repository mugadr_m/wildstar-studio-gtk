
#include "FileReader.h"
#include "log.h"
#include <stdexcept>
#include <utils/RuntimeError.h>

namespace io::utils {
    FileReader::FileReader(FILE *file) : mFile(file), mPosition(0) {
#ifdef _WIN32
        _fseeki64(mFile, 0, SEEK_END);
        mSize = static_cast<size_t>(_ftelli64(mFile));
        _fseeki64(mFile, 0, SEEK_SET);
#else
        fseek(mFile, 0, SEEK_END);
        mSize = static_cast<size_t>(ftell(mFile));
        fseek(mFile, 0, SEEK_SET);
#endif
    }

    FileReader::FileReader(FileReader &&other) : mFile(other.mFile), mPosition(other.mPosition),
                                                         mSize(other.mSize) {
        other.mFile = nullptr;
    }

    FileReader::~FileReader() {
        if (mFile != nullptr) {
            fclose(mFile);
        }

        mFile = nullptr;
    }

    FileReader &FileReader::operator=(FileReader &&other) {
        mFile = other.mFile;
        mPosition = other.mPosition;
        mSize = other.mSize;

        other.mFile = nullptr;
        return *this;
    }

    void FileReader::read(void *buffer, std::size_t numBytes) {
        if (mPosition + numBytes > mSize) {
            log_console->warn("Attempted to read past the end of the stream: {} + {} (= {}) > {}", mPosition, numBytes,
                              mPosition + numBytes, mSize);
            throw ::utils::RuntimeError("Attempted to read past the end of the file");
        }

        const auto numRead = fread(buffer, 1, numBytes, mFile);
        if (numRead != numBytes) {
            log_console->warn("Unable to read enough bytes from file");
            throw ::utils::RuntimeError("Error reading from file");
        }
        mPosition += numBytes;
    }

    void FileReader::seek(std::size_t position) {
        mPosition = position;
#ifdef _WIN32
        _fseeki64(mFile, position, SEEK_SET);
#else
        fseek(mFile, position, SEEK_SET);
#endif
    }

    void FileReader::seekMod(int32_t amount) {
        if (amount < 0 && std::abs(amount) > mPosition) {
            log_console->warn("Attempted to seek behind the stream: {} - {} < 0", mPosition, std::abs(amount));
            throw ::utils::RuntimeError("Attempted to seek behind the beginning of the file");
        }

        mPosition += amount;
#ifdef _WIN32
        _fseeki64(mFile, mPosition, SEEK_SET);
#else
        fseek(mFile, mPosition, SEEK_SET);
#endif
    }

    std::string FileReader::readToString(const std::string &fileName) {
        FILE *file = fopen(fileName.c_str(), "rb");
        if (file == nullptr) {
            log_console->warn("Unable to open file {}", fileName);
            throw ::utils::RuntimeError("Unable to open file");
        }

        fseek(file, 0, SEEK_END);
        const auto size = static_cast<const size_t>(ftell(file));
        fseek(file, 0, SEEK_SET);
        std::vector<char> content(size);
        if (fread(content.data(), 1, size, file) != size) {
            log_console->warn("Unable to read all bytes from file {}", fileName);
            throw ::utils::RuntimeError("Unable to read file");
        }

        fclose(file);
        content.push_back('\0');
        return content.data();
    }
}
