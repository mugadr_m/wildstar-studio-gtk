
#include <io/files/TexReader.h>
#include "TextureManager.h"
#include "utils/Utilities.h"
#include "modules/libtex.h"
#include "log.h"

namespace io::textures {

    TextureManager::TextureManager(io::archive::FileManagerPtr fileManager, const std::function<void(gl::Texture *, const std::string&)> &deleter) :
            mTextureDeleter(deleter),
            mFileManager(std::move(fileManager)) {
        mIsStopped.store(false);
        const auto numThreads = std::max(2u, std::thread::hardware_concurrency());
        for (auto i = 0u; i < numThreads; ++i) {
            mLoadThreads.emplace_back(std::bind(&TextureManager::loaderThread, this));
        }
    }

    TextureManager::TextureManager(io::archive::FileManagerPtr fileManager) :
            TextureManager(fileManager, std::bind(&TextureManager::deleteTexture, this, std::placeholders::_1, std::placeholders::_2)) {

    }

    TextureManager::~TextureManager() {
        mIsStopped.store(true);
        mLoadEvent.notify_all();

        for (auto &thread : mLoadThreads) {
            if (thread.joinable()) {
                thread.join();
            }
        }
    }

    void TextureManager::onFrame() {
        if(mSyncWorkItems.empty()) {
            return;
        }

        if(!mSyncWorkLock.try_lock()) {
            return;
        }

        std::lock_guard<std::mutex> l(mSyncWorkLock, std::adopt_lock);
        for(auto i = 0u; i < 5 && !mSyncWorkItems.empty(); ++i) {
            mSyncWorkItems.front()();
            mSyncWorkItems.pop_front();
        }
    }

    void TextureManager::deleteTexture(gl::Texture *texture, const std::string&) {
        delete texture;
    }

    gl::TexturePtr TextureManager::getTexture(const std::string &textureName, bool isUpperCase, const gl::AddressingMode& addressingMode) {
        auto texName = isUpperCase ? textureName : ::utils::toUpper(textureName);
        gl::TexturePtr ret;
        {
            std::lock_guard<std::mutex> l(mCacheLock);
            const auto itr = mCache.find(texName);
            if (itr != mCache.end()) {
                const auto retTexture = itr->second.lock();
                if (retTexture != nullptr) {
                    return retTexture;
                }
            }

            ret = createNewManagedTexture(textureName);
            if(itr != mCache.end()) {
                mCache[texName] = ret;
            } else {
                mCache.insert(std::make_pair(texName, ret));
            }
        }

        {
            std::lock_guard<std::mutex> l(mLoadLock);
            mWorkItems.emplace_back(textureName, ret, addressingMode);
            mLoadEvent.notify_one();
        }

        return ret;
    }

    gl::TexturePtr TextureManager::createNewManagedTexture(const std::string& tag) const {
        struct Deleter {
            std::function<void(gl::Texture *, const std::string&)> deleter;
            std::string tag;

            void operator()(gl::Texture *tex) {
                deleter(tex, this->tag);
            }
        };

        Deleter d{mTextureDeleter, tag};

        return std::shared_ptr<gl::Texture>(new gl::Texture(), d);
    }

    void TextureManager::loaderThread() {
        while (!mIsStopped) {
            WorkItem workItem;
            {
                std::unique_lock<std::mutex> l(mLoadLock);
                if (mWorkItems.empty()) {
                    mLoadEvent.wait(l);
                    continue;
                }

                workItem = mWorkItems.front();
                mWorkItems.pop_front();
            }

            if (workItem.getFileName().empty() || workItem.getTexture() == nullptr) {
                continue;
            }

            log_console->info("Loading texture {}", workItem.getFileName());

            try {
                textureLoadImpl(workItem.getFileName(), workItem.getTexture(), workItem.getAddressingMode());
            } catch (::utils::RuntimeError &e) {
                log_console->warn("Error loading texture {}: {}", workItem.getFileName(), std::string(e.what()));
            }
        }
    }

    void TextureManager::loadTextureArgb(gl::TexturePtr texture, const io::files::TexReader &reader, const gl::AddressingMode& addressingMode) {
        const auto &header = reader.getTexHeader();
        if (header.isCompressed != 0) {
            std::vector<std::vector<uint8_t>> layers;
            auto width = header.width;
            auto height = header.height;
            for (const auto &layer : reader.getLayerData()) {
                std::vector<uint8_t> curLayer(width * height * 4);
                libtex::decompressImageLayer(header.textureType, layer.data(), static_cast<uint32_t>(layer.size()),
                                             width, height,
                                             header.layerInfo[0].qualityCode, header.layerInfo[0].hasDefaultValue, header.layerInfo[0].defaultValue,
                                             header.layerInfo[1].qualityCode, header.layerInfo[1].hasDefaultValue, header.layerInfo[1].defaultValue,
                                             header.layerInfo[2].qualityCode, header.layerInfo[2].hasDefaultValue, header.layerInfo[2].defaultValue,
                                             header.layerInfo[3].qualityCode, header.layerInfo[3].hasDefaultValue, header.layerInfo[3].defaultValue,
                                             curLayer.data());
                layers.emplace_back(curLayer);
                width = std::max(width >> 1, 1u);
                height = std::max(height >> 1, 1u);
            }

            std::lock_guard<std::mutex> l(mSyncWorkLock);
            mSyncWorkItems.emplace_back([texture, header, layers, addressingMode]() {
                texture->loadFromMemoryARGBWithLayers(header.width, header.height, layers);
                texture->setAddressingMode(addressingMode);
            });
        } else {
            std::lock_guard<std::mutex> l(mSyncWorkLock);
            mSyncWorkItems.emplace_back([texture, header, reader, addressingMode]() {
                texture->loadFromMemoryARGBWithLayers(header.width, header.height, reader.getLayerData());
                texture->setAddressingMode(addressingMode);
            });
        }
    }

    void TextureManager::textureLoadImpl(const std::string &textureName, gl::TexturePtr texture, const gl::AddressingMode& addressingMode) {
        auto reader = mFileManager->openFile(textureName);
        io::files::TexReader texReader(reader);
        const auto &header = texReader.getTexHeader();

        if (header.formatIndex == 0 || header.formatIndex == 1) {
            loadTextureArgb(texture, texReader, addressingMode);
            return;
        }

        std::lock_guard<std::mutex> l(mSyncWorkLock);
        mSyncWorkItems.emplace_back([texture, header, texReader, addressingMode]() {
            switch (header.formatIndex) {
                case 6: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R8, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 7: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R16, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 8: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R16G16, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 9: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R16G16B16A16, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 10: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R8G8_SNORM, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 11: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::RGBA8_SNORM, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 12: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R16G16_SNORM, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 16: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R16_FLOAT, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 17: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R16G16_FLOAT, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 18: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R16G16B16A16_FLOAT, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 19: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R32_FLOAT, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 20: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R32G32B32A32_FLOAT, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 21: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::D16, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 23: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::D24_S8, header.width, header.height, texReader.getLayerData());
                    break;
                }

                case 26: {
                    texture->loadFromMemoryWithLayers(gl::MemoryFormat::R10G10B10A2, header.width, header.height, texReader.getLayerData());
                    break;
                }

                default: {
                    texture->loadFromMemoryARGB(2, 2, {
                            0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0x00, 0xFF,
                            0xFF, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF
                    });
                    break;
                }
            }

            texture->setAddressingMode(addressingMode);
        });
    }
}