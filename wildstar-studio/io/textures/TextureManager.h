#include <utility>

#include <utility>


#ifndef WILDSTAR_STUDIO_TEXTUREMANAGER_H
#define WILDSTAR_STUDIO_TEXTUREMANAGER_H

#include <map>
#include <unordered_map>
#include <memory>
#include <functional>
#include <deque>
#include <atomic>

#ifdef USE_CROSS_COMPILE
#include "cross_compile/mingw.thread.h"
#include "cross_compile/mingw.mutex.h"
#include "cross_compile/mingw.condition_variable.h"
#else
#include <mutex>
#include <thread>
#include <condition_variable>
#endif

#include "gl/Texture.h"
#include <io/archive/FileManager.h>
#include <io/files/TexReader.h>

namespace io::textures {
    class TextureManager {
        class WorkItem {
            std::string mFileName;
            gl::TexturePtr mTexture;
            gl::AddressingMode mAddressingMode;

        public:
            WorkItem() = default;
            WorkItem(std::string fileName, gl::TexturePtr texture, gl::AddressingMode mode) : mFileName(std::move(fileName)), mTexture(std::move(texture)), mAddressingMode(mode) { }
            const std::string& getFileName() const { return mFileName; }
            const gl::TexturePtr& getTexture() const { return mTexture; }
            const gl::AddressingMode& getAddressingMode() const { return mAddressingMode; }
        };

        std::function<void(gl::Texture*, const std::string&)> mTextureDeleter;
        std::unordered_map<std::string, std::weak_ptr<gl::Texture>> mCache;
        std::mutex mCacheLock;

        std::mutex mLoadLock;
        std::condition_variable mLoadEvent;
        std::deque<WorkItem> mWorkItems;

        std::deque<std::function<void()>> mSyncWorkItems;
        std::mutex mSyncWorkLock;

        std::vector<std::thread> mLoadThreads;
        std::atomic_bool mIsStopped;

        io::archive::FileManagerPtr mFileManager;

        void deleteTexture(gl::Texture* texture, const std::string&);

        gl::TexturePtr createNewManagedTexture(const std::string& tag) const;

        void loaderThread();

        void loadTextureArgb(gl::TexturePtr texture, const io::files::TexReader& reader, const gl::AddressingMode& addressingMode);

        void textureLoadImpl(const std::string& textureName, gl::TexturePtr texture, const gl::AddressingMode& addressingMode);

    public:
        explicit TextureManager(io::archive::FileManagerPtr);
        explicit TextureManager(io::archive::FileManagerPtr, const std::function<void(gl::Texture*, const std::string&)>& deleter);
        ~TextureManager();

        gl::TexturePtr getTexture(const std::string& textureName, bool isUpperCase = false, const gl::AddressingMode& addressingMode = gl::AddressingMode::REPEAT);

        void onFrame();
    };

    typedef std::shared_ptr<TextureManager> TextureManagerPtr;
}


#endif //WILDSTAR_STUDIO_TEXTUREMANAGER_H
