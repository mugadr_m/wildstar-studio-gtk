
#include <utils/RuntimeError.h>
#include "LzmaStream.h"
#include "lzma/LzmaLib.h"
#include "log.h"

namespace io::compression {

    utils::BinaryReader LzmaStream::decompress(const std::vector<uint8_t> &compressedData, uint64_t uncompressedSize) {
        std::vector<uint8_t> uncompressedData(uncompressedSize);

        uint64_t compressedSize = compressedData.size() - 5;

        const auto res = LzmaUncompress(
                uncompressedData.data(),
                &uncompressedSize,
                compressedData.data() + 5,
                &compressedSize,
                compressedData.data(),
                5
        );

        if (res != SZ_OK) {
            log_console->warn("LZMA decompression failed: {}", res);
            throw ::utils::RuntimeError("LZMA decompression failed");
        }

        return utils::BinaryReader(uncompressedData);
    }
}