
#ifndef WILDSTAR_STUDIO_LZMASTREAM_H
#define WILDSTAR_STUDIO_LZMASTREAM_H


#include "io/utils/BinaryReader.h"

namespace io::compression {
    class LzmaStream {
    public:
        static utils::BinaryReader decompress(const std::vector<uint8_t>& compressedData, uint64_t uncompressedSize);
    };
}

#endif //WILDSTAR_STUDIO_LZMASTREAM_H
