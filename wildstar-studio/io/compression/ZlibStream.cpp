
#include "ZlibStream.h"
#include "zlib/zlib.h"
#include "log.h"
#include <cstring>
#include <utils/RuntimeError.h>

namespace io::compression {
    utils::BinaryReader ZlibStream::decompress(const std::vector<uint8_t> &compressedData, uint64_t uncompressedSize) {
        std::vector<uint8_t> uncompressedData(uncompressedSize);
        const auto compressedSize = compressedData.size();

        z_stream stream;
        memset(&stream, 0, sizeof(z_stream));
        inflateInit(&stream);

        stream.avail_in = static_cast<uInt>(compressedSize);
        stream.next_in = (Bytef*) (compressedData.data());
        stream.avail_out = static_cast<uInt>(uncompressedSize);
        stream.next_out = uncompressedData.data();

        while(stream.avail_in > 0 && stream.avail_out > 0) {
            const auto res = inflate(&stream, Z_FINISH);
            if(res == Z_STREAM_END) {
                break;
            }

            if(res != Z_OK) {
                log_console->warn("Error decompressing ZLib stream: {}", res);
                throw ::utils::RuntimeError("Invalid ZLib decompression");
            }
        }

        inflateEnd(&stream);
        if(stream.avail_in > 0 || stream.avail_out > 0) {
            log_console->warn("Could not decompress enough data from zlib stream");
            throw ::utils::RuntimeError("Invalid ZLib decompression");
        }

        return utils::BinaryReader(uncompressedData);
    }
}