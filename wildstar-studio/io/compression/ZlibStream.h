
#ifndef WILDSTAR_STUDIO_ZLIBSTREAM_H
#define WILDSTAR_STUDIO_ZLIBSTREAM_H

#include <vector>
#include <cstdint>
#include "io/utils/BinaryReader.h"

namespace io::compression {
    class ZlibStream {
    public:
        static utils::BinaryReader decompress(const std::vector<uint8_t>& compressedData, uint64_t uncompressedSize);
    };
}


#endif //WILDSTAR_STUDIO_ZLIBSTREAM_H
