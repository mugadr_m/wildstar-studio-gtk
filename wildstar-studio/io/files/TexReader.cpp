
#include <utils/RuntimeError.h>
#include "TexReader.h"
#include "log.h"

namespace io::files {
    TexReader::TexReader(utils::BinaryReader &reader) {
        mHeader = reader.read<TexHeader>();
        loadLayers(reader);
    }

    void TexReader::loadLayers(utils::BinaryReader &reader) {
        reader.seek(112);
        for (auto i = 0u; i < mHeader.mipLevels; ++i) {
            const auto size = getLayerSize(i);
            mLayerData.push_back(reader.readBytes(size));
            mLayerSizes.push_back(size);
        }

        std::reverse(mLayerData.begin(), mLayerData.end());
        std::reverse(mLayerSizes.begin(), mLayerSizes.end());
    }

    uint32_t TexReader::getLayerSize(uint32_t level) {
        const auto w = std::max(1u, mHeader.width >> (mHeader.mipLevels - 1 - level));
        const auto h = std::max(1u, mHeader.height >> (mHeader.mipLevels - 1 - level));

        if (mHeader.isCompressed != 0) {
            return mHeader.sizes[level];
        } else {
            switch (mHeader.formatIndex) {
                case 0: // RGBA8
                case 1: // RGBX8
                    return w * h * 4;
                case 6: // R8
                    return w * h;
                case 7: // R16
                    return w * h * 2;
                case 8: // R16G16
                    return w * h * 4;
                case 9: // R16G16B16A16
                    return 2 * h * 8;
                case 10: // R8G8
                    return w * h * 2;
                case 11: // R8G8B8A8_SNORM
                    return w * h * 4;
                case 12: // R16G16_SNORM
                    return w * h * 4;
                case 13: // BC1
                    return ((w + 3) / 4) * ((h + 3) / 4) * 8;
                case 14: // BC2
                case 15: // BC3
                    return ((w + 3) / 4) * ((h + 3) / 4) * 16;
                case 16:
                    return w * h * 2;
                case 17:
                    return w * h * 4;
                case 18:
                    return w * h * 8;
                case 19:
                    return w * h * 4;
                case 20:
                    return w * h * 16;
                case 23:
                    return w * h * 3;
                case 26:
                    return w * h * 4;
                default: {
                    log_console->error("Cannot calculated size for format index {}", mHeader.formatIndex);
                    throw ::utils::RuntimeError("Unknown format index in texture");
                }
            }
        }
    }
}