
#ifndef WILDSTAR_STUDIO_TBLFILE_H
#define WILDSTAR_STUDIO_TBLFILE_H

#include <cstdint>
#include "io/utils/BinaryReader.h"
#include <map>
#include <utils/RuntimeError.h>
#include "log.h"
#include "TblStructures.h"

namespace io::files {


    template<typename T>
    class TblFile {
        TBLHeader mHeader;

        std::map<uint32_t, T *> mEntryMap;
        std::vector<T> mEntries;

        void load(utils::BinaryReader &reader) {
            mHeader = reader.read<TBLHeader>();
            reader.seek(0x60 + mHeader.ofsFieldDesc);

            std::vector<FieldDescription> fields(mHeader.numFields);
            reader.read(fields);

            /*auto strOffset = 0x60 + mHeader.ofsFieldDesc + mHeader.numFields * sizeof(FieldDescription);
            if(strOffset % 16) {
                strOffset += 16 - (strOffset % 16);
            }
            for (const auto &field : fields) {
                reader.seek(strOffset + field.ofsFieldTitle);
                log_console->info("Field: {}, type: {}", reader.readStringUtf16(), (uint32_t) field.fieldType);
            }*/

            mEntries.resize(mHeader.numRows);

            for (auto i = 0u; i < mHeader.numRows; ++i) {
                reader.seek(0x60 + mHeader.ofsEntries + i * mHeader.recordSize);
                T val{};
                auto *ptr = (uint8_t *) &val;
                auto skip = false;

                for (auto j = 0u; j < fields.size(); ++j) {
                    if (skip && (j > 0 && fields[j - 1].fieldType == FieldType::STRING_OFFSET && fields[j].fieldType != FieldType::STRING_OFFSET)) {
                        reader.seekMod(4);
                    }

                    switch (fields[j].fieldType) {
                        case FieldType::UINT32: {
                            *(uint32_t *) ptr = reader.read<uint32_t>();
                            ptr += sizeof(uint32_t);
                            break;
                        }

                        case FieldType::UINT64: {
                            *(uint64_t *) ptr = reader.read<uint64_t>();
                            ptr += sizeof(uint64_t);
                            break;
                        }

                        case FieldType::FLOAT: {
                            *(float *) ptr = reader.read<float>();
                            ptr += sizeof(float);
                            break;
                        }

                        case FieldType::BOOL: {
                            *(bool *) ptr = reader.read<uint32_t>() != 0;
                            ptr += sizeof(bool);
                            break;
                        }

                        case FieldType::STRING_OFFSET: {
                            const auto ofsLower = reader.read<uint32_t>();
                            uint64_t offset = reader.read<uint32_t>();
                            skip = ofsLower == 0;

                            if (ofsLower > 0) {
                                offset = ofsLower;
                            }

                            offset += mHeader.ofsEntries + 0x60;
                            const auto oldPos = reader.tell();
                            if (offset < reader.getSize()) {
                                reader.seek(offset);
                                *(std::string *) ptr = reader.readStringUtf16();
                            } else {
                                *(std::string *) ptr = "";
                            }
                            reader.seek(oldPos);

                            ptr += sizeof(std::string);
                            break;
                        }

                        default: {
                            throw ::utils::RuntimeError("Unknown field type");
                        }
                    }
                }

                mEntries[i] = val;
                mEntryMap.emplace(*(uint32_t *) &val, &mEntries[i]);
            }
        }

    public:
        explicit TblFile(utils::BinaryReader &reader) {
            load(reader);
        }

        const T &getByEntry(uint32_t entry) const {
            return *mEntryMap.at(entry);
        }

        const T &getByIndex(uint32_t index) const {
            return mEntries.at(index);
        }

        std::size_t getNumEntries() const {
            return mEntries.size();
        }
    };

    template<typename T>
    using TblFilePtr = std::shared_ptr<TblFile<T>>;
}

#endif //WILDSTAR_STUDIO_TBLFILE_H
