#include <glm/geometric.hpp>
#include "AreaReader.h"
#include "glm/vec3.hpp"
#include "log.h"

namespace io::files {
    const uint32_t SIG_PROp = 0x50524F70;
    const uint32_t SIG_CHNK = 0x43484E4B;

    AreaReader::AreaReader(uint32_t indexX, uint32_t indexY, utils::BinaryReader &fileReader, TblFilePtr<WorldLayer> worldLayerDb) : mIndexX(indexX), mIndexY(indexY) {
        fileReader.seek(8);
        readAllChunks(fileReader, mDataChunks);
        loadProps();
        loadChunks(std::move(worldLayerDb));
    }

    void AreaReader::readAllChunks(utils::BinaryReader &reader, std::map<uint32_t, utils::BinaryReader> &outMap) {
        while (reader.available() > 8) {
            const auto magic = reader.read<uint32_t>();
            const auto size = reader.read<uint32_t>();
            if (size > 0) {
                outMap[magic] = utils::BinaryReader(reader.readBytes(size));
            } else {
                outMap[magic] = utils::BinaryReader();
            }
        }
    }

    void AreaReader::loadProps() {
        const auto cprop = mDataChunks.find(SIG_PROp);
        if (cprop == mDataChunks.end() || !cprop->second.available()) {
            return;
        }

        utils::BinaryReader reader = cprop->second;
        const auto numProps = reader.read<uint32_t>();
        std::vector<PropElement> props(numProps);
        reader.read(props);
        for (const auto &prop : props) {
            const auto parentItr = mProperties.find(prop.parentId);
            PropEntryPtr parent = parentItr == mProperties.end() ? nullptr : parentItr->second;
            mProperties.insert(std::make_pair(prop.uniqueId, std::make_shared<PropEntry>(prop, reader, parent)));
        }
    }

    void AreaReader::loadChunks(TblFilePtr<WorldLayer> worldLayerDb) {
        const auto cchunk = mDataChunks.find(SIG_CHNK);
        if (cchunk == mDataChunks.end() || !cchunk->second.available()) {
            return;
        }

        auto lastIndex = 0;
        auto &reader = cchunk->second;
        while (reader.available() >= 8) {
            const auto val = reader.read<uint32_t>();
            auto index = (val >> 24) & 0xFF;
            index += lastIndex;
            lastIndex = index + 1;

            const auto size = val & 0x00FFFFFF;
            if (index >= 256 || reader.available() < size) {
                break;
            }

            if (size < 4) {
                continue;
            }

            const auto cx = (index % 16) * 32.0f;
            const auto cy = (index / 16) * 32.0f; // NOLINT(bugprone-integer-division)

            utils::BinaryReader chunkStream(reader.readBytes(size));
            const auto chunk = std::make_shared<AreaChunk>(cx, cy, mProperties, chunkStream, worldLayerDb);
            if(!chunk->hasHeightMap()) {
                continue;
            }

            mChunkList.emplace_back(chunk);
            mChunkMap.insert(std::make_pair(index, chunk));
        }

        const auto maxPos = std::numeric_limits<float>::max();
        const auto minPos = -maxPos;
        mMinPos = glm::vec3(maxPos, maxPos, maxPos);
        mMaxPos = glm::vec3(minPos, minPos, minPos);

        mVertices.reserve(16 * 16 * mChunkList.size());
        for(const auto& chunk : mChunkList) {
            chunk->setVertexOffset(static_cast<uint32_t>(mVertices.size()));
            const auto& verts = chunk->getVertices();
            mVertices.insert(mVertices.end(), verts.begin(), verts.end());

            mMinPos = glm::min(mMinPos, chunk->getMinPos());
            mMaxPos = glm::max(mMaxPos, chunk->getMaxPos());
        }
        mVertices.shrink_to_fit();
    }

    PropEntry::PropEntry(const PropElement &element, utils::BinaryReader &chunkReader, PropEntryPtr parent) : mProp(element), mParent(std::move(parent)) {
        if (element.nameOffset > 0 && element.nameOffset <= chunkReader.getSize()) {
            chunkReader.seek(element.nameOffset);
            mName = chunkReader.readStringUtf16();
        }
    }

    AreaChunk::AreaChunk(float x, float y, std::map<uint32_t, PropEntryPtr> &propMap, utils::BinaryReader &chunkStream, TblFilePtr<WorldLayer> worldLayerDb) :
            mBaseX(x), mBaseY(y) {
        mFlags = chunkStream.read<uint32_t>();
        if (!hasHeightMap()) {
            return;
        }

        loadHeightMap(chunkStream);
        loadTextures(chunkStream, std::move(worldLayerDb));

        loadBlendOld(chunkStream);
        loadColorsOld(chunkStream);
        if((mFlags & 0x20) != 0) {
            chunkStream.seekMod(4); // areaId
        }

        if((mFlags & 0x40) != 0 || (mFlags & 0x80) != 0) {
            chunkStream.seekMod(80);
        }

        loadColorsNew(chunkStream);

        if((mFlags & 0x10000) != 0) {
            chunkStream.seekMod(64 * 64);
        }

        loadBlendNew(chunkStream);
    }

    void AreaChunk::loadHeightMap(utils::BinaryReader &reader) {
        std::vector<uint16_t> heights(19 * 19);
        reader.read(heights);

        mFullVertexData.reserve(19 * 19);
        for (auto y = -1; y < 18; ++y) {
            for (auto x = -1; x < 18; ++x) {
                const auto h = ((heights[(y + 1) * 19 + x + 1] & 0x7FFF) / 8.0f) - 2048.0f;
                mFullVertexData.emplace_back(AreaChunkVertex{
                        mBaseX + x * 2.0f,
                        mBaseY + y * 2.0f,
                        h,
                        0.0f,
                        0.0f,
                        0.0f,
                        x / 16.0f,
                        y / 16.0f
                });
            }
        }

        extendBuffer();
        calculateNormals();

        const auto maxPos = std::numeric_limits<float>::max();
        const auto minPos = -maxPos;
        mMinPos = glm::vec3(maxPos, maxPos, maxPos);
        mMaxPos = glm::vec3(minPos, minPos, minPos);

        for(const auto& v : mVertexData) {
            mMinPos = glm::min(mMinPos, glm::vec3(v.x, v.y, v.z));
            mMaxPos = glm::max(mMaxPos, glm::vec3(v.x, v.y, v.z));
        }
    }

    void AreaChunk::extendBuffer() {
        mVertexData.resize(17 * 17 + 16 * 16);
        for (auto i = 0u; i < 17; ++i) {
            for (auto j = 0u; j < 17; ++j) {
                const auto idx = i * 33 + j;
                const auto vidx = ((i + 1) * 19 + j + 1);
                mVertexData[idx] = mFullVertexData[vidx];
            }
        }

        for (auto i = 0u; i < 16; ++i) {
            for (auto j = 0u; j < 16; ++j) {
                const auto idx = (17 + i * 33 + j);
                const auto i0 = ((i + 1) * 19 + j + 1);
                const auto i1 = ((i + 1) * 19 + j + 2);
                const auto i2 = ((i + 2) * 19 + j + 1);
                const auto i3 = ((i + 2) * 19 + j + 2);

                mVertexData[idx].x = (mFullVertexData[i0].x + mFullVertexData[i1].x + mFullVertexData[i2].x + mFullVertexData[i3].x) / 4.0f;
                mVertexData[idx].y = (mFullVertexData[i0].y + mFullVertexData[i1].y + mFullVertexData[i2].y + mFullVertexData[i3].y) / 4.0f;
                mVertexData[idx].z = (mFullVertexData[i0].z + mFullVertexData[i1].z + mFullVertexData[i2].z + mFullVertexData[i3].z) / 4.0f;

                mVertexData[idx].tx = std::min(std::max(0.0f, (mFullVertexData[i0].tx + mFullVertexData[i3].tx) / 2.0f), 1.0f);
                mVertexData[idx].ty = std::min(std::max(0.0f, (mFullVertexData[i0].ty + mFullVertexData[i3].ty) / 2.0f), 1.0f);
            }
        }
    }

    void AreaChunk::calculateNormals() {
        for (auto i = 1u; i < 18; ++i) {
            for (auto j = 1u; j < 18; ++j) {
                const auto i0 = ((i - 1) * 19 + j - 1);
                const auto i1 = ((i - 1) * 19 + j + 1);
                const auto i2 = ((i + 1) * 19 + j + 1);
                const auto i3 = ((i + 1) * 19 + j - 1);
                const auto v0 = (i * 19 + j);

                const auto p1 = glm::vec3(mFullVertexData[i0].x, mFullVertexData[i0].y, mFullVertexData[i0].z);
                const auto p2 = glm::vec3(mFullVertexData[i1].x, mFullVertexData[i1].y, mFullVertexData[i1].z);
                const auto p3 = glm::vec3(mFullVertexData[i2].x, mFullVertexData[i2].y, mFullVertexData[i2].z);
                const auto p4 = glm::vec3(mFullVertexData[i3].x, mFullVertexData[i3].y, mFullVertexData[i3].z);
                const auto vt = glm::vec3(mFullVertexData[v0].x, mFullVertexData[v0].y, mFullVertexData[v0].z);

                const auto n1 = glm::cross(p2 - vt, p1 - vt);
                const auto n2 = glm::cross(p3 - vt, p2 - vt);
                const auto n3 = glm::cross(p4 - vt, p3 - vt);
                const auto n4 = glm::cross(p1 - vt, p4 - vt);

                const auto norm = glm::normalize(n1 + n2 + n3 + n4) * -1.0f;
                const auto vindex = ((i - 1) * 33 + j - 1);
                mVertexData[vindex].nx = norm.x;
                mVertexData[vindex].ny = norm.y;
                mVertexData[vindex].nz = norm.z;
            }
        }

        for (auto i = 0u; i < 16; ++i) {
            for (auto j = 0u; j < 16; ++j) {
                const auto idx = (17 + i * 33 + j);
                const auto i0 = (i * 33 + j);
                const auto i1 = (i * 33 + j + 1);
                const auto i2 = ((i + 1) * 33 + j);
                const auto i3 = ((i + 1) * 33 + j + 1);

                mVertexData[idx].nx = (mVertexData[i0].nx + mVertexData[i1].nx + mVertexData[i2].nx + mVertexData[i3].nx) / 4.0f;
                mVertexData[idx].ny = (mVertexData[i0].ny + mVertexData[i1].ny + mVertexData[i2].ny + mVertexData[i3].ny) / 4.0f;
                mVertexData[idx].nz = (mVertexData[i0].nz + mVertexData[i1].nz + mVertexData[i2].nz + mVertexData[i3].nz) / 4.0f;
            }
        }
    }

    void AreaChunk::loadTextures(utils::BinaryReader &reader, TblFilePtr<WorldLayer> worldLayerDb) {
        if ((mFlags & 2) == 0) {
            return;
        }

        std::vector<int32_t> textureIds(4);
        reader.read(textureIds);
        for (const auto &texId : textureIds) {
            if (texId <= 0) {
                mTextureNames.emplace_back("");
                mTextureScales.emplace_back(1.0f);
                continue;
            }

            try {
                const auto layer = worldLayerDb->getByEntry((uint32_t) texId);
                mTextureNames.emplace_back(layer.colorMapPath);
                mTextureScales.emplace_back(32.0f / layer.metersPerTextureTile);
            } catch (std::out_of_range &) {
                log_console->warn("Texture layer not found: {}", texId);
                mTextureNames.emplace_back("");
                mTextureScales.emplace_back(1.0f);

            }
        }
    }

    void AreaChunk::loadBlendOld(utils::BinaryReader &reader) {
        if ((mFlags & 2) == 0 || (mFlags & 4) == 0) {
            return;
        }

        std::vector<uint16_t> rawData(65 * 65);
        reader.read(rawData);
        mBlendMap.resize(65 * 65 * 4);

        for (auto i = 0; i < 4; ++i) {
            if ((mFlags & 2) == 0 || mTextureNames[i].empty()) {
                continue;
            }

            for (auto j = 0; j < 65 * 65; ++j) {
                const auto blend = rawData[j];
                const auto value = ((blend & (0xF << (i * 4))) >> (i * 4));
                const auto blendValue = (uint8_t) ((value / 15.0f) * 255.0f);
                mBlendMap[j * 4 + i] = blendValue;
            }
        }
    }

    void AreaChunk::loadBlendNew(utils::BinaryReader &reader) {
        if ((mFlags & 2) == 0 || (mFlags & 0x20000) == 0) {
            return;
        }

        mBlendMap.resize(2312);
        reader.read(mBlendMap);
    }

    void AreaChunk::loadColorsOld(utils::BinaryReader &reader) {
        if((mFlags & 8) == 0) {
            return;
        }

        std::vector<uint16_t> rawData(65 * 65);
        reader.read(rawData);
        mColorMap.resize(65 * 65 * 4);

        for (auto i = 0; i < 65 * 65; ++i) {
            const auto value = rawData[i];
            auto r = (uint8_t)(value & 0x1F);
            auto g = (uint8_t)((value >> 5) & 0x3F);
            auto b = (uint8_t)((value >> 11) & 0x1F);
            const auto a = uint8_t(0xFF);
            r = (uint8_t)((r / 31.0f) * 255);
            g = (uint8_t)((g / 63.0f) * 255);
            b = (uint8_t)((b / 31.0f) * 255);
            mColorMap[i * 4] = b;
            mColorMap[i * 4 + 1] = g;
            mColorMap[i * 4 + 2] = r;
            mColorMap[i * 4 + 3] = a;
        }
    }

    void AreaChunk::loadColorsNew(utils::BinaryReader &reader) {
        if((mFlags & 0x2000) == 0) {
            return;
        }

        mColorMap.resize(4624);
        reader.read(mColorMap);
    }
}