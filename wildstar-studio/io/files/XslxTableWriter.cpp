
#include "XslxTableWriter.h"
#include <sstream>
#include <log.h>

namespace io::files {
    XlsxTableWriter::XlsxTableWriter(io::utils::BinaryWriter &writer, const DynamicTblFilePtr &table) : TableWriter(writer, table) {
        mWorkBook = std::make_shared<xlnt::workbook>();
        if (mWorkBook->sheet_count() == 0) {
            mWorkBook->create_sheet();
        }
        mWorkSheet = mWorkBook->sheet_by_index(0);

        auto index = 1u;
        for (const auto &field : table->getFields()) {
            mWorkSheet.cell(index++, 1).value(field.getFieldName());
        }
    }

    void XlsxTableWriter::convertRow(const DynamicTblFile::TVisibleRecord &record) {
        auto index = 1u;
        for (const auto &val : record) {
            const auto &field = mTable->getFields()[index - 1];
            auto cell = mWorkSheet.cell(index++, static_cast<xlnt::row_t>(mRowCount + 2));
            switch (field.getDescription().fieldType) {
                case FieldType::UINT32:
                case FieldType::UINT64: {
                    try {
                        cell.value(std::stoull(val));
                    } catch (std::exception& err) {
                        log_console->warn("Cannot convert value '{}' to an integer", val);
                        cell.value(val);
                    }
                    break;
                }

                case FieldType::FLOAT: {
                    try {
                        cell.value(std::stof(val));
                    } catch (std::exception& err) {
                        log_console->warn("Cannot convert value '{}' to a float", val);
                        cell.value(val);
                    }
                    break;
                }

                case FieldType::BOOL: {
                    cell.value(val == "1");
                    break;
                }

                case FieldType::STRING_OFFSET:
                default: {
                    cell.value(val);
                    break;
                }
            }
        }

        ++mRowCount;
    }

    void XlsxTableWriter::onClose() {
        std::stringstream stream;
        mWorkBook->save(stream);
        std::string data = stream.str();
        mWriter.write(data.c_str(), data.size());
    }
}
