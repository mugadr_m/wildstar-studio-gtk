
#ifndef WILDSTAR_STUDIO_TEXREADER_H
#define WILDSTAR_STUDIO_TEXREADER_H

#include "io/utils/BinaryReader.h"
#include <cstdint>

namespace io::files {
#pragma pack(push, 1)
    struct TextureLayerInfo {
        uint8_t qualityCode;
        uint8_t hasDefaultValue;
        uint8_t defaultValue;
    };

    struct TexHeader {
        uint32_t signature;
        uint32_t version;
        uint32_t width;
        uint32_t height;
        uint32_t depth;
        uint32_t sides;
        uint32_t mipLevels;
        uint32_t formatIndex;
        uint32_t isCompressed;
        uint32_t textureType;
        TextureLayerInfo layerInfo[4];
        uint32_t unk1;
        uint32_t sizes[12];
        uint32_t decompressedType;
        uint32_t unk2;
    };
#pragma pack(pop)

    class TexReader {
        TexHeader mHeader;
        std::vector<std::vector<uint8_t>> mLayerData;
        std::vector<uint32_t> mLayerSizes;

        void loadLayers(utils::BinaryReader &reader);

        uint32_t getLayerSize(uint32_t level);

    public:
        explicit TexReader(utils::BinaryReader &reader);

        const std::vector<uint32_t> &getLayerSizes() const {
            return mLayerSizes;
        }

        const std::vector<std::vector<uint8_t>> &getLayerData() const {
            return mLayerData;
        }

        const TexHeader &getTexHeader() const {
            return mHeader;
        }
    };
}


#endif //WILDSTAR_STUDIO_TEXREADER_H
