
#ifndef WILDSTAR_STUDIO_SQLTABLEWRITER_H
#define WILDSTAR_STUDIO_SQLTABLEWRITER_H

#include "TableWriter.h"

namespace io::files {
    class SqlTableWriter : public TableWriter {
        std::string escape(const std::string& value) const;

    public:
        explicit SqlTableWriter(io::utils::BinaryWriter &writer, const DynamicTblFilePtr &table);

        void convertRow(const DynamicTblFile::TVisibleRecord &record) override;

        void onClose() override { }
    };
}


#endif //WILDSTAR_STUDIO_SQLTABLEWRITER_H
