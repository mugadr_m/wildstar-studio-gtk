
#ifndef WILDSTAR_STUDIO_STORAGEDEFINITIONS_H
#define WILDSTAR_STUDIO_STORAGEDEFINITIONS_H

#include <cstdint>
#include <string>

namespace io::files {
#pragma pack(push, 1)
    struct WorldLayer {
        uint32_t id;
        std::string description;
        float heightScale;
        float heightOffset;
        float parallaxScale;
        float parallaxOffset;
        float metersPerTextureTile;
        std::string colorMapPath;
        std::string normalMapPath;
        uint32_t averageColor;
        uint32_t projection;
        uint32_t materialType;
        uint32_t worldClutterId00;
        uint32_t worldClutterId01;
        uint32_t worldClutterId02;
        uint32_t worldClutterId03;
        float specularPower;
        uint32_t emissiveGlow;
        float scrollSpeed00;
        float scrollSpeed01;
    };
#pragma pack(pop)
}


#endif //WILDSTAR_STUDIO_STORAGEDEFINITIONS_H
