
#ifndef WILDSTAR_STUDIO_DYNAMICTBLFILE_H
#define WILDSTAR_STUDIO_DYNAMICTBLFILE_H

#include <vector>
#include <io/utils/BinaryReader.h>
#include <map>
#include <memory>
#include "TblStructures.h"

namespace io::files {

    class DynamicTblFile {
    public:
        typedef std::vector<std::string> TVisibleRecord;

        class FieldEntry {
            std::string mFieldName;
            FieldDescription mDescription;

        public:
            FieldEntry(const std::string &fieldName, const FieldDescription &description);
            FieldEntry() = default;

            const std::string &getFieldName() const {
                return mFieldName;
            }

            const FieldDescription &getDescription() const {
                return mDescription;
            }
        };

    private:
        TBLHeader mHeader;
        std::vector<FieldEntry> mFields;
        utils::BinaryReader mReader;
        std::string mTableName;

        void initialLoad();

    public:
        explicit DynamicTblFile(utils::BinaryReader reader);

        std::vector<TVisibleRecord> loadRecordPage(std::size_t startIndex, std::size_t numEntries);

        std::size_t getNumRecords() const {
            return mHeader.numRows;
        }

        const std::vector<FieldEntry>& getFields() const {
            return mFields;
        }

        const std::string& getTableName() const {
            return mTableName;
        }

        const utils::BinaryReader& getReader() const {
            return mReader;
        }
    };

    typedef std::shared_ptr<DynamicTblFile> DynamicTblFilePtr;
}


#endif //WILDSTAR_STUDIO_DYNAMICTBLFILE_H
