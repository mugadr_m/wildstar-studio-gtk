
#ifndef WILDSTAR_STUDIO_TBLSTRUCTURES_H
#define WILDSTAR_STUDIO_TBLSTRUCTURES_H

#include <cstdint>

namespace io::files {
    enum class FieldType : uint32_t {
        UINT32 = 3,
        FLOAT = 4,
        BOOL = 11,
        UINT64 = 20,
        STRING_OFFSET = 0x82
    };

#pragma pack(push, 1)
    struct TBLHeader {
        uint32_t magic;
        uint32_t version;
        uint64_t lenTableName;
        uint64_t ofsTableName;
        uint64_t recordSize;
        uint64_t numFields;
        uint64_t ofsFieldDesc;
        uint64_t numRows;
        uint64_t sizeEntryBlock;
        uint64_t ofsEntries;
        uint64_t maxEntry;
        uint64_t ofsIDLookup;
        uint64_t unk3Zero;
    };

    struct FieldDescription {
        uint64_t unk1;
        uint64_t ofsFieldTitle;
        FieldType fieldType;
        uint32_t unk6;
    };
#pragma pack(pop)
}


#endif //WILDSTAR_STUDIO_TBLSTRUCTURES_H
