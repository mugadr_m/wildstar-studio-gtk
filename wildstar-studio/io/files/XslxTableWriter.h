
#ifndef WILDSTAR_STUDIO_XSLXTABLEWRITER_H
#define WILDSTAR_STUDIO_XSLXTABLEWRITER_H


#include "TableWriter.h"
#include <xlnt/xlnt.hpp>

namespace io::files {
    class XlsxTableWriter : public TableWriter {
        std::shared_ptr<xlnt::workbook> mWorkBook;
        xlnt::worksheet mWorkSheet;

        std::size_t mRowCount = 0;

    public:
        explicit XlsxTableWriter(io::utils::BinaryWriter &writer, const DynamicTblFilePtr &table);

        void convertRow(const DynamicTblFile::TVisibleRecord &record) override;

        void onClose() override;
    };
}


#endif //WILDSTAR_STUDIO_XSLXTABLEWRITER_H
