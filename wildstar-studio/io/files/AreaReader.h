
#ifndef WILDSTAR_STUDIO_AREAREADER_H
#define WILDSTAR_STUDIO_AREAREADER_H

#include <map>
#include <memory>
#include "glm/glm.hpp"
#include "io/utils/BinaryReader.h"
#include "io/files/StorageDefinitions.h"
#include "io/files/TblFile.h"

namespace io::files {
#pragma pack(push, 1)

    struct PropElement {
        uint32_t uniqueId;
        uint32_t parentId;
        uint32_t fill1[3];
        uint32_t nameOffset;
        uint32_t nameEnd;
        float scale;
        float rotationX, rotationY, rotationZ, rotationW;
        float x, y, z;
        uint32_t fill2[5];
        uint32_t color1;
        uint32_t color2;
        uint32_t fill3[2];
        uint32_t color3;
        uint32_t fill4[1];
    };

    struct AreaChunkVertex {
        float x, y, z;
        float nx, ny, nz;
        float tx, ty;
    };

#pragma pack(pop)

    class PropEntry;
    typedef std::shared_ptr<PropEntry> PropEntryPtr;

    class PropEntry {
        PropElement mProp;
        std::string mName;
        PropEntryPtr mParent;
    public:
        explicit PropEntry(const PropElement &element, utils::BinaryReader &chunkReader, PropEntryPtr parent);
    };

    class AreaChunk {
        float mBaseX, mBaseY;
        uint32_t mFlags;

        glm::vec3 mMinPos;
        glm::vec3 mMaxPos;

        std::vector<AreaChunkVertex> mFullVertexData;
        std::vector<AreaChunkVertex> mVertexData;

        std::vector<std::string> mTextureNames;
        std::vector<float> mTextureScales;
        std::vector<uint8_t> mBlendMap;
        std::vector<uint8_t> mColorMap;

        uint32_t mVertexOffset = 0;

        void loadHeightMap(utils::BinaryReader& reader);
        void extendBuffer();
        void calculateNormals();

        void loadTextures(utils::BinaryReader &reader, TblFilePtr<WorldLayer> worldLayerDb);

        void loadBlendOld(utils::BinaryReader& reader);
        void loadBlendNew(utils::BinaryReader& reader);
        void loadColorsOld(utils::BinaryReader& reader);
        void loadColorsNew(utils::BinaryReader& reader);

    public:
        AreaChunk(float x, float y, std::map<uint32_t, PropEntryPtr>& propMap, utils::BinaryReader& chunkStream, TblFilePtr<WorldLayer> worldLayerDb);

        bool hasHeightMap() const {
            return (mFlags & 1) != 0;
        }

        void setVertexOffset(uint32_t offset) {
            mVertexOffset = offset;
        }

        uint32_t getVertexOffset() const {
            return mVertexOffset;
        }

        const std::vector<AreaChunkVertex>& getVertices() const {
            return mVertexData;
        }

        const glm::vec3 &getMinPos() const {
            return mMinPos;
        }

        const glm::vec3 &getMaxPos() const {
            return mMaxPos;
        }

        bool hasOldColorMap() const {
            return (mFlags & 8) != 0;
        }

        bool hasNewColorMap() const {
            return (mFlags & 0x2000) != 0;
        }

        bool hasAnyColorMap() const {
            return hasOldColorMap() || hasNewColorMap();
        }

        bool hasOldBlendMap() const {
            return (mFlags & 4) != 0;
        }

        bool hasNewBlendMap() const {
            return (mFlags & 0x20000) != 0;
        }

        const std::vector<uint8_t>& getColorMap() const {
            return mColorMap;
        }

        const std::vector<uint8_t>& getBlendMap() const {
            return mBlendMap;
        }

        const std::vector<std::string> getTextureNames() const {
            return mTextureNames;
        }

        const std::vector<float> getTextureScales() const {
            return mTextureScales;
        }
    };

    typedef std::shared_ptr<AreaChunk> AreaChunkPtr;

    class AreaReader {
        uint32_t mIndexX;
        uint32_t mIndexY;

        glm::vec3 mMinPos;
        glm::vec3 mMaxPos;

        std::map<uint32_t, utils::BinaryReader> mDataChunks;
        std::map<uint32_t, PropEntryPtr> mProperties;

        std::vector<AreaChunkPtr> mChunkList;
        std::map<uint32_t, AreaChunkPtr> mChunkMap;

        std::vector<AreaChunkVertex> mVertices;

        void loadProps();

        void loadChunks(TblFilePtr<WorldLayer> worldLayerDb);

        void readAllChunks(utils::BinaryReader &reader, std::map<uint32_t, utils::BinaryReader> &outMap);

    public:
        explicit AreaReader(uint32_t indexX, uint32_t indexY, utils::BinaryReader &fileReader, TblFilePtr<WorldLayer> worldLayerDb);

        std::vector<AreaChunkVertex> getVertices() const {
            return mVertices;
        }

        const std::vector<AreaChunkPtr>& getChunks() const {
            return mChunkList;
        }

        const glm::vec3 &getMinPos() const {
            return mMinPos;
        }

        const glm::vec3 &getMaxPos() const {
            return mMaxPos;
        }
    };

    typedef std::shared_ptr<AreaReader> AreaReaderPtr;
}


#endif //WILDSTAR_STUDIO_AREAREADER_H
