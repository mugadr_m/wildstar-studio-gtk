
#ifndef WILDSTAR_STUDIO_TABLEWRITER_H
#define WILDSTAR_STUDIO_TABLEWRITER_H


#include <io/utils/BinaryWriter.h>
#include "DynamicTblFile.h"

namespace io::files {
    class TableWriter {
    protected:
        io::utils::BinaryWriter& mWriter;
        DynamicTblFilePtr mTable;

    public:
        explicit TableWriter(io::utils::BinaryWriter& writer, const DynamicTblFilePtr& table);
        virtual ~TableWriter() = default;

        virtual void convertRow(const DynamicTblFile::TVisibleRecord& record) = 0;
        virtual void onClose() = 0;
    };
}


#endif //WILDSTAR_STUDIO_TABLEWRITER_H
