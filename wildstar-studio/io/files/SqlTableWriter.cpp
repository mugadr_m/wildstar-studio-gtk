
#include "SqlTableWriter.h"
#include <sstream>
#include <log.h>
#include <utils/RuntimeError.h>

namespace io::files {

    SqlTableWriter::SqlTableWriter(io::utils::BinaryWriter &writer, const DynamicTblFilePtr &table) : TableWriter(writer, table) {
        std::stringstream createTableBuilder;
        createTableBuilder << "CREATE TABLE IF NOT EXISTS " << table->getTableName() << " (" << std::endl;
        auto isFirst = true;
        for(const auto& field : table->getFields()) {
            if(!isFirst) {
                createTableBuilder << "," << std::endl;
            }

            isFirst = false;

            createTableBuilder << "\t" << field.getFieldName() << " ";
            switch(field.getDescription().fieldType) {
                case FieldType::UINT64: {
                    createTableBuilder << "BIGINT";
                    break;
                }

                case FieldType::UINT32: {
                    createTableBuilder << "INT";
                    break;
                }

                case FieldType::FLOAT: {
                    createTableBuilder << "FLOAT";
                    break;
                }

                case FieldType::BOOL: {
                    createTableBuilder << "BIT";
                    break;
                }

                case FieldType::STRING_OFFSET: {
                    createTableBuilder << "TEXT CHARACTER SET utf8";
                    break;
                }

                default: {
                    log_console->warn("Unrecognized field type: {}", (uint32_t) field.getDescription().fieldType);
                    throw ::utils::RuntimeError("Unrecognized field type");
                }
            }
        }
        createTableBuilder << "," << std::endl;
        createTableBuilder << "\tPRIMARY KEY (" << table->getFields()[0].getFieldName() << ")" << std::endl;
        createTableBuilder << ");" << std::endl << std::endl;

        writer.writeText(createTableBuilder.str());
    }

    void SqlTableWriter::convertRow(const DynamicTblFile::TVisibleRecord &record) {
        std::stringstream queryBuilder;
        queryBuilder << "INSERT INTO " << mTable->getTableName() << " VALUES (";
        auto first = true;
        auto index = 0;
        for(const auto& field : record) {
            if(!first) {
                queryBuilder << ", ";
            }

            first = false;
            const auto type = mTable->getFields()[index++].getDescription().fieldType;
            switch(type) {
                case FieldType::UINT32:
                case FieldType::UINT64: {
                    try {
                        queryBuilder << std::stoull(field);
                    } catch (std::exception& err) {
                        log_console->warn("Cannot convert value '{}' to an integer", field);
                    }
                    break;
                }

                case FieldType::FLOAT: {
                    queryBuilder << std::stof(field);
                    break;
                }

                case FieldType::BOOL: {
                    queryBuilder << ((field == "1") ? 1 : 0);
                    break;
                }

                case FieldType::STRING_OFFSET: {
                    queryBuilder << "\"" << escape(field) << "\"";
                    break;
                }
            }
        }

        queryBuilder << ");" << std::endl;

        mWriter.writeText(queryBuilder.str());
    }

    std::string SqlTableWriter::escape(const std::string &value) const {
        std::stringstream retStream;
        for(const auto& c : value) {
            switch(c) {

                case '%':
                case '\\':
                case '\'':
                case '\"': {
                    retStream << "\\" << c;
                    break;
                }

                case '\n': {
                    retStream << "\\n";
                    break;
                }

                case '\b': {
                    retStream << "\\b";
                    break;
                }

                case '\r': {
                    retStream << "\\r";
                    break;
                }

                case '\t': {
                    retStream << "\\t";
                    break;
                }

                case '\0': {
                    retStream << "\\0";
                    break;
                }

                case 26: {
                    retStream << "\\z";
                    break;
                }

                default: {
                    retStream << c;
                    break;
                }
            }
        }
        return retStream.str();
    }
}