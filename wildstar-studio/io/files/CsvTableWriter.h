
#ifndef WILDSTAR_STUDIO_CSVTABLEWRITER_H
#define WILDSTAR_STUDIO_CSVTABLEWRITER_H


#include "TableWriter.h"

namespace io::files {
    class CsvTableWriter : public TableWriter {
        std::string escape(const std::string& value);

    public:
        explicit CsvTableWriter(io::utils::BinaryWriter &writer, const DynamicTblFilePtr &table);

        void convertRow(const DynamicTblFile::TVisibleRecord &record) override;

        void onClose() override;
    };
}

#endif //WILDSTAR_STUDIO_CSVTABLEWRITER_H
