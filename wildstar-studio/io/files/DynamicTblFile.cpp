
#include <algorithm>
#include <utils/RuntimeError.h>
#include <utils/Utilities.h>
#include "DynamicTblFile.h"

namespace io::files {
    DynamicTblFile::DynamicTblFile(utils::BinaryReader reader) : mReader(std::move(reader)) {
        initialLoad();
    }

    void DynamicTblFile::initialLoad() {
        mHeader = mReader.read<TBLHeader>();

        auto nameOffset = sizeof(TBLHeader) + mHeader.ofsTableName;
        if(nameOffset % 16) {
            nameOffset += 16 - (nameOffset % 16);
        }
        mReader.seek(nameOffset);
        std::vector<char16_t> tblName(mHeader.lenTableName);
        mReader.read(tblName);
        std::basic_string<char16_t> name(tblName.begin(), tblName.end());
        mTableName = ::utils::utf16ToUtf8(name);

        if(mTableName.length() > 0 && mTableName[mTableName.length() - 1] == '\0') {
            mTableName = mTableName.substr(0, mTableName.length() - 1);
        }

        mReader.seek(sizeof(TBLHeader) + mHeader.ofsFieldDesc);
        std::vector<FieldDescription> fields(mHeader.numFields);
        mReader.read(fields);

        auto strOffset = 0x60 + mHeader.ofsFieldDesc + mHeader.numFields * sizeof(FieldDescription);
        if(strOffset % 16) {
            strOffset += 16 - (strOffset % 16);
        }

        mFields.resize(mHeader.numFields);
        std::transform(fields.begin(), fields.end(), mFields.begin(), [this, &strOffset](const auto& field) {
            mReader.seek(strOffset + field.ofsFieldTitle);
            return FieldEntry(mReader.readStringUtf16(), field);
        });
    }

    std::vector<DynamicTblFile::TVisibleRecord> DynamicTblFile::loadRecordPage(std::size_t startIndex, std::size_t numEntries) {
        if(startIndex >= mHeader.numRows || numEntries == 0) {
            return {};
        }

        const auto recordOffset = sizeof(TBLHeader) + + mHeader.ofsEntries + mHeader.recordSize * startIndex;

        numEntries = std::min(numEntries, static_cast<std::size_t>(mHeader.numRows - startIndex));
        std::vector<DynamicTblFile::TVisibleRecord> records;
        records.reserve(numEntries);

        for(auto i = 0u; i < numEntries; ++i) {
            mReader.seek(recordOffset + i * mHeader.recordSize);

            DynamicTblFile::TVisibleRecord record;
            auto skip = false;

            for(auto j = 0u; j < mFields.size(); ++j) {
                if (skip && (j > 0 && mFields[j - 1].getDescription().fieldType == FieldType::STRING_OFFSET && mFields[j].getDescription().fieldType != FieldType::STRING_OFFSET)) {
                    mReader.seekMod(4);
                }

                const auto& field = mFields[j];
                switch (mFields[j].getDescription().fieldType) {
                    case FieldType::UINT32: {
                        record.emplace_back(std::to_string(mReader.read<uint32_t>()));
                        break;
                    }

                    case FieldType::UINT64: {
                        record.emplace_back(std::to_string(mReader.read<uint64_t>()));
                        break;
                    }

                    case FieldType::FLOAT: {
                        record.emplace_back(std::to_string(mReader.read<float>()));
                        break;
                    }

                    case FieldType::BOOL: {
                        record.emplace_back(mReader.read<uint32_t>() != 0 ? "true" : "false");
                        break;
                    }

                    case FieldType::STRING_OFFSET: {
                        const auto ofsLower = mReader.read<uint32_t>();
                        uint64_t offset = mReader.read<uint32_t>();
                        skip = ofsLower == 0;

                        if (ofsLower > 0) {
                            offset = ofsLower;
                        }

                        offset += mHeader.ofsEntries + 0x60;
                        const auto oldPos = mReader.tell();
                        std::string value;
                        if (offset < mReader.getSize()) {
                            mReader.seek(offset);
                            value = mReader.readStringUtf16();
                        }

                        mReader.seek(oldPos);
                        record.emplace_back(value);
                        break;
                    }

                    default: {
                        throw ::utils::RuntimeError("Unknown field type");
                    }
                }
            }

            records.emplace_back(record);
        }

        return records;
    }

    DynamicTblFile::FieldEntry::FieldEntry(const std::string &fieldName, const FieldDescription &description) : mFieldName(fieldName), mDescription(description) {}
}