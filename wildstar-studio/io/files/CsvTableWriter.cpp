
#include "CsvTableWriter.h"
#include <regex>

namespace io::files {
    CsvTableWriter::CsvTableWriter(io::utils::BinaryWriter &writer, const DynamicTblFilePtr &table) : TableWriter(writer, table) {
        for(const auto& field : table->getFields()) {
            writer.writeText(escape(field.getFieldName()) + ";");
        }

        writer.writeText("\n");
    }

    void CsvTableWriter::convertRow(const DynamicTblFile::TVisibleRecord &record) {
        for(const auto& field : record) {
            mWriter.writeText(escape(field) + ";");
        }

        mWriter.writeText("\n");
    }

    std::string CsvTableWriter::escape(const std::string &value) {
        static std::regex quoteRegex("\"");
        std::string quoted = std::regex_replace(value, quoteRegex, "\"\"");

        if(quoted.find_first_of(";\"\r\n\t") != std::string::npos) {
            return std::string("\"") + quoted + "\"";
        }

        return quoted;
    }

    void CsvTableWriter::onClose() {

    }
}