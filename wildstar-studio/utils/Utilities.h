
#ifndef WILDSTAR_STUDIO_UTILITIES_H
#define WILDSTAR_STUDIO_UTILITIES_H

#include <string>
#include <functional>
#include <vector>
#include <memory>
#include "cairo.h"
#include "gtk/gtk.h"

namespace utils {
    std::string getExtension(const std::string& filePath);
    std::string changeExtension(const std::string& filePath, const std::string& extension);

    struct CairoSurfaceFree {
    public:
        void operator () (cairo_surface_t* surface) {
            cairo_surface_destroy(surface);
        }
    };

    typedef std::unique_ptr<cairo_surface_t, CairoSurfaceFree> CairoSurfacePtr;

    struct CairoContextFree {
    public:
        void operator() (cairo_t* cairo) {
            cairo_destroy(cairo);
        }
    };

    typedef std::unique_ptr<cairo_t, CairoContextFree> CairoContextPtr;

    template<typename T>
    struct PangoFree {
    public:
        void operator () (T* obj) {
            g_object_unref(obj);
        }
    };

    template<typename T>
    using PangoPtr = std::unique_ptr<T, PangoFree<T>>;

    struct PangoFontDescFree {
    public:
        void operator () (PangoFontDescription* desc) {
            pango_font_description_free(desc);
        }
    };

    typedef std::unique_ptr<PangoFontDescription, PangoFontDescFree> PangoFontDescPtr;

    struct FileCloser {
    public:
        bool operator()(FILE* f) {
            if(f != nullptr) {
                fclose(f);
            }
        }
    };

    GtkWidget* getChildByName(GtkWidget* parent, const std::string& name);

    void registerEventImpl(GObject* widget, const std::string& eventType, const std::function<void()>& callback);
    void registerKeyEventImpl(GObject* element, const std::string& eventType, const std::function<void(uint32_t, uint32_t)>& callback);
    void registerMotionEventImpl(GObject* element, const std::string& eventType, const std::function<void(double, double, double, double)>& callback);
    void registerButtonEventImpl(GObject *element, const std::string &eventType, const std::function<void(uint32_t, bool)> &callback);

    void runInMainThread(const std::function<void()> &callback, guint priority = G_PRIORITY_DEFAULT_IDLE);

    template<typename T>
    void registerEvent(T* element, const std::string& eventType, const std::function<void()>& callback) {
        registerEventImpl(G_OBJECT(element), eventType, callback);
    }

    template<typename T>
    void registerKeyEvent(T* element, const std::string& eventType, const std::function<void(uint32_t, uint32_t)>& callback) {
        registerKeyEventImpl(G_OBJECT(element), eventType, callback);
    }

    template<typename T>
    void registerMotionEvent(T* element, const std::string& eventType, const std::function<void(double, double, double, double)>& callback) {
        registerMotionEventImpl(G_OBJECT(element), eventType, callback);
    }

    template<typename T>
    void registerButtonEvent(T* element, const std::string& eventType, const std::function<void(uint32_t, bool)>& callback) {
        registerButtonEventImpl(G_OBJECT(element), eventType, callback);
    }

    void writeBmp(uint32_t width, uint32_t height, const std::vector<uint8_t>& pixels, const std::string& fileName);
    void writePng(uint32_t width, uint32_t height, const std::vector<uint8_t>& pixels, const std::string& fileName);

    std::string replaceExtension(const std::string& filePath, const std::string& extension);
    bool createDirectories(const std::string& filePath);

    std::string toUpper(const std::string& str);
    std::string trimLeft(const std::string& str);
    std::string trimRight(const std::string& str);
    inline std::string trim(const std::string& str) {
        return trimLeft(trimRight(str));
    }

    bool isEqualIgnoreCase(const std::string& s1, const std::string& s2);

    std::string buildStackTrace();

    std::string utf16ToUtf8(const std::basic_string<char16_t>& input);
}


#endif //WILDSTAR_STUDIO_UTILITIES_H
