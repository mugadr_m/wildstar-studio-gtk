
#include "RuntimeError.h"
#include "Utilities.h"
#include <cstring>

namespace utils {

    RuntimeError::RuntimeError(std::string &&message) : mMessage(message), mBackTrace(buildStackTrace()) {
        mMessagePtr = strdup(mMessage.c_str());
    }

    RuntimeError::RuntimeError(const RuntimeError &other) noexcept : mMessage(other.mMessage), mBackTrace(other.mBackTrace) {
        mMessagePtr = strdup(mMessage.c_str());
    }

    RuntimeError::RuntimeError(RuntimeError&& other) noexcept : mMessage(std::move(other.mMessage)), mBackTrace(std::move(other.mBackTrace))  {
        mMessagePtr = other.mMessagePtr;
        other.mMessagePtr = nullptr;
    }

    RuntimeError::~RuntimeError() {
        if(mMessagePtr != nullptr) {
            free(mMessagePtr);
            mMessagePtr = nullptr;
        }
    }

    RuntimeError &RuntimeError::operator=(const RuntimeError &other) {
        if(&other == this) {
            return *this;
        }

        mMessage = other.mMessage;
        mBackTrace = other.mBackTrace;
        mMessagePtr = strdup(other.mMessagePtr);
        return *this;
    }

    RuntimeError &RuntimeError::operator=(RuntimeError &&other) {
        if(&other == this) {
            return *this;
        }

        mMessage = std::move(other.mMessage);
        mBackTrace = std::move(other.mBackTrace);
        mMessagePtr = other.mMessagePtr;
        other.mMessagePtr = nullptr;
        return *this;
    }

    const char *RuntimeError::what() const noexcept {
        return mMessagePtr;
    }
}