
#ifndef WILDSTAR_STUDIO_DXTHELPER_H
#define WILDSTAR_STUDIO_DXTHELPER_H

#include <vector>
#include <cstdint>
#include <io/utils/BinaryReader.h>

namespace utils {
    class DxtHelper {
    public:
        enum class Format {
            BC1,
            BC2,
            BC3
        };

    private:
        struct RgbDataArray {
            RgbDataArray() : data{0} {

            }

            union data {
                uint32_t color;
                uint8_t buffer[4];
            } data;
        };

        typedef void (*tConvertFunction)(io::utils::BinaryReader&, std::vector<uint32_t>&, const std::size_t&);

        static void bc1GetBlock(io::utils::BinaryReader &stream, std::vector<uint32_t> &blockData, const size_t &blockOffset);
        static void bc2GetBlock(io::utils::BinaryReader &stream, std::vector<uint32_t> &blockData, const size_t &blockOffset);
        static void bc3GetBlock(io::utils::BinaryReader &stream, std::vector<uint32_t> &blockData, const size_t &blockOffset);

        static void readDXTColors(io::utils::BinaryReader& stream, RgbDataArray* colors, bool preMultipliedAlpha, bool use4Colors = false);

        static tConvertFunction getDxtConvertFunction(const Format &format);

    public:
        DxtHelper() = delete;

        static void decompress(const Format& format, const std::vector<uint8_t>& imageData, uint32_t width, uint32_t height, std::vector<uint32_t>& colorData);
    };
}


#endif //WILDSTAR_STUDIO_DXTHELPER_H
