
#ifndef WILDSTAR_STUDIO_RUNTIMEERROR_H
#define WILDSTAR_STUDIO_RUNTIMEERROR_H

#include <stdexcept>

namespace utils {
    class RuntimeError : public std::exception {
        std::string mMessage;
        std::string mBackTrace;

        char* mMessagePtr;
    public:
        explicit RuntimeError(std::string&& message);
        RuntimeError(const RuntimeError& other) noexcept;
        RuntimeError(RuntimeError&& other) noexcept;

        RuntimeError& operator = (const RuntimeError& other);
        RuntimeError& operator = (RuntimeError&& other);

        ~RuntimeError() override;

        const std::string& getBackTrace() const {
            return mBackTrace;
        }

        const char *what() const noexcept override;
    };
}


#endif //WILDSTAR_STUDIO_RUNTIMEERROR_H
