
#include "Utilities.h"
#include <algorithm>
#include <fstream>
#include "log.h"
#include "io/utils/BinaryWriter.h"
#include "pngpp/png.hpp"
#include <sstream>
#include <iomanip>
#include <boost/stacktrace.hpp>
#include <boost/core/demangle.hpp>

#ifndef USE_CROSS_COMPILE
#ifdef _WIN32
#include <direct.h>
#else

#include <unistd.h>
#include <codecvt>

#endif
#else
#include <direct.h>
#endif

namespace utils {
    std::string getExtension(const std::string &filePath) {
        const auto rDot = filePath.rfind('.');
        if (rDot == std::string::npos) {
            return "";
        }

        auto extension = filePath.substr(rDot);
        std::transform(extension.begin(), extension.end(), extension.begin(), [](const auto &chr) { return std::tolower(chr); });
        return extension;
    }

    std::string changeExtension(const std::string &filePath, const std::string &extension) {
        const auto rDot = filePath.rfind('.');
        if (rDot == std::string::npos) {
            return filePath;
        }

        return filePath.substr(0, rDot) + '.' + extension;
    }

    GtkWidget *getChildByName(GtkWidget *parent, const std::string &name) {
        if (!GTK_IS_CONTAINER(parent)) {
            log_console->warn("Element is not a container: {}", gtk_widget_get_name(parent));
            return nullptr;
        }

        auto children = gtk_container_get_children(GTK_CONTAINER(parent));
        if (children == nullptr) {
            return nullptr;
        }

        do {
            std::string childName = gtk_widget_get_name(GTK_WIDGET(children->data));
            if (name == childName) {
                return GTK_WIDGET(children->data);
            }

            if (GTK_IS_CONTAINER(children->data)) {
                const auto subChild = getChildByName(GTK_WIDGET(children->data), name);
                if (subChild != nullptr) {
                    return subChild;
                }
            }

            children = g_list_next(children);
        } while (children != nullptr);

        return nullptr;
    }

    struct FunctionHolder {
        std::function<void()> callback;
    };

    struct KeyEventFunctionHolder {
        std::function<void(uint32_t, uint32_t)> callback;
    };

    struct MotionEventFunctionHolder {
        std::function<void(double, double, double, double)> callback;
    };

    struct ButtonEventFunctionHolder {
        std::function<void(uint32_t, bool)> callback;
    };

    gboolean gtkEventHandler(void *, FunctionHolder *holder) {
        holder->callback();
        return TRUE;
    }

    gboolean gtkKeyEventHandler(void *, GdkEventKey *event, KeyEventFunctionHolder *holder) {
        holder->callback(event->keyval, event->state);
        return TRUE;
    }

    gboolean gtkMotionEventHandler(void *, GdkEventMotion *event, MotionEventFunctionHolder *holder) {
        holder->callback(event->x, event->y, event->x_root, event->y_root);
        return TRUE;
    }

    gboolean gtkButtonEventHandler(void *, GdkEventButton *event, ButtonEventFunctionHolder *holder) {
        holder->callback(event->button, event->type != GDK_BUTTON_RELEASE);
        return TRUE;
    }

    gboolean mainThreadCallback(gpointer data) {
        const auto *holder = reinterpret_cast<FunctionHolder *>(data);
        holder->callback();
        delete holder;
        return FALSE;
    }

    void registerEventImpl(GObject *widget, const std::string &eventType, const std::function<void()> &callback) {
        g_signal_connect_after(widget, eventType.c_str(), G_CALLBACK(gtkEventHandler), new FunctionHolder{callback});
    }

    void writeBmp(uint32_t width, uint32_t height, const std::vector<uint8_t> &pixels, const std::string &fileName) {
        io::utils::BinaryWriter writer;
        writer << uint16_t(0x4D42) << uint32_t(pixels.size() + 54) << uint32_t(0) << uint32_t(54)
               << uint32_t(40) << width << -((int32_t) height) << uint16_t(1) << uint16_t(32) << uint32_t(0)
               << uint32_t(width * height * 4) << uint32_t(0) << uint32_t(0) << uint32_t(0)
               << uint32_t(0) << pixels;

        const auto &outData = writer.getFullData();
        const auto f = fopen(fileName.c_str(), "wb");
        std::unique_ptr<FILE, FileCloser> fp(f);
        fwrite(outData.data(), 1, outData.size(), f);
    }

    void writePng(uint32_t width, uint32_t height, const std::vector<uint8_t> &pixels, const std::string &fileName) {
        png::image<png::rgba_pixel> outImage(width, height);
        for (auto y = 0u; y < height; ++y) {
            auto &row = outImage.get_row(y);
            memcpy(row.data(), pixels.data() + width * 4 * y, width * 4);
        }

        std::stringstream strm;
        outImage.write_stream(strm);
        const auto imageData = strm.str();
        const auto f = fopen(fileName.c_str(), "wb");
        std::unique_ptr<FILE, FileCloser> fp(f);
        fwrite(imageData.data(), 1, imageData.size(), f);
    }

    std::string replaceExtension(const std::string &filePath, const std::string &extension) {
        const auto rDot = filePath.rfind('.');
        if (rDot == std::string::npos) {
            return filePath + std::string(".") + extension;
        }

        std::stringstream stream;
        stream << filePath.substr(0, rDot) << "." << extension;
        return stream.str();
    }

    bool createDirectories(const std::string &filePath) {
        std::string::size_type lastSlash = filePath.rfind('/');
        if (lastSlash == std::string::npos) {
            return true;
        }

        std::string pathPart;
        std::stringstream pathStream;
        pathStream << filePath.substr(0, lastSlash);
        std::stringstream curPath;

        while (std::getline(pathStream, pathPart, '/')) {
            if (pathPart.empty()) {
                curPath << '/';
                continue;
            }

            curPath << pathPart;
#ifdef _WIN32
            int ret = _mkdir(curPath.str().c_str());
#else
            int ret = mkdir(curPath.str().c_str(), S_IRWXU | S_IRWXG | S_IROTH);
#endif
            if (ret != 0 && errno != EEXIST) {
                log_console->error("Failed to create directories at {} with error {}", curPath.str(), errno);
                return false;
            }
            curPath << '/';
        }

        return true;
    }

    std::string toUpper(const std::string &str) {
        std::string ret;
        std::transform(str.begin(), str.end(), std::back_inserter(ret), [](const auto &c) { return std::toupper(c); });
        return ret;
    }

    bool isEqualIgnoreCase(const std::string &s1, const std::string &s2) {
        return std::equal(s1.begin(), s1.end(), s2.begin(), s2.end(), [](auto a, auto b) { return std::toupper(a) == std::toupper(b); });
    }

    void registerKeyEventImpl(GObject *element, const std::string &eventType, const std::function<void(uint32_t, uint32_t)> &callback) {
        g_signal_connect(element, eventType.c_str(), G_CALLBACK(gtkKeyEventHandler), new KeyEventFunctionHolder{callback});
    }

    void registerMotionEventImpl(GObject *element, const std::string &eventType, const std::function<void(double, double, double, double)> &callback) {
        g_signal_connect(element, eventType.c_str(), G_CALLBACK(gtkMotionEventHandler), new MotionEventFunctionHolder{callback});
    }

    void registerButtonEventImpl(GObject *element, const std::string &eventType, const std::function<void(uint32_t, bool)> &callback) {
        g_signal_connect(element, eventType.c_str(), G_CALLBACK(gtkButtonEventHandler), new ButtonEventFunctionHolder{callback});
    }

    std::string trimLeft(const std::string &str) {
        const auto nonSpaceItr = std::find_if(str.begin(), str.end(), [](const auto &chr) { return !std::isblank(chr) && !std::isspace(chr); });
        return std::string(nonSpaceItr, str.end());
    }

    std::string trimRight(const std::string &str) {
        const auto nonSpaceItr = std::find_if(str.rbegin(), str.rend(), [](const auto &chr) { return !std::isblank(chr) && !std::isspace(chr); });
        if (nonSpaceItr == str.rend()) {
            return std::string();
        }

        const auto numElements = std::distance(nonSpaceItr, str.rend());
        return std::string(str.begin(), str.begin() + numElements);
    }

    std::string buildStackTrace() {
        const auto stackTrace = boost::stacktrace::stacktrace();
        std::stringstream stream;
        stream << stackTrace;
        return stream.str();
    }

    void runInMainThread(const std::function<void()> &callback, guint priority) {
        if (priority == G_PRIORITY_DEFAULT_IDLE) {
            g_idle_add(mainThreadCallback, new FunctionHolder{callback});
        } else {
            const auto source = g_idle_source_new();
            g_source_set_callback(source, mainThreadCallback, new FunctionHolder{callback}, nullptr);
            g_source_set_priority(source, priority);
            g_source_attach(source, g_main_context_default());
        }
    }

    std::string utf16ToUtf8(const std::basic_string<char16_t> &input) {
        static std::wstring_convert<std::codecvt_utf8_utf16<char16_t>, char16_t> converter;
        return converter.to_bytes(input);
    }
}