
#include <log.h>
#include "DxtHelper.h"
#include "RuntimeError.h"

namespace utils {
    static void rgb565ToRgb8Array(const uint32_t &input, uint8_t *output) {
        auto r = (uint32_t) (input & 0x1Fu);
        auto g = (uint32_t) ((input >> 5u) & 0x3Fu);
        auto b = (uint32_t) ((input >> 11u) & 0x1Fu);

        r = (r << 3u) | (r >> 2u);
        g = (g << 2u) | (g >> 4u);
        b = (b << 3u) | (b >> 2u);

        output[0] = (uint8_t) b;
        output[1] = (uint8_t) g;
        output[2] = (uint8_t) r;
    }

    void DxtHelper::decompress(const DxtHelper::Format &format, const std::vector<uint8_t> &imageData, uint32_t width, uint32_t height, std::vector<uint32_t> &colorData) {
        const auto numBlocks = ((width + 3u) / 4u) * ((height + 3u) / 4u);
        std::vector<uint32_t> blockData(numBlocks * 16u);
        auto converter = getDxtConvertFunction(format);
        io::utils::BinaryReader reader(imageData);

        for (uint32_t i = 0u; i < numBlocks; ++i) {
            (*converter)(reader, blockData, std::size_t(i * 16));
        }

        std::vector<uint32_t> rowBuffer(width);
        for (uint32_t y = 0u; y < height; ++y) {
            for (uint32_t x = 0u; x < width; ++x) {
                uint32_t bx = x / 4u;
                uint32_t by = y / 4u;

                uint32_t ibx = x % 4u;
                uint32_t iby = y % 4u;

                uint32_t blockIndex = by * ((width + 3u) / 4u) + bx;
                uint32_t innerIndex = iby * 4u + ibx;
                rowBuffer[x] = blockData[blockIndex * 16u + innerIndex];
            }

            memcpy(colorData.data() + y * width, rowBuffer.data(), rowBuffer.size() * sizeof(uint32_t));
        }
    }

    DxtHelper::tConvertFunction DxtHelper::getDxtConvertFunction(const Format &format) {
        switch (format) {
            case Format::BC1:
                return &DxtHelper::bc1GetBlock;
            case Format::BC2:
                return &DxtHelper::bc2GetBlock;
            case Format::BC3:
                return &DxtHelper::bc3GetBlock;
            default: {
                log_console->warn("Invalid DXT compression format, must be 0, 1 or 2, but was {}", static_cast<uint32_t>(format));
                throw RuntimeError("Invalid DXT compression format");
            }
        }
    }

    void DxtHelper::bc1GetBlock(io::utils::BinaryReader &stream, std::vector<uint32_t> &blockData, const size_t &blockOffset) {
        RgbDataArray colors[4];
        readDXTColors(stream, colors, true);

        const auto indices = stream.read<uint32_t>();
        for (auto i = 0u; i < 16u; ++i) {
            const auto idx = (uint8_t) ((indices >> (2u * i)) & 3u);
            blockData[blockOffset + i] = colors[idx].data.color;
        }
    }

    void DxtHelper::bc2GetBlock(io::utils::BinaryReader &stream, std::vector<uint32_t> &blockData, const size_t &blockOffset) {
        uint8_t alphaValues[16];
        const auto alpha = stream.read<uint64_t>();
        for (auto i = 0u; i < 16u; ++i) {
            alphaValues[i] = (uint8_t) (((alpha >> (4u * i)) & 0x0Fu) * 17);
        }

        RgbDataArray colors[4];
        readDXTColors(stream, colors, false, true);

        const auto indices = stream.read<uint32_t>();
        for (auto i = 0u; i < 16u; ++i) {
            const auto idx = (uint8_t) ((indices >> (2u * i)) & 3u);
            const auto alphaVal = (uint32_t) alphaValues[i];
            blockData[blockOffset + i] = (colors[idx].data.color & 0x00FFFFFFu) | (alphaVal << 24u);
        }
    }

    void DxtHelper::bc3GetBlock(io::utils::BinaryReader &stream, std::vector<uint32_t> &blockData, const size_t &blockOffset) {
        uint8_t alphaValues[8];
        uint8_t alphaLookup[16];

        const auto alpha1 = (uint32_t) stream.read<uint8_t>();
        const auto alpha2 = (uint32_t) stream.read<uint8_t>();

        alphaValues[0] = (uint8_t) alpha1;
        alphaValues[1] = (uint8_t) alpha2;

        if (alpha1 > alpha2) {
            for (auto i = 0u; i < 6u; ++i) {
                alphaValues[i + 2u] = (uint8_t) (((6u - i) * alpha1 + (1u + i) * alpha2) / 7u);
            }
        } else {
            for (auto i = 0u; i < 4u; ++i) {
                alphaValues[i + 2u] = (uint8_t) (((4u - i) * alpha1 + (1u + i) * alpha2) / 5u);
            }

            alphaValues[6] = 0;
            alphaValues[7] = 255;
        }

        uint64_t lookupValue = 0;
        stream.read(&lookupValue, 6);

        for (auto i = 0u; i < 16u; ++i) {
            alphaLookup[i] = (uint8_t) ((lookupValue >> (i * 3u)) & 7u);
        }

        RgbDataArray colors[4];
        readDXTColors(stream, colors, false);

        const auto indices = stream.read<uint32_t>();
        for (auto i = 0u; i < 16u; ++i) {
            const auto idx = (uint8_t) ((indices >> (2u * i)) & 3u);
            const auto alphaVal = (uint32_t) alphaValues[alphaLookup[i]];
            blockData[blockOffset + i] = (colors[idx].data.color & 0x00FFFFFFu) | (alphaVal << 24u);
        }
    }

    void DxtHelper::readDXTColors(io::utils::BinaryReader &stream, DxtHelper::RgbDataArray *colors, bool preMultipliedAlpha, bool use4Colors) {
        const auto color1 = stream.read<uint16_t>();
        const auto color2 = stream.read<uint16_t>();

        rgb565ToRgb8Array(color1, colors[0].data.buffer);
        rgb565ToRgb8Array(color2, colors[1].data.buffer);

        colors[0].data.buffer[3] = 0xFFu;
        colors[1].data.buffer[3] = 0xFFu;
        colors[2].data.buffer[3] = 0xFFu;
        colors[3].data.buffer[3] = 0xFFu;

        if (use4Colors || color1 > color2) {
            for (auto i = 0u; i < 3u; ++i) {
                colors[3].data.buffer[i] = (uint8_t) ((colors[0].data.buffer[i] + 2u * colors[1].data.buffer[i]) / 3u);
                colors[2].data.buffer[i] = (uint8_t) ((2u * colors[0].data.buffer[i] + colors[1].data.buffer[i]) / 3u);
            }
        } else {
            for (auto i = 0u; i < 3u; ++i) {
                colors[2].data.buffer[i] = (uint8_t) ((colors[0].data.buffer[i] + colors[1].data.buffer[i]) / 2u);
                colors[3].data.buffer[i] = 0;
            }

            if (preMultipliedAlpha) {
                colors[3].data.buffer[3] = 0;
            }
        }
    }
}