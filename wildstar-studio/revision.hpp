
#ifndef WILDSTAR_STUDIO_REVISION_H
#define WILDSTAR_STUDIO_REVISION_H

#include <string>

extern std::string GIT_VERSION_HASH;
extern std::string GIT_VERSION_NAME;
extern std::string CMAKE_OS_NAME;
extern int APPLICATION_VERSION;

#endif //WILDSTAR_STUDIO_REVISION_H
