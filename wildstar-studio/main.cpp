#include <iostream>
#include <memory>
#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>
#include <utils/RuntimeError.h>
#include <ui/MainWindow.h>
#include <codecvt>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "log.h"
#include "io/settings/SettingsManager.h"
#include "revision.hpp"

std::shared_ptr<spdlog::logger> log_console;

std::unique_ptr<ui::MainWindow> mainWindowPtr;

int main(int argc, char *argv[]) {
    std::set_terminate([]() {
        std::cout << "Terminate was called" << std::endl;
        const auto curException = std::current_exception();
        if(curException) {
            try {
                std::rethrow_exception(curException);
            } catch (utils::RuntimeError& err) {
                std::cout << "Unhandled Runtime Error: " << err.what() << std::endl;
                std::cout << err.getBackTrace() << std::endl;
            } catch (std::exception& e) {
                std::cout << "Unhandled C++ exception: " << e.what() << std::endl;
            } catch(...) {
                std::cout << "Unhandled unknown exception" << std::endl;
            }
        }
        std::abort();
    });

    log_console = spdlog::stdout_color_mt("console");
    log_console->set_pattern("%^[%Y-%m-%d %H:%I:%S.%e] [%P:%t] [%n] [%l] %v%$");

    io::settings::SettingsManager::instance()->validate();

    gtk_init(&argc, &argv);

    // The UI builder wont be able to recognize 3rd party elements in the UI template XML file
    // unless they have been created once before.
    const auto sourceView = gtk_source_view_new();
    gtk_widget_destroy(sourceView);

    log_console->info("Using GTK {}.{}.{}", GTK_MAJOR_VERSION, GTK_MINOR_VERSION, GTK_MICRO_VERSION);
    log_console->info("Using GTK Source View {}.{}.{}", GTK_SOURCE_MAJOR_VERSION, GTK_SOURCE_MINOR_VERSION, GTK_SOURCE_MICRO_VERSION);

    mainWindowPtr = std::make_unique<ui::MainWindow>();
    mainWindowPtr->runLoop();
    return 0;
}