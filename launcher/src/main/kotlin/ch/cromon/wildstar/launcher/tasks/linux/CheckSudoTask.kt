package ch.cromon.wildstar.launcher.tasks.linux

import ch.cromon.wildstar.launcher.tasks.Context
import ch.cromon.wildstar.launcher.tasks.Task
import org.apache.commons.io.IOUtils
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit

class CheckSudoTask : Task<LinuxContext> {
    private companion object {
        private val idRegex = Regex("uid=([0-9]+)\\([^)]{1,32}\\) gid=([0-9]+)\\([^)]{1,32}\\)")
    }

    override fun execute(context: LinuxContext): Boolean {
        val uid = readUserId(context) ?: return false
        if(uid != 0) {
            context.messageCallback("Program must be run as root (sudo java -jar ...) in order to install packages")
            return false
        }

        return true
    }

    private fun readUserId(context: LinuxContext): Int? {
        val proc = Runtime.getRuntime().exec("id")
        try {
            proc.waitFor(5, TimeUnit.SECONDS)
            if(proc.exitValue() != 0) {
                context.messageCallback("Cannot execute 'id'")
                return null
            }
        } catch (e: Exception) {
            context.messageCallback("Error executing 'id': $e")
            return null
        }

        val output = IOUtils.toString(proc.inputStream, StandardCharsets.US_ASCII)

        val result = idRegex.find(output, 0)
        if(result == null) {
            context.messageCallback("Unable to parse output from 'id' process: $output")
            return null
        }

        val uid = result.groupValues[1]
        val gid = result.groupValues[2]

        context.messageCallback("User: $uid, Group: $gid")
        return uid.toInt()
    }
}