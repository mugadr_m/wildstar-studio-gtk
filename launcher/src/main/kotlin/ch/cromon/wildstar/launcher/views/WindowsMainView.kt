package ch.cromon.wildstar.launcher.views

import ch.cromon.wildstar.launcher.controllers.WindowsController
import ch.cromon.wildstar.launcher.styles.CommonStyles.Companion.applicationHeader
import ch.cromon.wildstar.launcher.styles.CommonStyles.Companion.logTextArea
import ch.cromon.wildstar.launcher.styles.CommonStyles.Companion.sectionHeader
import ch.cromon.wildstar.launcher.styles.CommonStyles.Companion.windowRoot
import javafx.scene.control.TextArea
import tornadofx.*

class WindowsMainView : View() {
    private val controller: WindowsController by inject()
    private lateinit var logBox: TextArea

    override val root = vbox {
        addClass(windowRoot)

        label(text = "Wildstar Studio Launcher") {
            addClass(applicationHeader)
        }
        vbox {
            label(text = "System Info") {
                addClass(sectionHeader)
            }
            label(text = "Windows: ${System.getProperty("os.version")}")
            label(text = "Java: ${System.getProperty("java.version")}")
        }
        vbox {
            logBox = textarea {
                addClass(logTextArea)
                isEditable = false
            }
        }
    }

    override fun onBeforeShow() {
        super.onBeforeShow()
        controller.onStart().message.onChange {
            if(it != null) {
                logBox.appendText(it + System.lineSeparator())
            }
        }
    }
}