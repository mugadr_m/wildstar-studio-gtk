package ch.cromon.wildstar.launcher.utils

interface UnixPackageManager {
    fun hasPackage(name: String): Boolean
    fun installPackage(name: String): Boolean
}