package ch.cromon.wildstar.launcher.utils

interface AptPackageManager : UnixPackageManager {
    override fun installPackage(name: String): Boolean {
        println("sudo apt install $name")
        return true
    }
}