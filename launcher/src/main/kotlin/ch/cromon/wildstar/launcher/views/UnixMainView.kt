package ch.cromon.wildstar.launcher.views

import ch.cromon.wildstar.launcher.styles.CommonStyles.Companion.applicationHeader
import ch.cromon.wildstar.launcher.styles.CommonStyles.Companion.windowRoot
import tornadofx.View
import tornadofx.addClass
import tornadofx.label
import tornadofx.vbox

class UnixMainView : View() {
    init {
        title = "Wildstar Studio"
    }

    override val root = vbox {
        addClass(windowRoot)

        label(text = "Wildstar Studio Launcher") {
            addClass(applicationHeader)
        }
    }

}