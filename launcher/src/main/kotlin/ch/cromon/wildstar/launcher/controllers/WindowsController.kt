package ch.cromon.wildstar.launcher.controllers

import tornadofx.Controller
import tornadofx.TaskStatus

class WindowsController : Controller() {
    fun onStart(): TaskStatus {
        val taskStatus = TaskStatus()
        runAsync(taskStatus) {
            Exception().stackTrace.forEach { updateMessage(it.toString()) }
        }

        return taskStatus
    }
}