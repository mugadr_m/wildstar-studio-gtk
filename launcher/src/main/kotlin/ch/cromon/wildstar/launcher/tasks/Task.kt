package ch.cromon.wildstar.launcher.tasks

interface Task<T : Context> {
    fun execute(context: T): Boolean
}