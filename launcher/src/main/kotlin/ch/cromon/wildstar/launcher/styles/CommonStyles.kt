package ch.cromon.wildstar.launcher.styles

import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class CommonStyles : Stylesheet() {
    companion object {
        val windowRoot by cssclass()
        val applicationHeader by cssclass()
        val sectionHeader by cssclass()
        val logTextArea by cssclass()
    }

    init {
        windowRoot {
            padding = box(10.px)
        }

        applicationHeader {
            fontSize = 36.px
        }

        sectionHeader {
            fontSize = 16.px
            fontWeight = FontWeight.BOLD
        }

        logTextArea {
            fontFamily = "Consolas"
            minHeight = 200.px
        }
    }
}