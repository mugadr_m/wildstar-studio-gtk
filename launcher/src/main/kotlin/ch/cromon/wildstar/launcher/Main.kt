package ch.cromon.wildstar.launcher

import ch.cromon.wildstar.launcher.styles.CommonStyles
import ch.cromon.wildstar.launcher.views.UnixMainView
import ch.cromon.wildstar.launcher.views.WindowsMainView
import tornadofx.App
import tornadofx.launch

class WindowsApplication : App(WindowsMainView::class, CommonStyles::class)
class UnixApplication : App(UnixMainView::class, CommonStyles::class)

fun main(vararg args: String) {
    val osName = System.getProperty("os.name")
    when {
        osName.contains("windows", true) -> launch<WindowsApplication>(*args)
        osName.contains("linux", true) -> launch<UnixApplication>(*args)
        else -> throw IllegalStateException("OS not supported: $osName")
    }
}