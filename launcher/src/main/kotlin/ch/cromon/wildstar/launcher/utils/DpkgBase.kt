package ch.cromon.wildstar.launcher.utils

interface DpkgBase : UnixPackageManager {
    override fun hasPackage(name: String): Boolean {
        println("sudo dpkg -l $name")
        return false
    }
}